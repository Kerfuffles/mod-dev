#include "BIS_AddonInfo.hpp"
class CfgPatches {

	class insignia_addon
	{
		units[] = {};
		weapons[] = {};
		requiredAddons[] = {};
		version = "0.4";
		author[]= {"C. Crabill","SLT D.Dragitov"};
		authorUrl = "";
	};

};

class CfgUnitInsignia
{
	
	class insignia_cobra
	{
		displayName = "Cobra Insignia";
		author = "C. Crabill";
		texture = "\insignia_addon\icon\Cobra.paa";
		textureVehicle = "";
	};
	
	class insignia_IEU
	{
		displayName = "IEU Insignia";
		author = "";
		texture = "\insignia_addon\icon\IEU.paa";
		textureVehicle = "";
	};
	class insignia_Recon
	{
		displayName = "Recon";
		author = "";
		texture = "\insignia_addon\icon\Recon.paa";
		textureVehicle = "";
	};
	class insignia_RCT
	{
		displayName = "[IEU] RCT";
		author = "";
		texture = "\insignia_addon\icon\RCT.paa";
		textureVehicle = "";
	};
	class insignia_1SG
	{
		displayName = "[IEU] 1SG";
		author = "";
		texture = "\insignia_addon\icon\1SG.paa";
		textureVehicle = "";
	};
	class insignia_2LT
	{
		displayName = "[IEU] 2LT";
		author = "";
		texture = "\insignia_addon\icon\2LT.paa";
		textureVehicle = "";
	};
	class insignia_COL
	{
		displayName = "[IEU] COL";
		author = "";
		texture = "\insignia_addon\icon\COL.paa";
		textureVehicle = "";
	};
	class insignia_CPT
	{
		displayName = "[IEU] CPT";
		author = "";
		texture = "\insignia_addon\icon\CPT.paa";
		textureVehicle = "";
	};
	class insignia_CSM
	{
		displayName = "[IEU] CSM";
		author = "";
		texture = "\insignia_addon\icon\CSM.paa";
		textureVehicle = "";
	};
	class insignia_CSP
	{
		displayName = "[IEU] CSP";
		author = "";
		texture = "\insignia_addon\icon\CSP.paa";
		textureVehicle = "";
	};
	class insignia_CW1
	{
		displayName = "[IEU] CW1";
		author = "";
		texture = "\insignia_addon\icon\CW1.paa";
		textureVehicle = "";
	};
	class insignia_CW2
	{
		displayName = "[IEU] CW2";
		author = "";
		texture = "\insignia_addon\icon\CW2.paa";
		textureVehicle = "";
	};
	class insignia_CW3
	{
		displayName = "[IEU] CW3";
		author = "";
		texture = "\insignia_addon\icon\CW3.paa";
		textureVehicle = "";
	};
	class insignia_CW4
	{
		displayName = "[IEU] CW4";
		author = "";
		texture = "\insignia_addon\icon\CW4.paa";
		textureVehicle = "";
	};
	class insignia_CW5
	{
		displayName = "[IEU] CW5";
		author = "";
		texture = "\insignia_addon\icon\CW5.paa";
		textureVehicle = "";
	};
	class insignia_JLT
	{
		displayName = "[IEU] JLT";
		author = "";
		texture = "\insignia_addon\icon\JLT.paa";
		textureVehicle = "";
	};
	class insignia_JSG
	{
		displayName = "[IEU] JSG";
		author = "";
		texture = "\insignia_addon\icon\JSG.paa";
		textureVehicle = "";
	};
	class insignia_JWO
	{
		displayName = "[IEU] JWO";
		author = "";
		texture = "\insignia_addon\icon\JWO.paa";
		textureVehicle = "";
	};
	class insignia_LTC
	{
		displayName = "[IEU] LTC";
		author = "";
		texture = "\insignia_addon\icon\LTC.paa";
		textureVehicle = "";
	};
	class insignia_MAJ
	{
		displayName = "[IEU] MAJ";
		author = "";
		texture = "\insignia_addon\icon\MAJ.paa";
		textureVehicle = "";
	};
	class insignia_MCWO
	{
		displayName = "[IEU] MCWO";
		author = "";
		texture = "\insignia_addon\icon\MCWO.paa";
		textureVehicle = "";
	};
	class insignia_MGS
	{
		displayName = "[IEU] MGS";
		author = "";
		texture = "\insignia_addon\icon\MGS.paa";
		textureVehicle = "";
	};
	class insignia_OCD
	{
		displayName = "[IEU] OCD";
		author = "";
		texture = "\insignia_addon\icon\OCD.paa";
		textureVehicle = "";
	};
	class insignia_PVT
	{
		displayName = "[IEU] PVT";
		author = "";
		texture = "\insignia_addon\icon\PVT.paa";
		textureVehicle = "";
	};
	class insignia_SCP
	{
		displayName = "[IEU] SCP";
		author = "";
		texture = "\insignia_addon\icon\SCP.paa";
		textureVehicle = "";
	};
	class insignia_SCWO
	{
		displayName = "[IEU] SCWO";
		author = "";
		texture = "\insignia_addon\icon\SCWO.paa";
		textureVehicle = "";
	};
	class insignia_SFC
	{
		displayName = "[IEU] SFC";
		author = "";
		texture = "\insignia_addon\icon\SFC.paa";
		textureVehicle = "";
	};
	class insignia_SGM
	{
		displayName = "[IEU] SGM";
		author = "";
		texture = "\insignia_addon\icon\SGM.paa";
		textureVehicle = "";
	};
	class insignia_SGT
	{
		displayName = "[IEU] SGT";
		author = "";
		texture = "\insignia_addon\icon\SGT.paa";
		textureVehicle = "";
	};
	class insignia_SLT
	{
		displayName = "[IEU] SLT";
		author = "";
		texture = "\insignia_addon\icon\SLT.paa";
		textureVehicle = "";
	};
	class insignia_SP4
	{
		displayName = "[IEU] SP4";
		author = "";
		texture = "\insignia_addon\icon\SP4.paa";
		textureVehicle = "";
	};
	class insignia_SP5
	{
		displayName = "[IEU] SP5";
		author = "";
		texture = "\insignia_addon\icon\SP5.paa";
		textureVehicle = "";
	};
	class insignia_SP6
	{
		displayName = "[IEU] SP6";
		author = "";
		texture = "\insignia_addon\icon\SP6.paa";
		textureVehicle = "";
	};
	class insignia_SP7
	{
		displayName = "[IEU] SP7";
		author = "";
		texture = "\insignia_addon\icon\SP7.paa";
		textureVehicle = "";
	};
	class insignia_SP8
	{
		displayName = "[IEU] SP8";
		author = "";
		texture = "\insignia_addon\icon\SP8.paa";
		textureVehicle = "";
	};
	class insignia_SSG
	{
		displayName = "[IEU] SSG";
		author = "";
		texture = "\insignia_addon\icon\SSG.paa";
		textureVehicle = "";
	};
	class insignia_STP
	{
		displayName = "[IEU] STP";
		author = "";
		texture = "\insignia_addon\icon\STP.paa";
		textureVehicle = "";
	};
	class insignia_TPR
	{
		displayName = "[IEU] TPR";
		author = "";
		texture = "\insignia_addon\icon\TPR.paa";
		textureVehicle = "";
	};
	class insignia_1LT
	{
		displayName = "[IEU] 1LT";
		author = "";
		texture = "\insignia_addon\icon\1LT.paa";
		textureVehicle = "";
	};
	class insignia_RedCell
	{
		displayName = "[IEU] RedCell";
		author = "";
		texture = "\insignia_addon\icon\redcell2.paa";
		textureVehicle = "";
	};
	class insignia_TF1
	{
		displayName = "[IEU] TF1";
		author = "";
		texture = "\insignia_addon\icon\tf1.paa";
		textureVehicle = "";
	};
};