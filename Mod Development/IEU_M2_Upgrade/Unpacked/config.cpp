class CfgPatches {
	class M2A6FU {
		version = 1;
		units[] = {"B_MBT_01_FU_F", "B_MBT_01_FUC_AS_F", "B_MBT_01_FUC_IT_F"};
		weapons[] = {"cannon_120mmFU", "cannon_140mmFU","HMG_127_FU_Hull", "HMG_127_FU_Commander", "20mm_cannon_FU_Hull", "20mm_cannon_FU_Commander", "GMG_20mm_FU_Commander"};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Data_F", "A3_armor_f_gamma", "A3_armor_f_epc", "A3_Weapons_F"};
	};
};

class CfgAmmo {
	class Default;	// External class reference
	class BulletCore;	// External class reference
	class Sh_120mm_APFSDS;	// External class reference
	class Sh_120mm_HE;	// External class reference
	
	class Sh_140mm_APFSDS : Sh_120mm_APFSDS {
		airFriction = -4e-005;
		typicalSpeed = 1800;
		hit = 650;
		caliber = 35;
		indirectHit = 20;
	};
	
	class Sh_140mm_HE : Sh_120mm_HE {
		airFriction = -4e-005;
		caliber = 35;
		hit = 280;
		indirectHit = 100;
		indirectHitRange = 10;
		dangerRadiusHit = 200;
		suppressionRadiusHit = 50;
		typicalSpeed = 1500;
		explosive = 0.8;
		CraterEffects = "ArtyShellCrater";
		ExplosionEffects = "MortarExplosion";
		soundHit1[] = {"A3\Sounds_F\arsenal\explosives\shells\Artillery_tank_shell_155mm_explosion_01", 2.51189, 1, 1900};
		soundHit2[] = {"A3\Sounds_F\arsenal\explosives\shells\Artillery_tank_shell_155mm_explosion_02", 2.51189, 1, 1900};
		soundHit3[] = {"A3\Sounds_F\arsenal\explosives\shells\Artillery_tank_shell_155mm_explosion_03", 2.51189, 1, 1900};
		soundHit4[] = {"A3\Sounds_F\arsenal\explosives\shells\Artillery_tank_shell_155mm_explosion_04", 2.51189, 1, 1900};
		multiSoundHit[] = {"soundHit1", 0.25, "soundHit2", 0.25, "soundHit3", 0.25, "soundHit4", 0.25};
		
		class CamShakeExplode {
			power = (155*0.2);
			duration = "((round (155^0.5))*0.2 max 0.2)";
			frequency = 20;
			distance = ((30 + 155^0.5)*8);
		};
		
		class CamShakeHit {
			power = 155;
			duration = "((round (155^0.25))*0.2 max 0.2)";
			frequency = 20;
			distance = 1;
		};
		
		class CamShakeFire {
			power = (155^0.25);
			duration = "((round (155^0.5))*0.2 max 0.2)";
			frequency = 20;
			distance = ((155^0.5)*8);
		};
		
		class CamShakePlayerFire {
			power = (155^0.25);
			duration = "((round (155^0.5))*0.2 max 0.2)";
			frequency = 20;
			distance = ((155^0.5)*8);
		};
	};


	class Sh_140mm_HET : Sh_140mm_HE {
		hit = 100;
		indirectHit = 80;
		indirectHitRange = 30;
		dangerRadiusHit = 250;
		suppressionRadiusHit = 80;
		typicalSpeed = 1000;
		explosive = 0.9;
		CraterEffects = "BombCrater";
		explosionEffects = "BombExplosion";
		soundHit1[] = {"A3\Sounds_F\weapons\Explosion\expl_big_1", 2.51189, 1, 2400};
		soundHit2[] = {"A3\Sounds_F\weapons\Explosion\expl_big_2", 2.51189, 1, 2400};
		soundHit3[] = {"A3\Sounds_F\weapons\Explosion\expl_big_3", 2.51189, 1, 2400};
		soundHit4[] = {"A3\Sounds_F\weapons\Explosion\expl_shell_1", 2.51189, 1, 2400};
		soundHit5[] = {"A3\Sounds_F\weapons\Explosion\expl_shell_2", 2.51189, 1, 2400};
		multiSoundHit[] = {"soundHit1", 0.2, "soundHit2", 0.2, "soundHit3", 0.2, "soundHit4", 0.2, "soundHit5", 0.2};
		explosionSoundEffect = "DefaultExplosion";
		
	};
};

class CfgMagazines {
	
	class VehicleMagazine;	// External class reference
	
	class 4Rnd_140mm_HE_shells : VehicleMagazine {
		scope = 2;
		displayName = "140mm HE";
		displayNameShort = "140mm HE";
		ammo = "Sh_140mm_HE";
		count = 4;
		initSpeed = 1500;
		maxLeadSpeed = 300;
		tracersEvery = 0;
		nameSound = "cannon";
	};
	
	class 4Rnd_140mm_APFSDS_shells : 4Rnd_140mm_HE_shells {
		scope = 2;
		displayName = "140mm APFSDS";
		displayNameShort = "140mm APFSDS";
		ammo = "Sh_140mm_APFSDS";
		initSpeed = 1800;
	};
	
	class 4Rnd_140mm_HET_shells : 4Rnd_140mm_HE_shells {
		scope = 2;
		displayName = "140mm FAE";
		displayNameShort = "140mm FAE";
		ammo = "Sh_140mm_HET";
		initSpeed = 1000;
	};
	
};

class RCWSOptics;	// External class reference
class Optics_Armored;	// External class reference

class Optics_Commander_01 : Optics_Armored {
	class Wide;	// External class reference
	class Medium;	// External class reference
	class Narrow;	// External class reference
};

class Optics_Gunner_MBT_01 : Optics_Armored {
	class Wide;	// External class reference
	class Medium;	// External class reference
	class Narrow;	// External class reference
};

class Mode_SemiAuto;	// External class reference
class Mode_Burst;	// External class reference
class Mode_FullAuto;	// External class reference

class CfgWeapons {
	class MGun;	// External class reference
	class HMG_127;	// External class reference
	class HMG_127_MBT;	// External class reference
	class GMG_20mm;	// External class reference
	class CannonCore;	// External class reference
	class cannon_120mm : CannonCore {
		class player;	// External class reference
	};
	class HMG_127_APC : HMG_127 {};
	
	
	class cannon_120mmFU : cannon_120mm {
		lockAcquire = 1;
		weaponLockDelay = 3; // Most of the guided weapons
		weaponLockSystem = 4; // Laser lock
		canLock = 2; // Always locks
		laserLock = 1;
		ballisticsComputer = 1 + 2 + 4 + 8;
		magazines[] = {"32Rnd_120mm_APFSDS_shells", "32Rnd_120mm_APFSDS_shells_Tracer_Red", "32Rnd_120mm_APFSDS_shells_Tracer_Green", "32Rnd_120mm_APFSDS_shells_Tracer_Yellow", "30Rnd_120mm_APFSDS_shells", "30Rnd_120mm_APFSDS_shells_Tracer_Red", "30Rnd_120mm_APFSDS_shells_Tracer_Green", "30Rnd_120mm_APFSDS_shells_Tracer_Yellow", "30Rnd_120mm_HE_shells", "30Rnd_120mm_HE_shells_Tracer_Red", "30Rnd_120mm_HE_shells_Tracer_Green", "30Rnd_120mm_HE_shells_Tracer_Yellow", "16Rnd_120mm_HE_shells", "16Rnd_120mm_HE_shells_Tracer_Red", "16Rnd_120mm_HE_shells_Tracer_Green", "16Rnd_120mm_HE_shells_Tracer_Yellow"};
	};
	
	class cannon_140mmFU : cannon_120mm {
		displayName = "Cannon 140 mm";
		lockAcquire = 1;
		weaponLockDelay = 3; // Most of the guided weapons
		weaponLockSystem = 4; // Laser lock
		canLock = 2; // Always locks
		laserLock = 1;
		ballisticsComputer = 1 + 2 + 4 + 8;
		magazineReloadTime = 20;
		magazines[] = {"4Rnd_140mm_APFSDS_shells", "4Rnd_140mm_HE_shells", "4Rnd_140mm_HET_shells"};
		
		sounds[] = {"StandardSound"};
			
			class StandardSound {
				begin1[] = {"A3\Sounds_F\arsenal\weapons_vehicles\cannon_155mm\sochor_155mm_distant", 2.51189, 1, 1500};
				soundBegin[] = {"begin1", 1};
			};
			
		reloadSound[] = {"A3\Sounds_F\vehicles\armor\noises\reload_tank_cannon_2", 31.6228, 1, 15};
		reloadMagazineSound[] = {"A3\Sounds_F\vehicles\armor\noises\reload_tank_cannon_2", 31.6228, 1, 15};
		class GunParticles {
			class Effect1 {
				effectName = "CannonFired";
				positionName = "Usti hlavne";
				directionName = "Konec hlavne";
			};
			class Effect2 {
				effectName = "ArtilleryFired1";
				positionName = "Usti hlavne";
				directionName = "Konec hlavne";
			};
			
			class Effect3 {
				effectName = "ArtilleryFiredL";
				positionName = "Usti hlavne";
				directionName = "Konec hlavne";
			};
			
			class Effect4 {
				effectName = "ArtilleryFiredR";
				positionName = "Usti hlavne";
				directionName = "Konec hlavne";
			};
		};
		
		
		modes[] = {"player", "close", "short", "medium", "far"};
		
		class player : Mode_SemiAuto {
			aiRateOfFire = 6;
			sounds[] = {"StandardSound"};
			
			class StandardSound {
				begin1[] = {"A3\Sounds_F\arsenal\weapons_vehicles\cannon_155mm\sochor_155mm_distant", 2.51189, 0.7, 1500};
				soundBegin[] = {"begin1", 1};
			};
			reloadSound[] = {"A3\Sounds_F\vehicles\armor\noises\reload_tank_cannon_2", 31.6228, 1, 15};
			reloadMagazineSound[] = {"A3\Sounds_F\vehicles\armor\noises\reload_tank_cannon_2", 31.6228, 0.7, 15};
			soundContinuous = 0;
			reloadTime = 5.5;
			magazineReloadTime = 20;
		};
		
	};
	
	class 20mm_cannon_FU_Hull : HMG_127 {
	displayName = "20mm Cannon";
	lockAcquire = 0;
	weaponLockDelay = 3; // Most of the guided weapons
	weaponLockSystem = 4; // Laser lock
	canLock = 2; // Always locks
	laserLock = 1;
	ballisticsComputer = 1 + 2 + 4 + 8;
	magazineReloadTime = 10;
	reloadTime = 0.18;
	reloadAction = "ManActReloadMagazine";
	reloadMagazineSound[] = {"A3\Sounds_F\arsenal\weapons_static\Static_HMG\reload_static_HMG", 10.0, 1, 20};
	magazines[] = {"2000Rnd_20mm_shells", "1000Rnd_20mm_shells", "300Rnd_20mm_shells"};
		modes[] = {"manual", "close", "short", "medium", "far"};
	class manual : MGun {
	reloadTime = 0.18;
	sounds[] = {"StandardSound"};
			
			class StandardSound {
				begin1[] = {"A3\Sounds_F\arsenal\weapons_static\Static_GMG\GMG_01", 1.12202, 1.0, 1200};
				begin2[] = {"A3\Sounds_F\arsenal\weapons_static\Static_GMG\GMG_02", 1.12202, 1.0, 1200};
				begin3[] = {"A3\Sounds_F\arsenal\weapons_static\Static_GMG\GMG_03", 1.12202, 1.0, 1200};
				soundBegin[] = {"begin1", 0.33, "begin2", 0.33, "begin3", 0.34};
			};
			soundContinuous = 0;
			soundBurst = 0;
		
	};
		class close : manual {
	sounds[] = {"StandardSound"};
			class StandardSound {
				begin1[] = {"A3\Sounds_F\arsenal\weapons_static\Static_GMG\GMG_01", 1.12202, 1.0, 1200};
				begin2[] = {"A3\Sounds_F\arsenal\weapons_static\Static_GMG\GMG_02", 1.12202, 1.0, 1200};
				begin3[] = {"A3\Sounds_F\arsenal\weapons_static\Static_GMG\GMG_03", 1.12202, 1.0, 1200};
				soundBegin[] = {"begin1", 0.33, "begin2", 0.33, "begin3", 0.34};
			};
			burst = 12;
			aiRateOfFire = 1;
			aiRateOfFireDistance = 50;
			minRange = 0;
			minRangeProbab = 0.05;
			midRange = 20;
			midRangeProbab = 0.7;
			maxRange = 50;
			maxRangeProbab = 0.1;
			showToPlayer = 0;
		};
	class short : close {
	sounds[] = {"StandardSound"};
			class StandardSound {
				begin1[] = {"A3\Sounds_F\arsenal\weapons_static\Static_GMG\GMG_01", 1.12202, 1.0, 1200};
				begin2[] = {"A3\Sounds_F\arsenal\weapons_static\Static_GMG\GMG_02", 1.12202, 1.0, 1200};
				begin3[] = {"A3\Sounds_F\arsenal\weapons_static\Static_GMG\GMG_03", 1.12202, 1.0, 1200};
				soundBegin[] = {"begin1", 0.33, "begin2", 0.33, "begin3", 0.34};
			};
			burst = 6;
			aiRateOfFire = 2;
			aiRateOfFireDistance = 300;
			minRange = 50;
			minRangeProbab = 0.05;
			midRange = 200;
			midRangeProbab = 0.7;
			maxRange = 300;
			maxRangeProbab = 0.1;
		};
		
		class medium : close {
	sounds[] = {"StandardSound"};
			class StandardSound {
				begin1[] = {"A3\Sounds_F\arsenal\weapons_static\Static_GMG\GMG_01", 1.12202, 1.0, 1200};
				begin2[] = {"A3\Sounds_F\arsenal\weapons_static\Static_GMG\GMG_02", 1.12202, 1.0, 1200};
				begin3[] = {"A3\Sounds_F\arsenal\weapons_static\Static_GMG\GMG_03", 1.12202, 1.0, 1200};
				soundBegin[] = {"begin1", 0.33, "begin2", 0.33, "begin3", 0.34};
			};
			burst = 5;
			aiRateOfFire = 4;
			aiRateOfFireDistance = 600;
			minRange = 200;
			minRangeProbab = 0.05;
			midRange = 500;
			midRangeProbab = 0.7;
			maxRange = 600;
			maxRangeProbab = 0.1;
		};
		
		class far : close {
	sounds[] = {"StandardSound"};
			class StandardSound {
				begin1[] = {"A3\Sounds_F\arsenal\weapons_static\Static_GMG\GMG_01", 1.12202, 1.0, 1200};
				begin2[] = {"A3\Sounds_F\arsenal\weapons_static\Static_GMG\GMG_02", 1.12202, 1.0, 1200};
				begin3[] = {"A3\Sounds_F\arsenal\weapons_static\Static_GMG\GMG_03", 1.12202, 1.0, 1200};
				soundBegin[] = {"begin1", 0.33, "begin2", 0.33, "begin3", 0.34};
			};
			burst = 4;
			aiRateOfFire = 5;
			aiRateOfFireDistance = 1000;
			minRange = 400;
			minRangeProbab = 0.05;
			midRange = 850;
			midRangeProbab = 0.4;
			maxRange = 1100;
			maxRangeProbab = 0.01;
		};
	
	class GunParticles {
			class effect1 {
				positionName = "usti hlavne2";
				directionName = "konec hlavne2";
				effectName = "MachineGun1";
			};
		};
	};
	
	class 20mm_cannon_FU_Commander : 20mm_cannon_FU_Hull {
		
	ballisticsComputer = 2 + 4 + 8;
				class GunParticles {
			class effect1 {
				positionName = "commander_gun_muzzle_pos";
				directionName = "commander_gun_muzzle_end";
				effectName = "MachineGun1";
			};
		};
	};
	
	
	class HMG_127_FU_Hull : HMG_127_APC {
	lockAcquire = 0;
	weaponLockDelay = 3; // Most of the guided weapons
	weaponLockSystem = 4; // Laser lock
	canLock = 2; // Always locks
	laserLock = 1;
	ballisticsComputer = 2 + 4 + 8;
	};
	
	class HMG_127_FU_Commander : HMG_127_MBT {
		lockAcquire = 0;
		weaponLockDelay = 3; // Most of the guided weapons
		weaponLockSystem = 4; // Laser lock
		canLock = 2; // Always locks
		laserLock = 1;
		ballisticsComputer = 2 + 4 + 8;
			};

	class GMG_20mm_FU_Commander : GMG_20mm {
		lockAcquire = 0;
		weaponLockDelay = 3; // Most of the guided weapons
		weaponLockSystem = 4; // Laser lock
		canLock = 2; // Always locks
		laserLock = 1;
		ballisticsComputer = 2 + 4 + 8;
		
				
				class GunParticles {
			class effect1 {
				positionName = "commander_gun_muzzle_pos";
				directionName = "commander_gun_muzzle_end";
				effectName = "GrenadeLauncherCloud";
			};
		};
	};
};
	
class CfgVehicles {
	class All {
		class Turrets;	// External class reference
	};
	
	class AllVehicles : All {
		class NewTurret {
			class Turrets;	// External class reference
		};
	};
	
	class Land : AllVehicles {};
	
	class LandVehicle : Land {
		class CommanderOptics;	// External class reference
	};
	
	class Car : LandVehicle {};
	
	class Tank : LandVehicle {
		class Turrets {
			class MainTurret : NewTurret {
				class Turrets {
					class CommanderOptics;	// External class reference
				};
			};
		};
	};
	
	class Tank_F : Tank {
		class Turrets {
			class MainTurret : NewTurret {
				class Turrets {
					class CommanderOptics : CommanderOptics {};
				};
			};
		};
	};
	
	
	class MBT_01_base_F : Tank_F {};
	
	class B_MBT_01_base_F : MBT_01_base_F {};
	
	class B_MBT_01_cannon_F : B_MBT_01_base_F {};
	
	class B_MBT_01_TUSK_F : B_MBT_01_cannon_F {};
	
	class B_MBT_01_FU_F : B_MBT_01_TUSK_F {
		enginePower = 1200;
		maxOmega = 300;
		peakTorque = 5000;
		engineLosses = 10;
		transmissionLosses = 5;
		displayName = "M2A6 Slammer FU";
		laserScanner = 1;
		class Turrets : Turrets {
			class MainTurret : MainTurret {
				showAllTargets = 2;
				
				class OpticsIn : Optics_Gunner_MBT_01 {
					class Wide : Wide {
							thermalMode[] = {0, 1};
							};
					
					class Medium : Medium {
							thermalMode[] = {0, 1};
							};
					
					class Narrow : Narrow {
							thermalMode[] = {0, 1};
							};
							
							class extra_zoom1 : Narrow {
							thermalMode[] = {0, 1};
							initFov = (30 * 0.05625 / 120);
							minFov = (30 * 0.05625 / 120);
							maxFov = (30 * 0.05625 / 120);
							};
							
							class extra_zoom2 : Narrow {
							thermalMode[] = {0, 1};
							initFov = (15 * 0.05625 / 120);
							minFov = (15 * 0.05625 / 120);
							maxFov = (15 * 0.05625 / 120);
							};
				};
				weapons[] = {"cannon_120mmFU", HMG_127_FU_Hull};
				magazines[] = {"32Rnd_120mm_APFSDS_shells", "16Rnd_120mm_HE_shells" , "500Rnd_127x99_mag_Tracer_Red", "500Rnd_127x99_mag_Tracer_Red", "500Rnd_127x99_mag_Tracer_Red"};
				
				class Turrets : Turrets {
					class CommanderOptics : CommanderOptics {
					showAllTargets = 2;
					
						class OpticsIn : Optics_Commander_01 {
							class ultra_Wide : Wide {
							initFov = (60 / 120);
							minFov = (60 / 120);
							maxFov = (60 / 120);
							thermalMode[] = {0, 1};
							};
							
							class Wide : Wide {
							thermalMode[] = {0, 1};
							};
							
							class Medium : Medium {
							thermalMode[] = {0, 1};
							};
							
							class Narrow : Narrow {
							thermalMode[] = {0, 1};
							};
							
							class extra_zoom1 : Narrow {
							thermalMode[] = {0, 1};
							initFov = (30 * 0.05625 / 120);
							minFov = (30 * 0.05625 / 120);
							maxFov = (30 * 0.05625 / 120);
							};
							
							class extra_zoom2 : Narrow {
							thermalMode[] = {0, 1};
							initFov = (15 * 0.05625 / 120);
							minFov = (15 * 0.05625 / 120);
							maxFov = (15 * 0.05625 / 120);
							};
						};
					weapons[] = {HMG_127_FU_Commander, "SmokeLauncher", "Laserdesignator_mounted"};
						magazines[] = {"500Rnd_127x99_mag_Tracer_Red", "500Rnd_127x99_mag_Tracer_Red", "SmokeLauncherMag", "Laserbatteries"};
						};
			};
		};
	};
	};
	
	
	class B_MBT_01_FUC_IT_F : B_MBT_01_TUSK_F {
		enginePower = 1200;
		maxOmega = 300;
		peakTorque = 5000;
		engineLosses = 10;
		transmissionLosses = 5;
		displayName = "M2A7 Slammer FUC IT";
		laserScanner = 1;
		class Turrets : Turrets {
			class MainTurret : MainTurret {
				showAllTargets = 2;
				
				class OpticsIn : Optics_Gunner_MBT_01 {
					class Wide : Wide {
							thermalMode[] = {0, 1};
							};
					
					class Medium : Medium {
							thermalMode[] = {0, 1};
							};
					
					class Narrow : Narrow {
							thermalMode[] = {0, 1};
							};
							
							class extra_zoom1 : Narrow {
							thermalMode[] = {0, 1};
							initFov = (30 * 0.05625 / 120);
							minFov = (30 * 0.05625 / 120);
							maxFov = (30 * 0.05625 / 120);
							};
							
							class extra_zoom2 : Narrow {
							thermalMode[] = {0, 1};
							initFov = (15 * 0.05625 / 120);
							minFov = (15 * 0.05625 / 120);
							maxFov = (15 * 0.05625 / 120);
							};
				};
				weapons[] = {"cannon_120mmFU", "20mm_cannon_FU_Hull"};
				magazines[] = {"32Rnd_120mm_APFSDS_shells", "16Rnd_120mm_HE_shells", "300Rnd_20mm_shells", "300Rnd_20mm_shells", "300Rnd_20mm_shells"};
				
				class Turrets : Turrets {
					class CommanderOptics : CommanderOptics {
					showAllTargets = 2;
					
						class OpticsIn : Optics_Commander_01 {
							class ultra_Wide : Wide {
							initFov = (60 / 120);
							minFov = (60 / 120);
							maxFov = (60 / 120);
							thermalMode[] = {0, 1};
							};
							
							class Wide : Wide {
							thermalMode[] = {0, 1};
							};
							
							class Medium : Medium {
							thermalMode[] = {0, 1};
							};
							
							class Narrow : Narrow {
							thermalMode[] = {0, 1};
							};
							
							class extra_zoom1 : Narrow {
							thermalMode[] = {0, 1};
							initFov = (30 * 0.05625 / 120);
							minFov = (30 * 0.05625 / 120);
							maxFov = (30 * 0.05625 / 120);
							};
							
							class extra_zoom2 : Narrow {
							thermalMode[] = {0, 1};
							initFov = (15 * 0.05625 / 120);
							minFov = (15 * 0.05625 / 120);
							maxFov = (15 * 0.05625 / 120);
							};
						};
					weapons[] = {"GMG_20mm_FU_Commander", "SmokeLauncher", "Laserdesignator_mounted",};
						magazines[] = {"200Rnd_20mm_g_belt", "200Rnd_20mm_g_belt", "SmokeLauncherMag", "Laserbatteries"};
						};
			};
		};
	};
	};
	
		class B_MBT_01_FUC_AS_F : B_MBT_01_TUSK_F {
		enginePower = 1200;
		maxOmega = 300;
		peakTorque = 5000;
		engineLosses = 10;
		transmissionLosses = 5;
		displayName = "M2A8 Slammer FUC AS";
		laserScanner = 1;
		class Turrets : Turrets {
			class MainTurret : MainTurret {
				showAllTargets = 2;
				
				class OpticsIn : Optics_Gunner_MBT_01 {
					class Wide : Wide {
							thermalMode[] = {0, 1};
							};
					
					class Medium : Medium {
							thermalMode[] = {0, 1};
							};
					
					class Narrow : Narrow {
							thermalMode[] = {0, 1};
							};
							
							class extra_zoom1 : Narrow {
							thermalMode[] = {0, 1};
							initFov = (30 * 0.05625 / 120);
							minFov = (30 * 0.05625 / 120);
							maxFov = (30 * 0.05625 / 120);
							};
							
							class extra_zoom2 : Narrow {
							thermalMode[] = {0, 1};
							initFov = (15 * 0.05625 / 120);
							minFov = (15 * 0.05625 / 120);
							maxFov = (15 * 0.05625 / 120);
							};
				};
				weapons[] = {"cannon_120mmFU", "20mm_cannon_FU_Hull"};
				magazines[] = {"32Rnd_120mm_APFSDS_shells", "16Rnd_120mm_HE_shells", "300Rnd_20mm_shells", "300Rnd_20mm_shells", "300Rnd_20mm_shells"};
				
				class Turrets : Turrets {
					class CommanderOptics : CommanderOptics {
					showAllTargets = 2;
					
						class OpticsIn : Optics_Commander_01 {
							class ultra_Wide : Wide {
							initFov = (60 / 120);
							minFov = (60 / 120);
							maxFov = (60 / 120);
							thermalMode[] = {0, 1};
							};
							
							class Wide : Wide {
							thermalMode[] = {0, 1};
							};
							
							class Medium : Medium {
							thermalMode[] = {0, 1};
							};
							
							class Narrow : Narrow {
							thermalMode[] = {0, 1};
							};
							
							class extra_zoom1 : Narrow {
							thermalMode[] = {0, 1};
							initFov = (30 * 0.05625 / 120);
							minFov = (30 * 0.05625 / 120);
							maxFov = (30 * 0.05625 / 120);
							};
							
							class extra_zoom2 : Narrow {
							thermalMode[] = {0, 1};
							initFov = (15 * 0.05625 / 120);
							minFov = (15 * 0.05625 / 120);
							maxFov = (15 * 0.05625 / 120);
							};
						};
					weapons[] = {"20mm_cannon_FU_Commander", "SmokeLauncher", "Laserdesignator_mounted"};
						magazines[] = {"300Rnd_20mm_shells", "300Rnd_20mm_shells", "SmokeLauncherMag", "Laserbatteries"};
						};
			};
		};
	};
	};
	
		class B_MBT_01_FUC_140 : B_MBT_01_TUSK_F {
		transportSoldier = 0;
		engineLosses = 10;
		transmissionLosses = 5;
		displayName = "M2A9 Slammer FUC 140";
		laserScanner = 1;
		class Turrets : Turrets {
			class MainTurret : MainTurret {
				showAllTargets = 2;
				
				class OpticsIn : Optics_Gunner_MBT_01 {
					class Wide : Wide {
							thermalMode[] = {0, 1};
							};
					
					class Medium : Medium {
							thermalMode[] = {0, 1};
							};
					
					class Narrow : Narrow {
							thermalMode[] = {0, 1};
							};
							
							class extra_zoom1 : Narrow {
							thermalMode[] = {0, 1};
							initFov = (30 * 0.05625 / 120);
							minFov = (30 * 0.05625 / 120);
							maxFov = (30 * 0.05625 / 120);
							};
							
							class extra_zoom2 : Narrow {
							thermalMode[] = {0, 1};
							initFov = (15 * 0.05625 / 120);
							minFov = (15 * 0.05625 / 120);
							maxFov = (15 * 0.05625 / 120);
							};
				};
				weapons[] = {"cannon_140mmFU", "20mm_cannon_FU_Hull"};
				magazines[] = {"4Rnd_140mm_APFSDS_shells", "4Rnd_140mm_APFSDS_shells", "4Rnd_140mm_APFSDS_shells", "4Rnd_140mm_APFSDS_shells", "4Rnd_140mm_APFSDS_shells", "4Rnd_140mm_APFSDS_shells", "4Rnd_140mm_APFSDS_shells", "4Rnd_140mm_APFSDS_shells", "4Rnd_140mm_HE_shells", "4Rnd_140mm_HE_shells", "4Rnd_140mm_HE_shells", "4Rnd_140mm_HE_shells", "4Rnd_140mm_HET_shells", "4Rnd_140mm_HET_shells", "4Rnd_140mm_HET_shells", "4Rnd_140mm_HET_shells", "300Rnd_20mm_shells", "300Rnd_20mm_shells", "300Rnd_20mm_shells", "300Rnd_20mm_shells", "300Rnd_20mm_shells"};
				
				class Turrets : Turrets {
					class CommanderOptics : CommanderOptics {
					showAllTargets = 2;
					
						class OpticsIn : Optics_Commander_01 {
							class ultra_Wide : Wide {
							initFov = (60 / 120);
							minFov = (60 / 120);
							maxFov = (60 / 120);
							thermalMode[] = {0, 1};
							};
							
							class Wide : Wide {
							thermalMode[] = {0, 1};
							};
							
							class Medium : Medium {
							thermalMode[] = {0, 1};
							};
							
							class Narrow : Narrow {
							thermalMode[] = {0, 1};
							};
							
							class extra_zoom1 : Narrow {
							thermalMode[] = {0, 1};
							initFov = (30 * 0.05625 / 120);
							minFov = (30 * 0.05625 / 120);
							maxFov = (30 * 0.05625 / 120);
							};
							
							class extra_zoom2 : Narrow {
							thermalMode[] = {0, 1};
							initFov = (15 * 0.05625 / 120);
							minFov = (15 * 0.05625 / 120);
							maxFov = (15 * 0.05625 / 120);
							};
						};
					weapons[] = {"20mm_cannon_FU_Commander", "SmokeLauncher", "Laserdesignator_mounted"};
						magazines[] = {"300Rnd_20mm_shells", "300Rnd_20mm_shells", "300Rnd_20mm_shells", "300Rnd_20mm_shells", "SmokeLauncherMag", "Laserbatteries"};
						};
			};
		};
	};
	};
};