class CfgPatches
{
	class V_ACE3_HLC_Mag_Compat
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Weapons_F","hlcweapons_core","ace_common","hlcweapons_g3","hlcweapons_ar15","ace_ballistics"};
		version = "1";
		projectName = "IEU Compatibility";
		author = "V. Zeitsev";
	};
};

#include "CfgWeapons.hpp"