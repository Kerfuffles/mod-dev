class CfgWeapons {
    class Rifle_Base_F;
    class Rifle_Short_Base_F: Rifle_Base_F {};
    class Rifle_Long_Base_F: Rifle_Base_F {};

    /* Long Rifles */

    class DMR_06_base_F: Rifle_Long_Base_F {
       
    };
    class DMR_03_base_F: Rifle_Long_Base_F {
        
    };
    class EBR_base_F: Rifle_Long_Base_F {
       
    };


    /* SPAR */
    class arifle_SPAR_01_base_F: Rifle_Base_F {
        magazines[] += {
            "hlc_30rnd_556x45_EPR",
			"hlc_30rnd_556x45_SOST",
			"hlc_30rnd_556x45_SPR",
			"hlc_30rnd_556x45_S",
			"hlc_30rnd_556x45_MDim",
			"hlc_30rnd_556x45_TDim",
			"hlc_50rnd_556x45_EPR"
        };
    };
    class arifle_SPAR_03_base_F: Rifle_Base_F {
        magazines[] += {
            "hlc_20rnd_762x51_b_G3",
			"hlc_20rnd_762x51_Mk316_G3",
			"hlc_20rnd_762x51_barrier_G3",
			"hlc_20rnd_762x51_T_G3",
			"hlc_20rnd_762x51_MDim_G3",
			"hlc_50rnd_762x51_M_G3",
			"hlc_50rnd_762x51_MDIM_G3",
			"hlc_20rnd_762x51_IRDim_G3",
			"hlc_20rnd_762x51_S_G3"
			
        };
    };

    /* Other */
    class LMG_Mk200_F: Rifle_Long_Base_F {
        magazines[] = {
			"100Rnd_65x39_caseless_mag_Tracer",
			"100Rnd_65x39_caseless_mag",
			"30Rnd_65x39_caseless_mag",
			"30Rnd_65x39_caseless_mag_Tracer",
            "200Rnd_65x39_cased_Box",
            "200Rnd_65x39_cased_Box_Tracer",
            "ACE_200Rnd_65x39_cased_Box_Tracer_Dim",
			"ACE_100Rnd_65x39_caseless_mag_Tracer_Dim",
			"ACE_30Rnd_65x39_caseless_mag_Tracer_Dim",
			"ACE_30Rnd_65x47_Scenar_mag",
			"ACE_30Rnd_65_Creedmor_mag"
        };
    };
    class Tavor_base_F: Rifle_Base_F {};
    class mk20_base_F: Rifle_Base_F {};

    /* SMGs/Rifles */
    class SDAR_base_F: Rifle_Base_F {
      
    };
    class pdw2000_base_F: Rifle_Short_Base_F {};
    class SMG_01_Base: Rifle_Short_Base_F {};
    class SMG_02_base_F: Rifle_Base_F {};

    class arifle_SDAR_F: SDAR_base_F {
        magazines[] += {
            "hlc_30rnd_556x45_EPR",
			"hlc_30rnd_556x45_SOST",
			"hlc_30rnd_556x45_SPR",
			"hlc_30rnd_556x45_S",
			"hlc_30rnd_556x45_MDim",
			"hlc_30rnd_556x45_TDim",
			"hlc_50rnd_556x45_EPR"
        };
    };
    class arifle_TRG20_F: Tavor_base_F {
        magazines[] += {
            "hlc_30rnd_556x45_EPR",
			"hlc_30rnd_556x45_SOST",
			"hlc_30rnd_556x45_SPR",
			"hlc_30rnd_556x45_S",
			"hlc_30rnd_556x45_MDim",
			"hlc_30rnd_556x45_TDim",
			"hlc_50rnd_556x45_EPR"
        };
    };
    class arifle_TRG21_F: Tavor_base_F {
        magazines[] += {
            "hlc_30rnd_556x45_EPR",
			"hlc_30rnd_556x45_SOST",
			"hlc_30rnd_556x45_SPR",
			"hlc_30rnd_556x45_S",
			"hlc_30rnd_556x45_MDim",
			"hlc_30rnd_556x45_TDim",
			"hlc_50rnd_556x45_EPR"
        };
    };
    class arifle_TRG21_GL_F: arifle_TRG21_F {
        magazines[] += {
			"hlc_30rnd_556x45_EPR",
			"hlc_30rnd_556x45_SOST",
			"hlc_30rnd_556x45_SPR",
			"hlc_30rnd_556x45_S",
			"hlc_30rnd_556x45_MDim",
			"hlc_30rnd_556x45_TDim",
			"hlc_50rnd_556x45_EPR"
        };
    };
    class arifle_Mk20_F: mk20_base_F {
        magazines[] += {
			"hlc_30rnd_556x45_EPR",
			"hlc_30rnd_556x45_SOST",
			"hlc_30rnd_556x45_SPR",
			"hlc_30rnd_556x45_S",
			"hlc_30rnd_556x45_MDim",
			"hlc_30rnd_556x45_TDim",
			"hlc_50rnd_556x45_EPR"
        };
    };
    class arifle_Mk20C_F: mk20_base_F {
        magazines[] += {
            "hlc_30rnd_556x45_EPR",
			"hlc_30rnd_556x45_SOST",
			"hlc_30rnd_556x45_SPR",
			"hlc_30rnd_556x45_S",
			"hlc_30rnd_556x45_MDim",
			"hlc_30rnd_556x45_TDim",
			"hlc_50rnd_556x45_EPR"
        };
    };
    class arifle_Mk20_GL_F: mk20_base_F {
        magazines[] += {
            "hlc_30rnd_556x45_EPR",
			"hlc_30rnd_556x45_SOST",
			"hlc_30rnd_556x45_SPR",
			"hlc_30rnd_556x45_S",
			"hlc_30rnd_556x45_MDim",
			"hlc_30rnd_556x45_TDim",
			"hlc_50rnd_556x45_EPR"
        };
    };
	/*DMR*/
    class srifle_EBR_F: EBR_base_F {
        magazines[] += {
            "hlc_20rnd_762x51_b_G3",
			"hlc_20rnd_762x51_Mk316_G3",
			"hlc_20rnd_762x51_barrier_G3",
			"hlc_20rnd_762x51_T_G3",
			"hlc_20rnd_762x51_MDim_G3",
			"hlc_50rnd_762x51_M_G3",
			"hlc_50rnd_762x51_MDIM_G3",
			"hlc_20rnd_762x51_IRDim_G3",
			"hlc_20rnd_762x51_S_G3"
        };
    };
    class srifle_DMR_03_F: DMR_03_base_F {
        magazines[] += {
            "hlc_20rnd_762x51_b_G3",
			"hlc_20rnd_762x51_Mk316_G3",
			"hlc_20rnd_762x51_barrier_G3",
			"hlc_20rnd_762x51_T_G3",
			"hlc_20rnd_762x51_MDim_G3",
			"hlc_50rnd_762x51_M_G3",
			"hlc_50rnd_762x51_MDIM_G3",
			"hlc_20rnd_762x51_IRDim_G3",
			"hlc_20rnd_762x51_S_G3"
        };
    };
    class srifle_DMR_06_camo_F: DMR_06_base_F {
        magazines[] += {
           "hlc_20rnd_762x51_b_G3",
			"hlc_20rnd_762x51_Mk316_G3",
			"hlc_20rnd_762x51_barrier_G3",
			"hlc_20rnd_762x51_T_G3",
			"hlc_20rnd_762x51_MDim_G3",
			"hlc_50rnd_762x51_M_G3",
			"hlc_50rnd_762x51_MDIM_G3",
			"hlc_20rnd_762x51_IRDim_G3",
			"hlc_20rnd_762x51_S_G3"
        };
    };
};