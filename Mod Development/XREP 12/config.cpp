class CfgPatches
{
	class IEU_XREP_12
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Weapons_F", "A3_Data_F"};
		version = "1";
		projectName = "IEU XREP";
		author = "V. Zeitsev";
	};
};

class CfgFunctions
{
	class IEU
	{
		class IEU_Functions
		{
			class XREP {file = "functions\fnc_XREP.sqf";};
		};
	};
};

#include "CfgWeapons.hpp"
#include "CfgAmmo.hpp"