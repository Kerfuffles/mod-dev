_target = _this select 0;
_damage = 0;


if(isPlayer _target) then 
{
	[-2,{
		_target = _this select 0;
		if(player == _target) then 
		{
			[_target, true, 15, true] call ace_medical_fnc_setUnconscious;
		};
	},[_target]] call CBA_fnc_globalExecute;
	 
} else { // note that this damage won't be applied to AI group members of a player-led team on a server
	[_target, true, 15, true] call ace_medical_fnc_setUnconscious;
};