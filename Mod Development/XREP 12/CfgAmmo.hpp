class CfgAmmo
{
	class B_12Gauge_Slug;
	class IEU_XREP_12: B_12Gauge_Slug
	{
		hit = call IEU_fnc_XREP;
		indirectHit = 0;
		indirectHitRange = 0;
		caliber = 0.2113;
		typicalSpeed = 538.62;
		airFriction = -0.0035;
	};
};	