CfgMagazines
{
	class CA_Magazine;
	class IEU_5Rnd_XREP: CA_Magazine
	{
		scope = 2;
		author = "IEU";
		displayName = "5Rd XREP 12 Ga.";
		descriptionshort = "Caliber: 12 gauge<br />Rounds: 5<br />Used in: M590A1";
		picture = "\rhsusf\addons\rhsusf_weapons\icons\m_12g_slug.paa";
		model = "\rhsusf\addons\rhsusf_weapons\magazines\12g\12g_slug";
		ammo = "IEU_XREP_12";
		count = 5;
		mass = 5;
		initSpeed = 418;
	};
	class IEU_8Rnd_XREP: rhsusf_5Rnd_Slug
	{
		author = "IEU";
		displayName = "8Rd XREP 12 Ga.";
		descriptionshort = "Caliber: 12 gauge<br />Rounds: 8<br />Used in: M590A1";
		count = 8;
		mass = 8;
		initSpeed = 512;
	};
};