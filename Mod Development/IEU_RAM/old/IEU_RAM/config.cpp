////////////////////////////////////////////////////////////////////
//DeRap: Produced from mikero's Dos Tools Dll version 5.24
//Produced on Thu Sep 15 02:09:32 2016 : Created on Thu Sep 15 02:09:32 2016
//http://dev-heaven.net/projects/list_files/mikero-pbodll
////////////////////////////////////////////////////////////////////

#define _ARMA_

//Class IEU_RAM : config.bin{
class CfgPatches
{
	class rhsusf_c_heavyweapons
	{
		units[] = {};
		weapons[] = {"rhs_weap_m256","rhs_weap_m284","RHS_weap_M242BC","RHS_weap_M119","Rhs_weap_TOW_Launcher","Rhs_weap_TOW_Launcher_static","RHS_M2_Abrams_Commander","RHS_M2_Abrams_Gunner","RHS_M2","RHS_M2_M1117","rhs_weap_m240veh","rhs_weap_m240_abrams","rhs_weap_m240_m113","rhs_weap_m240_abrams_coax","rhs_weap_m134_minigun_1","rhs_weap_m134_minigun_2","rhs_weap_M197","rhs_weap_M230","RHS_weap_gau8","RHS_MK19","rhsusf_weap_M259","rhsusf_weap_CMFlareLauncher","rhs_weap_MASTERSAFE","rhs_weap_MASTERSAFE_Holdster15","RHS_MKV_M2_p","RHS_MKV_M2_s","RHS_MKV_MK19"};
		requiredVersion = 1.32;
		requiredAddons[] = {"A3_Weapons_F","A3_Weapons_F_gamma","A3_Weapons_F_epc","A3_Static_F_Gamma","A3_CargoPoses_F"};
		version = "0.1.1.1";
		magazines[] = {"rhs_mag_M1069","rhs_mag_M416","rhs_mag_M1028","rhs_mag_M829","rhs_mag_M829A1","rhs_mag_M829A2","rhs_mag_M829A3","rhs_mag_M830","rhs_mag_M830A1","rhs_mag_230Rnd_25mm_M242_HEI","rhs_mag_1100Rnd_762x51_M240","rhs_mag_762x51_M240_1200","RHS_mag_m1_he_12","rhs_mag_m314_ilum_4","rhs_mag_m60a2_smoke_4","rhs_mag_M197_750","rhs_mag_762x51_M240","rhs_mag_762x51_M240_200","rhs_mag_30x113mm_M789_HEDP_1200","rhs_mag_30x113mm_M789_HEDP_laser_1200","RHS_48Rnd_40mm_MK19","RHS_48Rnd_40mm_MK19_M430I","RHS_48Rnd_40mm_MK19_M1001","RHS_96Rnd_40mm_MK19","RHS_96Rnd_40mm_MK19_M430I","RHS_96Rnd_40mm_MK19_M1001","rhs_mag_100rnd_127x99_mag","rhs_mag_100rnd_127x99_mag_Tracer_Red","rhs_mag_100rnd_127x99_mag_Tracer_Green","rhs_mag_100rnd_127x99_mag_Tracer_Yellow","rhs_mag_200rnd_127x99_mag","rhs_mag_200rnd_127x99_mag_Tracer_Red","rhs_mag_2Rnd_TOW","rhs_mag_TOW2a","rhsusf_mag_L8A3_8"};
		ammo[] = {"rhsusf_ammo_basic_penetrator_base","rhsusf_ammo_basic_penetrator","rhs_ammo_spall","rhs_ammo_flechettes","rhs_ammo_flechettes_m1028","rhs_ammo_M1069","rhs_ammo_M416","rhs_ammo_M1028","rhs_ammo_M829","rhs_ammo_M829A1","rhs_ammo_M829A2","rhs_ammo_M829A3","rhs_ammo_M830","rhs_ammo_M830_penetrator","rhs_ammo_M830A1","rhs_ammo_M830A1_penetrator","RHS_ammo_M919_APFSDS","RHS_ammo_M792_HEI","rhs_ammo_mk19m3_M384","rhs_ammo_mk19m3_M430I","rhs_ammo_mk19m3_M430I_penetrator","rhs_ammo_mk19m3_M1001","rhs_ammo_m1_he","rhs_ammo_m314_ilum","rhs_ammo_m60a2_smoke","rhs_ammo_PGU14B_API","rhs_ammo_30x113mm_M789_HEDP","rhs_ammo_30x113mm_M789_HEDP_laser","rhs_ammo_20mm_AP","rhs_ammo_127x99_Ball","rhs_ammo_127x99_Ball_Tracer_Red","rhs_ammo_127x99_Ball_Tracer_Green","rhs_ammo_127x99_Ball_Tracer_Yellow","rhs_ammo_127x99_SLAP","rhs_ammo_127x99_SLAP_Tracer_Red","rhs_ammo_127x99_SLAP_Tracer_Green","rhs_ammo_127x99_SLAP_Tracer_Yellow","rhs_ammo_TOW_AT","rhs_ammo_ammo_TOW_AT_penetrator","rhs_ammo_TOW2_AT","rhs_ammo_TOW2_AT_static","rhs_ammo_TOW2_AT_penetrator","rhsusf_ammo_L8A3","rhsusf_ammo_L8A3_shell"};
	};
};
class CfgAmmo
{
	class ShellBase;
	class AIS_HEAT_JET: ShellBase
	{
		airFriction = -0.1;
		indirectHit = 20;
		indirectHitRange = 2;
	};
	class Sh_125mm_APFSDS;
	class rhsusf_ammo_basic_penetrator_base: Sh_125mm_APFSDS
	{
		rhs_ce_penetration = "penetrator";
		hit = 290;
		indirectHit = 0;
		indirectHitRange = 0;
		typicalspeed = 1000;
		explosive = 0;
		caliber = 0.1;
		timeToLive = 0.01;
		simulationStep = 0.001;
		whistleOnFire = 1;
		airFriction = -0.1;
		whistleDist = 14;
		deflecting = 0;
		cost = 500;
		muzzleEffect = "";
		model = "\A3\Weapons_f\empty";
	};
	class rhsusf_ammo_basic_penetrator: rhsusf_ammo_basic_penetrator_base
	{
		rhs_ce_penetration = "penetrator";
		caliber = "(420/((15*1000)/1000))";
		indirectHit = 0;
		indirectHitRange = 0;
		explosive = 0;
		typicalSpeed = 1000;
		whistleOnFire = 1;
		whistleDist = 14;
		deflecting = 0;
		model = "\A3\Weapons_f\empty";
	};
	class Sh_120mm_APFSDS: ShellBase
	{
		simulation = "shotBullet";
	};
	class Sh_120mm_HE;
	class B_30mm_APFSDS_Tracer_Red;
	class B_30mm_HE;
	class BulletBase;
	class SubmunitionBase;
	class rhs_ammo_spall: BulletBase
	{
		hit = 20;
		indirectHit = 0;
		indirectHitRange = 0.15;
		caliber = "(50/((15*200)/1000))";
		typicalSpeed = 200;
		deflecting = 90;
	};
	class rhs_ammo_flechettes: BulletBase
	{
		hit = 4;
		indirectHit = 0;
		indirectHitRange = 0.15;
		typicalSpeed = 200;
		deflecting = 50;
	};
	class rhs_ammo_flechettes_m1028: rhs_ammo_flechettes
	{
		typicalSpeed = 1000;
		airFriction = -0.00045;
	};
	class rhs_ammo_M1069: Sh_120mm_HE
	{
		hit = 350;
		indirectHit = 45;
		indirectHitRange = 7;
		typicalSpeed = 1000;
		explosive = 0.8;
		cost = 300;
		airFriction = -0.00045;
		caliber = 12;
		timeToLive = 15;
		whistleDist = 14;
		tracerScale = 2;
		tracerStartTime = 0.1;
		tracerEndTime = 2.3;
		model = "\A3\Weapons_f\Data\bullettracer\shell_tracer_red";
	};
	class rhs_ammo_M416: rhs_ammo_M1069
	{
		caliber = "(220/((15*900)/1000))";
		airFriction = -4.5e-013;
	};
	class rhs_ammo_M1028: rhs_ammo_M1069
	{
		simulation = "shotSubmunitions";
		submunitionAmmo = "rhs_ammo_flechettes_m1028";
		submunitionConeType[] = {"randomcenter",143};
		submunitionConeAngle = "0.009 * 120";
		triggerTime = 0.01;
	};
	class rhs_ammo_M829: Sh_120mm_APFSDS
	{
		hit = 236.17;
		indirectHit = 11;
		indirectHitRange = 0.15;
		typicalSpeed = 1670;
		deflecting = 6;
		airFriction = -4e-005;
		caliber = "(611/((15*1670)/1000))";
		timeToLive = 15;
		whistleOnFire = 1;
		whistleDist = 14;
		tracerScale = 2;
		tracerStartTime = 0.1;
		tracerEndTime = 2.3;
		model = "\A3\Weapons_f\Data\bullettracer\shell_tracer_red";
		simulationStep = 0.01;
	};
	class rhs_ammo_M829A1: rhs_ammo_M829
	{
		hit = 229.59;
		typicalSpeed = 1575;
		caliber = "(647/((15*1575)/1000))";
	};
	class rhs_ammo_M829A2: rhs_ammo_M829
	{
		hit = 244.9;
		typicalSpeed = 1680;
		caliber = "(769/((15*1680)/1000))";
	};
	class rhs_ammo_M829A3: rhs_ammo_M829
	{
		hit = 245.87;
		typicalSpeed = 1555;
		caliber = "(812/((15*1555)/1000))";
	};
	class rhs_ammo_M830: Sh_120mm_APFSDS
	{
		explosive = 0.4;
		caliber = 0.1;
		hit = 250;
		indirectHit = 11;
		indirectHitRange = 0.15;
		typicalSpeed = 1140;
		deflecting = 0;
		model = "\A3\Weapons_f\Data\bullettracer\shell_tracer_red";
		ais_ce_penetrators[] = {"rhs_ammo_M830_penetrator"};
	};
	class rhs_ammo_M830_penetrator: rhsusf_ammo_basic_penetrator
	{
		caliber = "(600/((15*1000)/1000))";
	};
	class rhs_ammo_M830A1: rhs_ammo_M830
	{
		explosive = 0.8;
		indirectHit = 14;
		indirectHitRange = 2.15;
		ais_ce_penetrators[] = {"rhs_ammo_M830A1_penetrator"};
	};
	class rhs_ammo_M830A1_penetrator: rhsusf_ammo_basic_penetrator
	{
		caliber = "(450/((15*1000)/1000))";
	};
	class RHS_ammo_M919_APFSDS: B_30mm_APFSDS_Tracer_Red
	{
		hit = 90;
		caliber = "(85/((15*1420)/1000))";
		typicalspeed = 1385;
		maxSpeed = 1385;
		airfriction = -0.0002;
	};
	class RHS_ammo_M792_HEI: B_30mm_HE
	{
		timeToLive = 7.5;
		airfriction = -0.00056;
		typicalspeed = 1100;
		maxSpeed = 1100;
		model = "\A3\Weapons_f\Data\bullettracer\shell_tracer_red";
	};
	class G_40mm_HE;
	class rhs_ammo_mk19m3_M384: G_40mm_HE
	{
		hit = 40;
		airFriction = -0.0008;
	};
	class rhs_ammo_mk19m3_M430I: rhs_ammo_mk19m3_M384
	{
		hit = 35;
		ais_ce_penetrators[] = {"rhs_ammo_mk19m3_M430I_penetrator"};
	};
	class rhs_ammo_mk19m3_M430I_penetrator: rhsusf_ammo_basic_penetrator
	{
		hit = 140;
		caliber = "(32/((15*1000)/1000))";
	};
	class rhs_ammo_mk19m3_M1001: rhs_ammo_mk19m3_M384
	{
		simulation = "shotSubmunitions";
		submunitionAmmo = "rhs_ammo_flechettes";
		submunitionConeType[] = {"randomcenter",56};
		submunitionConeAngle = "0.009 * 160";
		triggerTime = 0.01;
	};
	class Sh_155mm_AMOS;
	class Smoke_120mm_AMOS_White;
	class Flare_82mm_AMOS_White;
	class rhs_ammo_m1_he: Sh_155mm_AMOS
	{
		cost = 100;
		artilleryLock = 1;
		hit = 110;
		indirectHit = 75;
		indirectHitRange = 21;
		timetolive = 220;
		class CamShakeExplode
		{
			power = "(105*0.2)*10";
			duration = "((round (105^0.5))*0.2 max 0.2)";
			frequency = 20;
			distance = "((30 + 105^0.5))";
		};
		class CamShakeHit
		{
			power = "105 * 10";
			duration = "((round (105^0.25))*0.2 max 0.2)";
			frequency = 20;
			distance = 1;
		};
		class CamShakeFire
		{
			power = "(105^0.25)*10";
			duration = "((round (105^0.5))*0.2 max 0.2)";
			frequency = 20;
			distance = "((105^0.5))";
		};
	};
	class rhs_ammo_m314_ilum: Flare_82mm_AMOS_White
	{
		hit = 8;
		indirectHit = 0;
		indirectHitRange = 0;
		explosive = 0;
		ExplosionEffects = "";
		soundHit[] = {"",0,1};
		CraterEffects = "";
		whistleDist = 0;
		intensity = "10000 * 6";
		timeToLive = 90;
	};
	class rhs_ammo_m60a2_smoke: Smoke_120mm_AMOS_White
	{
		hit = 8;
		indirectHit = 0;
		indirectHitRange = 0;
		explosive = 0.1;
		ExplosionEffects = "";
		soundHit[] = {"",0,1};
		CraterEffects = "";
		whistleDist = 0;
	};
	class Gatling_30mm_HE_Plane_CAS_01_F;
	class rhs_ammo_PGU14B_API: Gatling_30mm_HE_Plane_CAS_01_F
	{
		timeToLive = 16;
		caliber = 5.5;
	};
	class B_20mm;
	class rhs_ammo_30x113mm_M789_HEDP: B_30mm_HE
	{
		allowagainstinfantry = 1;
		hit = 37;
		indirectHit = 15;
		indirectHitRange = 3;
		model = "\A3\Weapons_f\Data\bullettracer\tracer_red";
		tracerScale = 2;
		tracerStartTime = 0.1;
		tracerEndTime = 2.3;
		explosive = 0.4;
		caliber = 2.5;
		airFriction = -0.00078;
		timeToLive = 12;
	};
	class rhs_ammo_30x113mm_M789_HEDP_laser: rhs_ammo_30x113mm_M789_HEDP
	{
		laserLock = 1;
	};
	class rhs_ammo_20mm_AP: B_20mm
	{
		hit = 45;
		indirectHit = 10;
		indirectHitRange = 2;
		visibleFire = 28;
		audibleFire = 28;
		visibleFireTime = 3;
		cost = 25;
		airLock = 1;
		explosive = 0.3;
		model = "\A3\Weapons_f\Data\bullettracer\tracer_red";
		tracerScale = 1.8;
		tracerStartTime = 0.1;
		tracerEndTime = 2;
		airFriction = -0.00077;
		caliber = 2.33;
		timeToLive = 12;
	};
	class B_127x99_Ball;
	class rhs_ammo_127x99_Ball: B_127x99_Ball
	{
		caliber = "(31/((15*880)/1000))";
	};
	class rhs_ammo_127x99_Ball_Tracer_Red: rhs_ammo_127x99_Ball
	{
		model = "\A3\Weapons_f\Data\bullettracer\tracer_red";
	};
	class rhs_ammo_127x99_Ball_Tracer_Green: rhs_ammo_127x99_Ball
	{
		model = "\A3\Weapons_f\Data\bullettracer\tracer_green";
	};
	class rhs_ammo_127x99_Ball_Tracer_Yellow: rhs_ammo_127x99_Ball
	{
		model = "\A3\Weapons_f\Data\bullettracer\tracer_yellow";
	};
	class B_127x99_SLAP;
	class rhs_ammo_127x99_SLAP: B_127x99_SLAP
	{
		caliber = "(51/((15*1210)/1000))";
	};
	class rhs_ammo_127x99_SLAP_Tracer_Red: rhs_ammo_127x99_SLAP
	{
		model = "\A3\Weapons_f\Data\bullettracer\tracer_red";
	};
	class rhs_ammo_127x99_SLAP_Tracer_Green: rhs_ammo_127x99_SLAP
	{
		model = "\A3\Weapons_f\Data\bullettracer\tracer_green";
	};
	class rhs_ammo_127x99_SLAP_Tracer_Yellow: rhs_ammo_127x99_SLAP
	{
		model = "\A3\Weapons_f\Data\bullettracer\tracer_yellow";
	};
	class M_Titan_AT;
	class rhs_ammo_TOW_AT: M_Titan_AT
	{
		ais_ce_penetrators[] = {"rhs_ammo_TOW_AT_penetrator"};
		hit = 480;
		indirectHit = 12;
		indirectHitRange = 1.2;
		cost = 800;
		irLock = 1;
		manualControl = 1;
		trackOversteer = 0.95;
		trackLead = 0.9;
		timeToLive = 9;
		maneuvrability = 10;
		simulationStep = 0.005;
		sideAirFriction = 0.05;
		maxControlRange = 3750;
		maxSpeed = 296;
		initTime = 0.151;
		thrustTime = 1.45;
		thrust = 300;
		deflecting = 0;
		fuseDistance = 5;
		effectsMissile = "missile2";
		whistleDist = 2;
		lockType = 1;
		rhs_muzzleEffect = "RHSUSF_fnc_firedSaclos";
		rhs_saclos = 1;
	};
	class rhs_ammo_ammo_TOW_AT_penetrator: rhsusf_ammo_basic_penetrator
	{
		hit = 290;
		caliber = "(430/((15*1000)/1000))";
	};
	class rhs_ammo_TOW2_AT: rhs_ammo_TOW_AT
	{
		ais_ce_penetrators[] = {"rhs_ammo_TOW2_AT_penetrator"};
		model = "\rhsusf\addons\rhsusf_heavyweapons\atgm\TOW2A";
		hit = 450;
		indirectHit = 20;
		indirectHitRange = 2;
		maxSpeed = 329;
		thrust = 334;
		whistleDist = 4;
	};
	class rhs_ammo_TOW2_AT_static: rhs_ammo_TOW2_AT
	{
		initTime = 0.15;
	};
	class rhs_ammo_TOW2_AT_penetrator: rhsusf_ammo_basic_penetrator
	{
		hit = 300;
		caliber = "(900/((15*1000)/1000))";
	};
	class SmokeLauncherAmmo;
	class rhsusf_ammo_L8A3: SmokeLauncherAmmo
	{
		rhs_smokeShell = "rhsusf_ammo_L8A3_shell";
		muzzleEffect = "";
		rhs_muzzleEffect = "RHSUSF_fnc_effectFiredSmokeLauncher";
		weaponLockSystem = "1";
		simulation = "shotCM";
		AIAmmoUsageFlags = "4 + 8";
		hit = 0;
		indirectHit = 0;
		indirectHitRange = 0;
	};
	class Grenade;
	class GrenadeHand;
	class rhsusf_ammo_L8A3_shell: Grenade
	{
		simulation = "shotShell";
		AIAmmoUsageFlags = "4";
		hit = 3;
		indirectHit = 1;
		indirectHitRange = 2;
		suppressionRadiusHit = 15;
		typicalspeed = 50;
		airFriction = -0.012;
		model = "rhsusf\addons\rhsusf_heavyweapons\galix\galix_proj";
		visibleFireTime = 1;
		fuseDistance = 0;
		soundHit1[] = {"A3\Sounds_F\arsenal\explosives\grenades\Explosion_HE_grenade_01",1.5,1,750};
		soundHit2[] = {"A3\Sounds_F\arsenal\explosives\grenades\Explosion_HE_grenade_02",1.5,1,750};
		soundHit3[] = {"A3\Sounds_F\arsenal\explosives\grenades\Explosion_HE_grenade_03",1.5,1,750};
		soundHit4[] = {"A3\Sounds_F\arsenal\explosives\grenades\Explosion_HE_grenade_04",1.5,1,750};
		multiSoundHit[] = {"soundHit1",0.25,"soundHit2",0.25,"soundHit3",0.25,"soundHit4",0.25};
		explosionEffects = "RHSUSF_CM_L8A3_Effect";
		CraterEffects = "EmptyEffect";
		explosionTime = 0.7;
		timeToLive = 1;
		coefGravity = 2.5;
	};
};
class CfgMagazines
{
	class VehicleMagazine;
	class rhs_mag_M1069: VehicleMagazine
	{
		scope = 2;
		displayName = "M1069 HE-FRAG";
		displayNameShort = "M1069";
		ammo = "rhs_ammo_M1069";
		count = 8;
		initSpeed = 1410;
		maxLeadSpeed = 100;
		nameSound = "heat";
		tracersEvery = 1;
	};
	class rhs_mag_M416: rhs_mag_M1069
	{
		displayName = "M416 Smoke";
		displayNameShort = "M416";
		ammo = "rhs_ammo_M416";
		initSpeed = 732;
	};
	class rhs_mag_M1028: rhs_mag_M1069
	{
		displayName = "M1028 Canister";
		displayNameShort = "M1028";
		ammo = "rhs_ammo_M1028";
		initSpeed = 1410;
	};
	class rhs_mag_M829: rhs_mag_M1069
	{
		displayName = "M829 APFSDS-T";
		displayNameShort = "M829";
		ammo = "rhs_ammo_M829";
		count = 28;
		initSpeed = 1670;
		nameSound = "sabot";
	};
	class rhs_mag_M829A1: rhs_mag_M829
	{
		displayName = "M829A1 APFSDS-T";
		displayNameShort = "M829A1";
		ammo = "rhs_ammo_M829A1";
		count = 28;
		initSpeed = 1575;
	};
	class rhs_mag_M829A2: rhs_mag_M829
	{
		displayName = "M829A2 APFSDS-T";
		displayNameShort = "M829A2";
		ammo = "rhs_ammo_M829A2";
		count = 28;
		initSpeed = 1680;
	};
	class rhs_mag_M829A3: rhs_mag_M829
	{
		displayName = "M829A3 APFSDS-T";
		displayNameShort = "M829A3";
		ammo = "rhs_ammo_M829A3";
		count = 28;
		initSpeed = 1555;
	};
	class rhs_mag_M830: rhs_mag_M1069
	{
		displayName = "M830 HEAT-FS";
		displayNameShort = "M830";
		ammo = "rhs_ammo_M830";
		initSpeed = 915;
	};
	class rhs_mag_M830A1: rhs_mag_M830
	{
		displayName = "M830A1 MPAT";
		displayNameShort = "M830A1";
		ammo = "rhs_ammo_M830A1";
		count = 8;
		initSpeed = 1440;
	};
	class 60Rnd_30mm_APFSDS_shells_Tracer_RED;
	class rhs_mag_70Rnd_25mm_M242_APFSDS: 60Rnd_30mm_APFSDS_shells_Tracer_RED
	{
		ammo = "RHS_ammo_M919_APFSDS";
		count = 70;
		displayname = "25x137mm M919 APFSDS-T";
		displaynameshort = " M919 APFSDS-T";
		initspeed = 1385;
	};
	class 140Rnd_30mm_MP_shells_Tracer_RED;
	class rhs_mag_230Rnd_25mm_M242_HEI: 140Rnd_30mm_MP_shells_Tracer_RED
	{
		scope = 2;
		ammo = "RHS_ammo_M792_HEI";
		count = 230;
		displayname = "25x137mm M792 HEI-T";
		displaynameshort = "M792 HEI-T";
		initspeed = 1100;
		lastroundstracer = 4;
		maxleadspeed = 300;
		namesound = "cannon";
		tracersevery = 1;
	};
	class rhs_mag_1100Rnd_762x51_M240: VehicleMagazine
	{
		scope = 2;
		ammo = "B_762x51_Tracer_Red";
		displayname = "7.62x51mm Ball";
		initspeed = 838;
		maxleadspeed = 200;
		count = 1100;
		namesound = "mgun";
		tracersevery = 4;
	};
	class rhs_mag_762x51_M240_1200: rhs_mag_1100Rnd_762x51_M240
	{
		count = 1200;
	};
	class 32Rnd_155mm_Mo_shells;
	class RHS_mag_m1_he_12: 32Rnd_155mm_Mo_shells
	{
		scope = 2;
		count = 12;
		displayname = "He-FRAG M1";
		displaynameshort = "He-FRAG";
		nameSound = "heat";
		ammo = "rhs_ammo_m1_he";
	};
	class rhs_mag_m314_ilum_4: RHS_mag_m1_he_12
	{
		count = 4;
		displayname = "Illumination M314";
		displaynameshort = "Illumination";
		ammo = "rhs_ammo_m314_ilum";
	};
	class rhs_mag_m60a2_smoke_4: RHS_mag_m1_he_12
	{
		count = 4;
		displayname = "Smoke M60A2";
		displaynameshort = "Smoke";
		ammo = "rhs_ammo_m60a2_smoke";
	};
	class 1000Rnd_Gatling_30mm_Plane_CAS_01_F;
	class rhs_mag_1000Rnd_30x173: 1000Rnd_Gatling_30mm_Plane_CAS_01_F
	{
		ammo = "rhs_ammo_PGU14B_API";
		tracersEvery = 0;
	};
	class rhs_mag_M197_750: VehicleMagazine
	{
		scope = 2;
		displayname = "20x102mm PGU-28/B SAPHEI-T";
		displaynameshort = "SAPHEI-T";
		ammo = "rhs_ammo_20mm_AP";
		count = 750;
		initSpeed = 1036;
		maxLeadSpeed = 200;
		tracersEvery = 0;
		nameSound = "cannon";
	};
	class rhs_mag_762x51_M240: VehicleMagazine
	{
		scope = 1;
		displayName = "100rnd M240";
		count = 100;
		ammo = "rhs_ammo_762x51_M993_Ball";
		initSpeed = 910;
		tracersEvery = 4;
		lastRoundsTracer = 4;
		nameSound = "mgun";
		descriptionShort = "762x51 ball";
	};
	class rhs_mag_762x51_M240_200: rhs_mag_762x51_M240
	{
		count = 200;
	};
	class rhs_mag_30x113mm_M789_HEDP_1200: VehicleMagazine
	{
		scope = 1;
		displayName = "30x113mm M789 HEDP-T";
		ammo = "rhs_ammo_30x113mm_M789_HEDP";
		count = 1200;
		initSpeed = 805;
		tracersEvery = 0;
		maxLeadSpeed = 500;
		nameSound = "cannon";
		airLock = 1;
	};
	class rhs_mag_30x113mm_M789_HEDP_laser_1200: rhs_mag_30x113mm_M789_HEDP_1200
	{
		ammo = "rhs_ammo_30x113mm_M789_HEDP_laser";
	};
	class RHS_48Rnd_40mm_MK19: VehicleMagazine
	{
		scope = 2;
		displayName = "Mk. 19 40mm M384 HE";
		ammo = "rhs_ammo_mk19m3_M384";
		count = 48;
		initSpeed = 240;
		maxLeadSpeed = 100;
		nameSound = "grenadelauncher";
		displaynameshort = "HE";
	};
	class RHS_48Rnd_40mm_MK19_M430I: RHS_48Rnd_40mm_MK19
	{
		displayName = "Mk. 19 40mm M430I HEDP";
		displaynameshort = "HEDP";
		ammo = "rhs_ammo_mk19m3_M430I";
	};
	class RHS_48Rnd_40mm_MK19_M1001: RHS_48Rnd_40mm_MK19
	{
		displayName = "Mk. 19 40mm M1001 Carnister";
		displaynameshort = "Carnister";
		ammo = "rhs_ammo_mk19m3_M1001";
	};
	class RHS_96Rnd_40mm_MK19: RHS_48Rnd_40mm_MK19
	{
		count = 96;
	};
	class RHS_96Rnd_40mm_MK19_M430I: RHS_48Rnd_40mm_MK19_M430I
	{
		count = 96;
	};
	class RHS_96Rnd_40mm_MK19_M1001: RHS_48Rnd_40mm_MK19_M1001
	{
		count = 96;
	};
	class rhs_mag_100rnd_127x99_mag: VehicleMagazine
	{
		scope = 2;
		ammo = "rhs_ammo_127x99_Ball";
		initSpeed = 880;
		maxLeadSpeed = 200;
		tracersEvery = 4;
		nameSound = "mgun";
		count = 100;
		displayName = "12.7mm M2 HMG Belt";
		descriptionShort = "Caliber: 12.7x99 mm NATO<br/>Rounds: 100<br />Used in: Mounted M2";
		displaynameshort = "12.7x99mm Ball";
	};
	class rhs_mag_100rnd_127x99_mag_Tracer_Red: rhs_mag_100rnd_127x99_mag
	{
		ammo = "rhs_ammo_127x99_Ball_Tracer_Red";
		displayName = "12.7mm M2 HMG Tracer (Red) Belt";
		descriptionShort = "Caliber: 12.7x99 mm Tracer - Red<br/>Rounds: 100<br />Used in: Mounted M2";
		displaynameshort = "Tracer Red";
	};
	class rhs_mag_100rnd_127x99_mag_Tracer_Green: rhs_mag_100rnd_127x99_mag_Tracer_Red
	{
		ammo = "rhs_ammo_127x99_Ball_Tracer_Green";
		displayName = "12.7mm M2 HMG Tracer (Green) Belt";
		descriptionShort = "Caliber: 12.7x99 mm Tracer - Green<br/>Rounds: 100<br />Used in: Mounted M2";
		displaynameshort = "Tracer Green";
	};
	class rhs_mag_100rnd_127x99_mag_Tracer_Yellow: rhs_mag_100rnd_127x99_mag_Tracer_Red
	{
		ammo = "rhs_ammo_127x99_Ball_Tracer_Yellow";
		displayName = "12.7mm M2 HMG Tracer (Yellow) Belt";
		descriptionShort = "Caliber: 12.7x99 mm Tracer - Yellow<br/>Rounds: 100<br />Used in: Mounted M2";
		displaynameshort = "Tracer Yellow";
	};
	class rhs_mag_200rnd_127x99_mag: rhs_mag_100rnd_127x99_mag
	{
		scope = 2;
		count = 200;
		descriptionShort = "Caliber: 12.7x99 mm NATO<br/>Rounds: 200<br />Used in: Mounted M2";
	};
	class rhs_mag_200rnd_127x99_mag_Tracer_Red: rhs_mag_100rnd_127x99_mag_Tracer_Red
	{
		scope = 2;
		count = 200;
		descriptionShort = "Caliber: 12.7x99 mm Tracer - Red<br/>Rounds: 200<br />Used in: Mounted M2";
	};
	class rhs_mag_2Rnd_TOW: VehicleMagazine
	{
		scope = 2;
		ammo = "rhs_ammo_TOW2_AT";
		displayname = "BGM-71E TOW-2A";
		displaynameshort = "BGM-71E";
		nameSound = "missiles";
		count = 2;
		initSpeed = 55.71;
	};
	class rhs_mag_TOW2a: rhs_mag_2Rnd_TOW
	{
		count = 1;
		ammo = "rhs_ammo_TOW2_AT_static";
	};
	class SmokeLauncherMag;
	class rhsusf_mag_L8A3_8: SmokeLauncherMag
	{
		count = 8;
		scope = 2;
		ammo = "rhsusf_ammo_L8A3";
		initSpeed = 50;
	};
};
class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;
class CfgWeapons
{
	class Default;
	class rhs_weap_MASTERSAFE: Default
	{
		scope = 1;
		displayName = "MASTERSAFE";
		descriptionShort = "MASTERSAFE";
		showToPlayer = 0;
		modes[] = {"this"};
		magazines[] = {};
		minRange = 0;
		minRangeProbab = 0.001;
		midRange = 0.001;
		midRangeProbab = 0.001;
		maxRange = 0.001;
		maxRangeProbab = 0.001;
	};
	class rhs_weap_MASTERSAFE_Holdster15: rhs_weap_MASTERSAFE
	{
		holdsterAnimValue = 1.502;
	};
	class CannonCore;
	class cannon_120mm: CannonCore
	{
		class player;
		class close;
		class short;
		class medium;
		class far;
	};
	class rhs_weap_m256: cannon_120mm
	{
		canLock = 0;
		scope = 1;
		nameSound = "cannon";
		aidispersioncoefy = 0.125;
		airateoffire = 7;
		airateoffiredistance = 250;
		dispersion = 0.00025;
		showaimcursorinternal = 0;
		cursor = "EmptyCursor";
		cursoraim = "EmptyCursor";
		cursoraimon = "EmptyCursor";
		displayname = "120mm M256A1";
		ballisticsComputer = 1;
		flash = "gunfire";
		flashSize = 10;
		minRange = 1;
		minRangeProbab = 0.1;
		midRange = 2500;
		midRangeProbab = 0.7;
		maxRange = 4000;
		maxRangeProbab = 0.1;
		maxLeadSpeed = 100;
		reloadTime = 17;
		magazineReloadTime = 12;
		autoReload = 1;
		magazines[] = {"rhs_mag_M829A3","rhs_mag_M829A2","rhs_mag_M829A1","rhs_mag_M829","rhs_mag_M830","rhs_mag_M830A1","rhs_mag_M1069","rhs_mag_M1028","rhs_mag_M416"};
		class player: player
		{
			reloadTime = 11;
		};
		class close: close
		{
			reloadTime = 11;
			dispersion = "0.00025*3.1";
		};
		class short: short
		{
			reloadTime = 11;
			dispersion = "0.00025*3.1";
		};
		class medium: medium
		{
			reloadTime = 11;
			dispersion = "0.00025*3.1";
		};
		class far: far
		{
			reloadTime = 11;
			dispersion = "0.00025*3.1";
		};
	};
	class mortar_155mm_AMOS;
	class cannon_155mm: mortar_155mm_AMOS
	{
		class Single1;
		class Single2;
		class Single3;
		class Single4;
		class Single5;
		class Burst1;
		class Burst2;
		class Burst3;
		class Burst4;
		class Burst5;
	};
	class rhs_weap_m284: cannon_155mm
	{
		scope = 1;
		displayName = "$STR_A3_mortar_120mm_AMOS0";
		nameSound = "cannon";
		cursor = "mortar";
		cursorAim = "EmptyCursor";
		sounds[] = {"StandardSound"};
		class StandardSound
		{
			weaponSoundEffect = "DefaultRifle";
			begin1[] = {"rhsusf\addons\rhsusf_sounds\155mm\155mm_1",3.3,1,3000};
			begin2[] = {"rhsusf\addons\rhsusf_sounds\155mm\155mm_2",3.3,1,3000};
			soundBegin[] = {"begin1",0.5,"begin2",0.5};
		};
		reloadSound[] = {"A3\Sounds_F\vehicles\armor\noises\reload_tank_cannon_2",31.622776,1,15};
		minRange = 10;
		minRangeProbab = 0.7;
		midRange = 1800;
		midRangeProbab = 0.7;
		maxRange = 3000;
		maxRangeProbab = 0.1;
		reloadTime = 6;
		magazineReloadTime = 7;
		autoReload = 1;
		canLock = 0;
		magazines[] = {"32Rnd_155mm_Mo_shells","6Rnd_155mm_Mo_smoke","2Rnd_155mm_Mo_guided","2Rnd_155mm_Mo_LG","6Rnd_155mm_Mo_mine","2Rnd_155mm_Mo_Cluster","6Rnd_155mm_Mo_AT_mine"};
		modes[] = {"Single1","Single2","Single3","Single4","Single5","Burst1","Burst2","Burst3","Burst4","Burst5"};
		class Single1: Single1{};
		class Single2: Single2{};
		class Single3: Single3{};
		class Single4: Single4{};
		class Single5: Single5{};
		class Burst1: Burst1{};
		class Burst2: Burst2{};
		class Burst3: Burst3{};
		class Burst4: Burst4{};
		class Burst5: Burst5{};
	};
	class autocannon_Base_F;
	class autocannon_30mm_CTWS: autocannon_Base_F
	{
		class HE: autocannon_Base_F
		{
			class player;
			class close;
			class short;
			class medium;
			class far;
		};
		class AP: autocannon_Base_F
		{
			class player;
			class close;
			class short;
			class medium;
			class far;
		};
	};
	class RHS_weap_M242BC: autocannon_30mm_CTWS
	{
		aidispersioncoefx = "3*2";
		aidispersioncoefy = "3*2";
		airateoffire = 2;
		airateoffiredistance = 1000;
		class GunParticles
		{
			class Effect
			{
				effectName = "AutoCannonFired";
				positionName = "Usti hlavne";
				directionName = "Konec hlavne";
			};
			class Shell
			{
				positionName = "shell_eject_pos";
				directionName = "shell_eject_dir";
				effectName = "RHS_HeavyGunCartridge1";
			};
		};
		displayName = "25mm M242";
		muzzles[] = {"AP","HE"};
		class HE: HE
		{
			displayName = "25mm M242";
			magazines[] = {"rhs_mag_230Rnd_25mm_M242_HEI","rhs_mag_70Rnd_25mm_M242_APFSDS"};
			modes[] = {"single","100rpm","player","close","short","medium","far"};
			class player: player
			{
				reloadTime = "60/200";
				textureType = "fullAuto";
			};
			class 100rpm: player
			{
				reloadTime = "60/100";
				textureType = "burst";
			};
			class single: player
			{
				autofire = 0;
				textureType = "semi";
			};
		};
		class AP: AP
		{
			displayName = "25mm M242 - M919 APFSDS-T";
			magazines[] = {"rhs_mag_70Rnd_25mm_M242_APFSDS"};
			showToPlayer = 0;
			class player: player
			{
				showToPlayer = 0;
				reloadTime = "60/200";
			};
			class close: close
			{
				showToPlayer = 0;
				reloadTime = "60/200";
			};
			class short: short
			{
				showToPlayer = 0;
				reloadTime = "60/200";
			};
			class medium: medium
			{
				showToPlayer = 0;
				reloadTime = "60/200";
			};
			class far: far
			{
				showToPlayer = 0;
				reloadTime = "60/100";
			};
		};
	};
	class RHS_weap_M119: mortar_155mm_AMOS
	{
		displayName = "$STR_RHS_DN_M119";
		ballisticscomputer = 2;
		reloadTime = 6;
		magazineReloadTime = 6;
		class GunParticles
		{
			class Effect1
			{
				directionname = "Konec hlavne";
				effectname = "ArtilleryFired1";
				positionname = "Usti hlavne";
			};
		};
		modes[] = {"Single1","Single2","Single3","Burst1","Burst2","Burst3"};
		magazines[] = {"RHS_mag_m1_he_12","rhs_mag_m314_ilum_4","rhs_mag_m60a2_smoke_4"};
	};
	class HMG_M2;
	class Gatling_30mm_Plane_CAS_01_F: CannonCore
	{
		class LowROF;
	};
	class RHS_M2_Abrams_Commander: HMG_M2
	{
		canLock = 0;
		initspeed = 0;
		magazines[] = {"rhs_mag_100rnd_127x99_mag","rhs_mag_100rnd_127x99_mag_Tracer_Red","rhs_mag_100rnd_127x99_mag_Tracer_Green","rhs_mag_100rnd_127x99_mag_Tracer_Yellow"};
		class GunParticles
		{
			class effect1
			{
				positionname = "usti hlavne3";
				directionname = "konec hlavne3";
				effectname = "MachineGunCloud";
			};
			class effect2
			{
				positionname = "nabojnicestart";
				directionname = "nabojniceend";
				effectname = "MachineGunEject";
			};
			class effect3
			{
				positionname = "nabojnicestart";
				directionname = "nabojniceend";
				effectname = "MachineGunCartridge2";
			};
			class RHSUSF_BarrelRefract
			{
				positionName = "usti hlavne3";
				directionName = "usti hlavne3";
				effectName = "RHSUSF_BarrelRefractHeavy";
			};
		};
	};
	class RHS_M2_Abrams_Gunner: HMG_M2
	{
		canLock = 0;
		initspeed = 0;
		magazines[] = {"rhs_mag_100rnd_127x99_mag","rhs_mag_100rnd_127x99_mag_Tracer_Red","rhs_mag_100rnd_127x99_mag_Tracer_Green","rhs_mag_100rnd_127x99_mag_Tracer_Yellow"};
		class GunParticles
		{
			class effect1
			{
				positionname = "usti hlavne4";
				directionname = "konec hlavne4";
				effectname = "MachineGunCloud";
			};
			class effect2
			{
				positionname = "nabojnicestart2";
				directionname = "nabojniceend2";
				effectname = "MachineGunEject";
			};
			class effect3
			{
				positionname = "nabojnicestart2";
				directionname = "nabojniceend2";
				effectname = "MachineGunCartridge2";
			};
			class RHSUSF_BarrelRefract
			{
				positionName = "usti hlavne4";
				directionName = "usti hlavne4";
				effectName = "RHSUSF_BarrelRefractHeavy";
			};
		};
	};
	class RHS_M2: HMG_M2
	{
		canLock = 0;
		initspeed = 0;
		cursor = "EmptyCursor";
		cursoraim = "EmptyCursor";
		cursoraimon = "EmptyCursor";
		magazines[] = {"rhs_mag_100rnd_127x99_mag","rhs_mag_100rnd_127x99_mag_Tracer_Red","rhs_mag_100rnd_127x99_mag_Tracer_Green","rhs_mag_100rnd_127x99_mag_Tracer_Yellow","rhs_mag_200rnd_127x99_mag","rhs_mag_200rnd_127x99_mag_Tracer_Red"};
		class GunParticles
		{
			class effect1
			{
				positionname = "usti hlavne";
				directionname = "konec hlavne";
				effectname = "MachineGunCloud";
			};
			class effect2
			{
				positionname = "nabojnicestart";
				directionname = "nabojniceend";
				effectname = "MachineGunEject";
			};
			class effect3
			{
				positionname = "nabojnicestart";
				directionname = "nabojniceend";
				effectname = "MachineGunCartridge2";
			};
			class RHSUSF_BarrelRefract
			{
				positionName = "usti hlavne";
				directionName = "usti hlavne";
				effectName = "RHSUSF_BarrelRefractHeavy";
			};
		};
	};
	class RHS_M2_M1117: RHS_M2
	{
		class GunParticles
		{
			class effect1
			{
				positionname = "kulas";
				directionname = "ZaslehKulas";
				effectname = "MachineGunCloud";
			};
			class RHSUSF_BarrelRefract: effect1
			{
				effectName = "RHSUSF_BarrelRefractHeavy";
			};
		};
	};
	class RHS_MKV_M2_p: RHS_M2
	{
		class GunParticles
		{
			class effect1
			{
				positionName = "m2_p_muzzle";
				directionName = "m2_p_endBarrel";
				effectName = "MachineGunCloud";
			};
			class effect2
			{
				positionName = "m2_p_caseStart";
				directionName = "m2_p_caseEnd";
				effectName = "MachineGunEject";
			};
			class effect3
			{
				positionName = "m2_p_caseStart";
				directionName = "m2_p_caseEnd";
				effectName = "MachineGunCartridge2";
			};
		};
		scope = 1;
		selectionfireanim = "m2_p_flash";
		displayName = "Port M2";
	};
	class RHS_MKV_M2_s: RHS_MKV_M2_p
	{
		displayName = "Stbd M2";
		class GunParticles
		{
			class effect1
			{
				positionName = "m2_s_muzzle";
				directionName = "m2_s_endBarrel";
				effectName = "MachineGunCloud";
			};
			class effect2
			{
				positionName = "m2_s_caseStart";
				directionName = "m2_s_caseEnd";
				effectName = "MachineGunEject";
			};
			class effect3
			{
				positionName = "m2_s_caseStart";
				directionName = "m2_s_caseEnd";
				effectName = "MachineGunCartridge2";
			};
		};
		selectionfireanim = "m2_s_flash";
	};
	class LMG_M200;
	class rhs_weap_m240veh: LMG_M200
	{
		canLock = 0;
		initspeed = 0;
		cursor = "EmptyCursor";
		cursoraim = "EmptyCursor";
		cursoraimon = "EmptyCursor";
		scope = 1;
		aidispersioncoefx = "7*2";
		aidispersioncoefy = "8*2";
		displayName = "7.62 M240";
		displaynameshort = "M240";
		class manual;
		class close;
		class short;
		class medium;
		class far;
		magazines[] = {"rhs_mag_1100Rnd_762x51_M240","rhs_mag_762x51_M240_1200","rhs_mag_762x51_M240_200"};
		magazineReloadTime = 12;
		class GunParticles
		{
			class effect1
			{
				directionname = "kulas";
				positionname = "kulas start";
				effectname = "MachineGunCloud";
			};
		};
	};
	class rhs_weap_m240_abrams: rhs_weap_m240veh
	{
		class GunParticles
		{
			class effect1
			{
				positionname = "usti hlavne5";
				directionname = "konec hlavne5";
				effectname = "MachineGunCloud";
			};
			class RHSUSF_BarrelRefract
			{
				positionname = "usti hlavne5";
				directionname = "konec hlavne5";
				effectName = "RHSUSF_BarrelRefractHeavy";
			};
		};
	};
	class rhs_weap_m240_m113: rhs_weap_m240veh
	{
		class GunParticles
		{
			class M240_AmmoBeltLinkEject
			{
				positionName = "ammobeltlinks_start";
				directionName = "ammobeltlinks_end";
				effectName = "MachineGunEject2";
			};
			class M240_AmmoBeltCaseEject
			{
				positionName = "shelleject_start";
				directionName = "shelleject_end";
				effectName = "RHSUSF_762Cartridge";
			};
			class M240_WhiteGas
			{
				positionName = "usti hlavne";
				directionName = "konec hlavne";
				effectName = "MachineGunCloud";
			};
			class M240_RHSUSF_BarrelRefract
			{
				positionName = "usti hlavne";
				directionName = "usti hlavne up";
				effectName = "RHSUSF_BarrelRefractHeavy";
			};
		};
	};
	class rhs_weap_m240_abrams_coax: rhs_weap_m240veh
	{
		class GunParticles
		{
			class effect1
			{
				positionname = "usti hlavne2";
				directionname = "konec hlavne2";
				effectname = "MachineGunCloud";
			};
		};
	};
	class M134_minigun;
	class rhs_weap_m134_minigun_1: M134_minigun
	{
		displayName = "M134 Minigun 7.62mm";
		class GunParticles
		{
			class FirstEffect
			{
				effectName = "MachineGun1";
				positionName = "muzzle_1";
				directionName = "chamber_1";
			};
			class effect1
			{
				positionName = "machinegun_eject_pos";
				directionName = "machinegun_eject_dir";
				effectName = "MachineGunCartridge";
			};
			class RHSUSF_BarrelRefract
			{
				positionName = "muzzle_1";
				directionName = "muzzle_1";
				effectName = "RHSUSF_BarrelRefractHeavy";
			};
		};
	};
	class rhs_weap_m134_minigun_2: rhs_weap_m134_minigun_1
	{
		class GunParticles
		{
			class SecondEffect
			{
				effectName = "MachineGun1";
				positionName = "muzzle_2";
				directionName = "chamber_2";
			};
			class effect2
			{
				positionName = "machinegun_eject_2_pos";
				directionName = "machinegun_eject_2_dir";
				effectName = "MachineGunCartridge";
			};
			class RHSUSF_BarrelRefract
			{
				positionName = "muzzle_2";
				directionName = "muzzle_2";
				effectName = "RHSUSF_BarrelRefractHeavy";
			};
		};
	};
	class RHS_MKV_M134: rhs_weap_m134_minigun_1
	{
		magazines[] = {"2000Rnd_762x51_Belt_T_Red"};
		displayName = "Port M134";
		class gunParticles
		{
			class effect1
			{
				positionName = "m134_p_caseStart";
				directionName = "m134_p_caseEnd";
				effectName = "MachineGunCartridge1";
			};
			class effect2
			{
				positionName = "m134_p_caseStart";
				directionName = "m134_p_caseEnd";
				effectName = "MachineGun1";
			};
		};
	};
	class gatling_30mm;
	class rhs_weap_M197: gatling_30mm
	{
		scope = 1;
		cursor = "mg";
		cursorAim = "EmptyCursor";
		cursorSize = 0;
		class GunParticles
		{
			class Effect
			{
				effectName = "MachineGun2";
				positionName = "machinegun";
				directionName = "machinegun_end";
			};
		};
		displayName = "M197";
		ballisticsComputer = 1;
		autoFire = 1;
		nameSound = "cannon";
		magazines[] = {"rhs_mag_M197_750"};
		canLock = 2;
		modes[] = {"manual","close","short","medium","far"};
		class manual: Mode_FullAuto
		{
			displayName = "M197";
			autoFire = 1;
			sounds[] = {"StandardSound"};
			class StandardSound
			{
				weaponSoundEffect = "DefaultRifle";
				begin1[] = {"\rhsusf\addons\rhsusf_a2port_air\data\sounds\a10vulcanVII",3.1622777,1,1100};
				soundBegin[] = {"begin1",1};
			};
			reloadTime = 0.06;
			dispersion = 0.00025;
			showToPlayer = 1;
			burst = 1;
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 50;
			minRange = 1;
			minRangeProbab = 0.01;
			midRange = 2;
			midRangeProbab = 0.01;
			maxRange = 3;
			maxRangeProbab = 0.01;
		};
		class close: manual
		{
			showToPlayer = 0;
			soundBurst = 0;
			burst = 10;
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 50;
			minRange = 0;
			minRangeProbab = 0.35;
			midRange = 200;
			midRangeProbab = 0.88;
			maxRange = 500;
			maxRangeProbab = 0.34;
		};
		class short: close
		{
			burst = 8;
			aiRateOfFire = 2;
			aiRateOfFireDistance = 300;
			minRange = 300;
			minRangeProbab = 0.35;
			midRange = 600;
			midRangeProbab = 0.88;
			maxRange = 800;
			maxRangeProbab = 0.34;
		};
		class medium: close
		{
			burst = 4;
			aiRateOfFire = 3;
			aiRateOfFireDistance = 600;
			minRange = 700;
			minRangeProbab = 0.05;
			midRange = 900;
			midRangeProbab = 0.58;
			maxRange = 1000;
			maxRangeProbab = 0.04;
		};
		class far: close
		{
			burst = 5;
			aiRateOfFire = 5;
			aiRateOfFireDistance = 1000;
			minRange = 1000;
			minRangeProbab = 0.05;
			midRange = 1200;
			midRangeProbab = 0.4;
			maxRange = 1400;
			maxRangeProbab = 0.01;
		};
	};
	class rhs_weap_M230: rhs_weap_M197
	{
		displayName = "M230";
		magazines[] = {"rhs_mag_30x113mm_M789_HEDP_1200","rhs_mag_30x113mm_M789_HEDP_laser_1200"};
		canLock = 2;
		flash = "gunfire";
		flashSize = 0.1;
		recoil = "Empty";
		aiDispersionCoefX = 6;
		ffMagnitude = 0.5;
		ffFrequency = 11;
		ffCount = 6;
		modes[] = {"manual","close","short","medium","far"};
		class manual: Mode_FullAuto
		{
			displayName = "M230";
			autoFire = 1;
			sounds[] = {"StandardSound"};
			class StandardSound
			{
				weaponSoundEffect = "DefaultRifle";
				begin1[] = {"\rhsusf\addons\rhsusf_a2port_air\data\sounds\a10vulcanVII",3.1622777,1,1100};
				soundBegin[] = {"begin1",1};
			};
			reloadTime = 0.096;
			dispersion = 0.002;
			soundContinuous = 0;
			showToPlayer = 1;
			burst = 1;
			multiplier = 1;
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 50;
			minRange = 1;
			minRangeProbab = 0.001;
			midRange = 2;
			midRangeProbab = 0.001;
			maxRange = 3;
			maxRangeProbab = 0.001;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			ffCount = 6;
		};
		class close: manual
		{
			showToPlayer = 0;
			soundBurst = 0;
			burst = 15;
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 400;
			minRange = 0;
			minRangeProbab = 0.35;
			midRange = 200;
			midRangeProbab = 0.88;
			maxRange = 400;
			maxRangeProbab = 0.3;
		};
		class short: close
		{
			burst = 10;
			aiRateOfFire = 1;
			aiRateOfFireDistance = 600;
			minRange = 200;
			midRange = 500;
			maxRange = 800;
		};
		class medium: close
		{
			burst = 7;
			aiRateOfFire = 2;
			aiRateOfFireDistance = 800;
			minRange = 600;
			minRangeProbab = 0.35;
			midRange = 800;
			midRangeProbab = 0.78;
			maxRange = 1000;
			maxRangeProbab = 0.3;
		};
		class far: close
		{
			burst = 4;
			aiRateOfFire = 3;
			aiRateOfFireDistance = 1000;
			minRange = 800;
			minRangeProbab = 0.2;
			midRange = 1000;
			midRangeProbab = 0.6;
			maxRange = 1500;
			maxRangeProbab = 0.1;
		};
	};
	class RHS_weap_gau8: Gatling_30mm_Plane_CAS_01_F
	{
		displayName = "GAU-8";
		cursorSize = 0;
		cursor = "mg";
		cursorAim = "EmptyCursor";
		initspeed = 0;
		magazines[] = {"rhs_mag_1000Rnd_30x173"};
		modes[] = {"LowROF","HighROF","close","near","short","medium","far"};
		class LowROF: LowROF
		{
			class StandardSound;
			displayName = "Lo";
			textureType = "burst";
			reloadTime = 0.03;
		};
		class HighROF: LowROF
		{
			class StandardSound: StandardSound
			{
				begin1[] = {"A3\Sounds_F\arsenal\weapons_vehicles\gatling_30mm\30mm_01_burst",5.62341,1,1500,{12852,16079.5}};
				soundBegin[] = {"begin1",1};
			};
			displayName = "Hi";
			textureType = "fullAuto";
			reloadtime = 0.015;
		};
	};
	class GMG_F;
	class GMG_20mm;
	class RHS_MK19: GMG_20mm
	{
		class GunParticles
		{
			class effect1
			{
				positionName = "usti hlavne";
				directionName = "konec hlavne";
				effectName = "GrenadeLauncherCloud";
			};
		};
		autoreload = 1;
		canlock = 0;
		initspeed = 0;
		dispersion = 0.006;
		magazinereloadtime = 6;
		class manual: GMG_F
		{
			displayname = "Mk. 19 Grenade Launcher";
			sounds[] = {"StandardSound"};
			class StandardSound
			{
				begin1[] = {"A3\Sounds_F\weapons\GMG\GMG_1",1.12202,1,1200};
				begin2[] = {"A3\Sounds_F\weapons\GMG\GMG_2",1.12202,1,1200};
				begin3[] = {"A3\Sounds_F\weapons\GMG\GMG_3",1.12202,1,1200};
				soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.34};
				weaponSoundEffect = "DefaultRifle";
			};
		};
		reloadtime = 0.4;
		scope = 1;
		displayName = "Mk. 19";
		magazines[] = {"RHS_48Rnd_40mm_MK19","RHS_48Rnd_40mm_MK19_M430I","RHS_48Rnd_40mm_MK19_M1001","RHS_96Rnd_40mm_MK19","RHS_96Rnd_40mm_MK19_M430I","RHS_96Rnd_40mm_MK19_M1001"};
	};
	class RHS_MKV_MK19: RHS_MK19
	{
		scope = 1;
		displayName = "Stbd MK19";
		class GunParticles
		{
			class effect1
			{
				positionName = "mk19_s_muzzle";
				directionName = "mk19_s_endBarrel";
				effectName = "GrenadeLauncherCloud";
			};
		};
	};
	class missiles_titan;
	class Rhs_weap_TOW_Launcher: missiles_titan
	{
		holdsterAnimValue = 1;
		autoReload = 0;
		scope = 1;
		displayName = "BGM-71 TOW-2A";
		canLock = 0;
		cursor = "rocket";
		minRange = 10;
		minRangeProbab = 0.5;
		midRange = 1450;
		midRangeProbab = 1;
		maxRange = 3750;
		maxRangeProbab = 0.6;
		reloadTime = 2;
		magazineReloadTime = 90;
		magazines[] = {"rhs_mag_2Rnd_TOW"};
		aiRateOfFire = 5;
		aiRateOfFireDistance = 500;
	};
	class Rhs_weap_TOW_Launcher_static: missiles_titan
	{
		canLock = 0;
		autoReload = 0;
		magazineReloadTime = 15;
		displayName = "M41A4 TOW";
		magazines[] = {"rhs_mag_TOW2A"};
	};
	class SmokeLauncher;
	class rhsusf_weap_M259: SmokeLauncher
	{
		scope = 2;
		magazines[] = {"rhsusf_mag_L8A3_8"};
		reloadTime = 0.04;
		magazineReloadTime = 60;
		showToPlayer = 1;
		autoFire = 1;
		minRange = 0;
		maxRange = 10000;
		soundBurst = 0;
		burst = 8;
	};
	class CMFlareLauncher: SmokeLauncher
	{
		class Single;
		class Burst;
	};
	class rhsusf_weap_CMFlareLauncher: CMFlareLauncher
	{
		modes[] = {"Burst","AIBurst","Timed","AITimed"};
		class Timed: Single
		{
			displayName = "Timed";
			burst = 8;
			reloadTime = 1.3;
		};
		class AITimed: Timed
		{
			showToPlayer = 0;
			minRange = 0;
			maxRange = 3000;
		};
	};
};
class CfgFunctions
{
	class RHSUSF
	{
		tag = "RHSUSF";
		class Functions
		{
			class firedSaclos
			{
				file = "\rhsusf\addons\rhsusf_c_heavyweapons\functions\rhs_firedSaclos.sqf";
				description = "SACLOS laser notification for Shtora-1";
			};
		};
		class WeaponsEH
		{
			class effectFired
			{
				file = "\rhsusf\addons\rhsusf_c_heavyweapons\effects\rhs_effectFired.sqf";
				description = "RHS Effects handler";
			};
			class effectFiredSmokeLauncher
			{
				file = "\rhsusf\addons\rhsusf_c_heavyweapons\effects\rhs_effectFiredSmokeLauncher.sqf";
				description = "Effects for smoke launcher";
			};
		};
	};
};
class CfgCloudlets
{
	class Default;
	class HeavyGunCartridge1;
	class RHS_HeavyGunCartridge1: HeavyGunCartridge1
	{
		moveVelocity[] = {"-directionX * 3","- directionY * 3","- directionZ * 3"};
		particleShape = "\rhsusf\addons\rhsusf_c_heavyweapons\rhs_casing_30mm.p3d";
		size[] = {1};
	};
	class RHSUSF_CM_L8A3_Spark_Base: Default
	{
		interval = 0.002;
		color[] = {{1,0.3,0.1,-500},{1,0.3,0.1,-500}};
		colorCoef[] = {1,1,1,1};
		emissiveColor[] = {{250,50,25,0},{0,0,0,0}};
		lifeTime = 0.75;
		lifeTimeVar = 1.5;
		weight = 100;
		volume = 0.01;
		rubbing = 0.05;
		size[] = {0.15,0.075,0};
		sizeVar = 0.1;
		moveVelocity[] = {"inDirX * inSpeed * 0.35","inDirY * inSpeed * 0.35","inDirZ * inSpeed * 0.35"};
		moveVelocityVar[] = {11,11,11};
		bounceOnSurface = 0.25;
		bounceOnSurfaceVar = 0.1;
		circleRadius = 0;
		circleVelocity[] = {0,0,0};
		particleShape = "\A3\data_f\ParticleEffects\Universal\Universal";
		particleFSNtieth = 16;
		particleFSIndex = 13;
		particleFSFrameCount = 2;
		particleFSLoop = 0;
		angleVar = 360;
		animationName = "";
		particleType = "Billboard";
		timerPeriod = 3;
		rotationVelocity = 1;
		sizeCoef = 1;
		animationSpeed[] = {1000};
		animationSpeedCoef = 1;
		randomDirectionPeriod = 0;
		randomDirectionIntensity = 0;
		onTimerScript = "";
		beforeDestroyScript = "";
		blockAIVisibility = 0;
		position[] = {0,0,0};
		positionVar[] = {0.01,0.01,0.01};
		rotationVelocityVar = 0;
		colorVar[] = {0.05,0.05,0.05,5};
		randomDirectionPeriodVar = 0;
		randomDirectionIntensityVar = 0;
	};
	class RHSUSF_CM_L8A3_Spark_Tail: RHSUSF_CM_L8A3_Spark_Base
	{
		color[] = {{1,0.2,0.1,-500},{1,0.2,0.1,-500}};
		emissiveColor[] = {{5000,750,350,0},{0,0,0,0}};
		particleEffects = "RHSUSF_CM_L8A3_Spark_Tail_Subeffect";
	};
	class RHSUSF_CM_L8A3_Spark_Small: RHSUSF_CM_L8A3_Spark_Base
	{
		interval = 0.002;
		color[] = {{1,0.3,0.1,-500},{1,0.3,0.1,-500}};
		emissiveColor[] = {{250,50,25,0},{0,0,0,0}};
		lifeTime = 0.1;
		lifeTimeVar = 5;
		weight = 100;
		volume = 0.01;
		rubbing = 0.05;
		size[] = {0.2,0.1,0};
		sizeVar = 0.15;
		moveVelocity[] = {"inDirX * inSpeed * 0.25","inDirY * inSpeed * 0.25","inDirZ * inSpeed * 0.25"};
		moveVelocityVar[] = {15,15,15};
	};
	class RHSUSF_CM_L8A3_Smoke_Tail_Base: Default
	{
		circleRadius = 0;
		circleVelocity[] = {0,0,0};
		particleShape = "\A3\data_f\ParticleEffects\Universal\Universal";
		particleFSNtieth = 16;
		particleFSIndex = 12;
		particleFSFrameCount = 8;
		particleFSLoop = 1;
		angleVar = 1;
		animationName = "";
		particleType = "Billboard";
		timerPeriod = 1;
		moveVelocity[] = {0,0,0};
		sizeCoef = 1;
		colorCoef[] = {1,1,1,1};
		animationSpeed[] = {1};
		animationSpeedCoef = 1;
		onTimerScript = "";
		beforeDestroyScript = "";
		MoveVelocityVar[] = {0,0,0};
		emissiveColor[] = {{0,0,0,0}};
		colorVar[] = {0,0,0,0};
		lifeTime = 3;
		lifeTimeVar = 0.25;
		weight = 1.277;
		volume = "0.99 + randomValue / 40";
		rubbing = 0.075;
		sizeVar = 0.5;
		rotationVelocity = 0.75;
		rotationVelocityVar = 0.25;
		randomDirectionPeriod = 1.25;
		randomDirectionIntensity = 0.25;
		randomDirectionIntensityVar = 0.1;
		position[] = {0,0,0};
		positionVar[] = {0.1,0.1,0.1};
		blockAIVisibility = 0;
		color[] = {{0.6,0.6,0.6,0},{0.5,0.5,0.5,0.4},{0.5,0.5,0.5,0.5},{0.5,0.5,0.5,0.4},{0.5,0.5,0.5,0.3},{0.8,0.8,0.8,0.05},{1,1,1,0}};
		size[] = {3.515625,7.03125,9.84375};
		interval = 0.5;
	};
	class RHSUSF_CM_L8A3_Smoke_Tail_Small_0: RHSUSF_CM_L8A3_Smoke_Tail_Base{};
	class RHSUSF_CM_L8A3_Smoke_Tail_Small_1: RHSUSF_CM_L8A3_Smoke_Tail_Small_0
	{
		color[] = {{0.6,0.6,0.6,0},{0.5,0.5,0.5,0.3},{0.5,0.5,0.5,0.4},{0.5,0.5,0.5,0.3},{0.5,0.5,0.5,0.2},{0.8,0.8,0.8,0.05},{1,1,1,0}};
		size[] = {2.8125,5.625,7.875};
		interval = 0.3;
	};
	class RHSUSF_CM_L8A3_Smoke_Tail_Small_2: RHSUSF_CM_L8A3_Smoke_Tail_Small_0
	{
		color[] = {{0.6,0.6,0.6,0},{0.5,0.5,0.5,0.2},{0.5,0.5,0.5,0.25},{0.5,0.5,0.5,0.3},{0.5,0.5,0.5,0.1},{0.8,0.8,0.8,0.02},{1,1,1,0}};
		size[] = {1.875,3.75,5.25};
		interval = 0.15;
	};
	class RHSUSF_CM_L8A3_Smoke_Tail_Big_0: RHSUSF_CM_L8A3_Smoke_Tail_Base
	{
		particleShape = "\A3\data_f\cl_basic.p3d";
		particleFSNtieth = 1;
		particleFSIndex = 0;
		particleFSFrameCount = 1;
		particleFSLoop = 0;
		animationName = "";
		particleType = "Billboard";
		timerPeriod = 1;
		animationSpeed[] = {0};
		circleRadius = 0;
		circleVelocity[] = {0,0,0};
		lifeTime = 44;
		lifeTimeVar = 1;
		weight = 1.277;
		volume = "0.985 + randomValue / 60";
		rubbing = 0.1;
		rotationVelocity = 1.5;
		rotationVelocityVar = 1.5;
		randomDirectionPeriod = 0.4;
		randomDirectionIntensity = 0.1;
		bounceOnSurface = -1;
		bounceOnSurfaceVar = 0;
		blockAIVisibility = 1;
		color[] = {{0.8,0.8,0.8,0.005},{0.6,0.6,0.6,0.6},{0.6,0.6,0.6,0.5},{0.6,0.6,0.6,0.3},{0.6,0.6,0.6,0.2},{0.8,0.8,0.8,0.05},{1,1,1,0}};
		size[] = {4.5,7.875,14.625};
		interval = 0.4;
	};
	class RHSUSF_CM_L8A3_Smoke_Tail_Big_1: RHSUSF_CM_L8A3_Smoke_Tail_Big_0
	{
		color[] = {{0.8,0.8,0.8,0.005},{0.6,0.6,0.6,0.45},{0.6,0.6,0.6,0.4},{0.6,0.6,0.6,0.25},{0.6,0.6,0.6,0.1},{0.8,0.8,0.8,0.02},{1,1,1,0}};
		size[] = {3,5.25,9.75};
		interval = 0.2;
	};
	class RHSUSF_CM_L8A3_Smoke_Tail_Big_2: RHSUSF_CM_L8A3_Smoke_Tail_Big_0
	{
		color[] = {{0.8,0.8,0.8,0.005},{0.6,0.6,0.6,0.35},{0.6,0.6,0.6,0.4},{0.6,0.6,0.6,0.15},{0.6,0.6,0.6,0.05},{0.8,0.8,0.8,0.02},{1,1,1,0}};
		size[] = {3,5.25,9.75};
		interval = 0.1;
	};
};
class RHSUSF_CM_L8A3_Spark_Tail_Subeffect
{
	class RHSUSF_CM_L8A3_Smoke_Tail_Small_0
	{
		simulation = "particles";
		type = "RHSUSF_CM_L8A3_Smoke_Tail_Small_0";
		position[] = {0,0,0};
		intensity = 1;
		interval = 1;
		lifeTime = "1 + random 0.25";
		qualityLevel = 0;
	};
	class RHSUSF_CM_L8A3_Smoke_Tail_Small_1: RHSUSF_CM_L8A3_Smoke_Tail_Small_0
	{
		type = "RHSUSF_CM_L8A3_Smoke_Tail_Small_1";
		qualityLevel = 1;
	};
	class RHSUSF_CM_L8A3_Smoke_Tail_Small_2: RHSUSF_CM_L8A3_Smoke_Tail_Small_0
	{
		type = "RHSUSF_CM_L8A3_Smoke_Tail_Small_2";
		qualityLevel = 2;
	};
	class RHSUSF_CM_L8A3_Smoke_Tail_Big_0
	{
		simulation = "particles";
		type = "RHSUSF_CM_L8A3_Smoke_Tail_Big_0";
		position[] = {0,0,0};
		intensity = 1;
		interval = 1;
		lifeTime = "1.5 + random 0.25";
		qualityLevel = 0;
	};
	class RHSUSF_CM_L8A3_Smoke_Tail_Big_1: RHSUSF_CM_L8A3_Smoke_Tail_Big_0
	{
		type = "RHSUSF_CM_L8A3_Smoke_Tail_Big_1";
		qualityLevel = 1;
	};
	class RHSUSF_CM_L8A3_Smoke_Tail_Big_2: RHSUSF_CM_L8A3_Smoke_Tail_Big_0
	{
		type = "RHSUSF_CM_L8A3_Smoke_Tail_Big_2";
		qualityLevel = 2;
	};
};
class RHSUSF_CM_L8A3_Effect
{
	class Explosion
	{
		simulation = "particles";
		type = "GrenadeExp";
		position[] = {0,0,0};
		intensity = 0.5;
		interval = 1;
		lifeTime = 0.5;
	};
	class Explosion_Light
	{
		simulation = "light";
		type = "GrenadeExploLight";
		position[] = {0,0,0};
		intensity = 0.01;
		interval = 1;
		lifeTime = 1;
	};
	class RHSUSF_CM_L8A3_Spark_Tail
	{
		simulation = "particles";
		type = "RHSUSF_CM_L8A3_Spark_Tail";
		position[] = {0,0,0};
		intensity = 1;
		interval = 1;
		lifeTime = 0.2;
	};
	class RHSUSF_CM_L8A3_Spark_Small_0
	{
		simulation = "particles";
		type = "RHSUSF_CM_L8A3_Spark_Small";
		position[] = {0,0,0};
		intensity = 1;
		interval = 1;
		lifeTime = 0.01;
		qualityLevel = 0;
	};
	class RHSUSF_CM_L8A3_Spark_Small_1: RHSUSF_CM_L8A3_Spark_Small_0
	{
		lifeTime = 0.02;
		qualityLevel = 1;
	};
	class RHSUSF_CM_L8A3_Spark_Small_2: RHSUSF_CM_L8A3_Spark_Small_0
	{
		lifeTime = 0.03;
		qualityLevel = 2;
	};
};
class RHS_HeavyGunCartridge1
{
	class RHS_30mm_Catridge
	{
		simulation = "particles";
		type = "RHS_HeavyGunCartridge1";
		position[] = {0,0,0};
		intensity = 1;
		interval = 1;
		lifeTime = 0.05;
		qualityLevel = 2;
	};
	class RHS_30mm_CatridgeMed
	{
		simulation = "particles";
		type = "RHS_HeavyGunCartridge1";
		position[] = {0,0,0};
		intensity = 1;
		interval = 1;
		lifeTime = 0.05;
		qualityLevel = 1;
	};
};
class RHS_ExhaustEffectTankGasBack
{
	class ExhaustsEffect01
	{
		simulation = "particles";
		type = "ExhaustSmoke1HeliBig";
	};
	class ExhaustsEffectRefract01
	{
		simulation = "particles";
		type = "ExhaustSmokeRefractHeliBig";
	};
	class ExhaustsEffectWater01
	{
		simulation = "particles";
		type = "ExhaustSmokeBigWater1";
		qualityLevel = 2;
	};
	class ExhaustsEffectWater01Med
	{
		simulation = "particles";
		type = "ExhaustSmokeBigWater1";
		qualityLevel = 1;
	};
	class ExhaustsEffectWater02
	{
		simulation = "particles";
		type = "ExhaustSmokeBigWater2";
		qualityLevel = 2;
	};
	class ExhaustsEffectWater02Med
	{
		simulation = "particles";
		type = "ExhaustSmokeBigWater2";
		qualityLevel = 1;
	};
};
class RHS_FAE_Explosion
{
	class Light1
	{
		simulation = "light";
		type = "GrenadeExploLight";
		position[] = {0,0,0};
		intensity = 0.01;
		interval = 1;
		lifeTime = 1;
	};
	class MortarExp1
	{
		simulation = "particles";
		type = "MortarExp";
		position[] = {0,0,0};
		intensity = 1;
		interval = 1;
		lifeTime = 0.5;
	};
	class MortarSmoke1
	{
		simulation = "particles";
		type = "CloudSmallLight";
		position[] = {0,0,0};
		intensity = 1;
		interval = 1;
		lifeTime = 1;
	};
};
//};
