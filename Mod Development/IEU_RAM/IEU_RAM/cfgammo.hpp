class cfgAmmo
{
	class ShellBase;
	class Sh_120mm_APFSDS: ShellBase
	{
		simulation = "shotBullet";
	};
	class AIS_HEAT_JET: ShellBase
	{
		airFriction = -0.1;
		indirectHit = 20;
		indirectHitRange = 2;
	};
};