class CfgPatches
{
	class ram_core
	{
		units[]={};
		weapons[]={};
		requiredVersion=1.5;
		requiredAddons[]=
		{
			"A3_Data_F",
			"cba_main"
		};
		author[]=
		{
			"Olds",
			"Bakerman",
			"Afevis"
		};
	};
};
class CfgFunctions
{
	class AIS
	{
		tag="AIS";
		class RealArmour
		{
			file="ieu_ram\addons\ieu_ram\functions";
			class hitPart
			{
			};
			class init
			{
				preInit=1;
			};
		};
	};
};
class Extended_hitPart_EventHandlers
{
	class All
	{
		AIS_cba_hitPart="_this call AIS_fnc_hitPart;";
	};
};
#include "cfgammo.hpp"