class CfgPatches
{
	class RHS_MELB_50DMG
	{
		units[]={};
		weapons[]=
		{
			"mastersafe",
			"RHS_LWIRCM_MELB2",
			"IEU_LAIRCMS"
		};
		requiredVersion=1.5;
		requiredAddons[]=
		{
			"A3_Weapons_F",
			"A3_Air_F",
			"A3_Data_F",
			"rhsusf_a2port_air",
			"RHS_US_A2_AirImport",
			"A3_Air_F_Beta",
			"A3_Weapons_F_beta",
			"rhsusf_c_heavyweapons",
			"A3_Air_F_EPB_Heli_Light_03",
			"rhsusf_c_airweapons",
			"rhsusf_c_melb",
			"A3_Characters_F_Exp_Headgear",
			"A3_Characters_F_exp"
		};
		author[]=
		{
			"Zeitsev"
		};
		magazines[]=
		{
			"2000Rnd_HEIAP_red",
			"1000Rnd_HEIAP_ir"
		};
		ammo[]=
		{
			"HEIAP_50cal_red",
			"HEIAP_50cal_ir",
			"RHS_M151_Ammo_MELB",
			"rhs_ammo_30x113mm_M789_HEDP",
			"rhs_ammo_20mm_AP",
			"RHS_LWIRCM_Ammo_MELB2"
		};
	};
};

class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;
class SlotInfo;
class CowsSlot;
class PointerSlot;
class asdg_OpticRail1913;
class asdg_UnderSlot;
class asdg_MuzzleSlot;
class asdg_MuzzleSlot_762: asdg_MuzzleSlot
{
};
class cfgWeapons
{
	class Default;
	class Rifle_Base_F
	{
		class WeaponSlotsInfo;
	};
	class Rifle_Long_Base_F: Rifle_Base_F
	{
	};
	class LMG_Mk200_F: Rifle_Long_Base_F
	{
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			mass=110;
		};
	};
	class hlc_g3_base: Rifle_Base_F
	{
	};
	class hlc_rifle_PSG1A1_RIS: hlc_g3_base
	{
		displayName="MSG90A2";
		class WeaponSlotsInfo: WeaponSlotsInfo
		{
			mass=123;
			class MuzzleSlot: asdg_MuzzleSlot_762
			{
			};
			class CowsSlot: asdg_OpticRail1913
			{
			};
			class UnderBarrelSlot: asdg_UnderSlot
			{
			};
		};
	};
	class HMG_127;
	class RHS_GAU19_MELB: HMG_127
	{
		displayName="GAU-19/B";
		magazines[]=
		{
			"2000Rnd_HEIAP_red",
			"1000Rnd_HEIAP_ir"
		};
		canLock=2;
	};
	class mastersafe: Default
	{
		scope=2;
		displayName="Mastersafe";
		cursor="EmptyCursor";
		cursorAim="EmptyCursor";
	};
	class M134_minigun;
	class RHS_MARM_SAFE_MELB2: M134_minigun
	{
		CanLock=0;
		displayName="SAFE";
		nameSound="cannon";
		cursor="EmptyCursor";
		cursorAim="EmptyCursor";
		textureType="semi";
		magazines[]=
		{
			"RHS_FakeMagazine_MELB2"
		};
		burst=0;
		reloadTime=0.0099999998;
		magazineReloadTime=0.1;
	};
	class MGun;
	class RHS_LWIRCM_MELB2: MGun
	{
		scope=2;
		displayName="LWIRCM";
		magazines[]=
		{
			"RHS_LWIRCM_Magazine_MELB2"
		};
		magazineReloadTime=60;
		simulation="cmlauncher";
		modes[]=
		{
			"Burst1"
		};
		reloadMagazineSound[]=
		{
			"A3\sounds_f\dummysound",
			2,
			1,
			5
		};
		class Burst1: Mode_Burst
		{
			displayName="LWIRCM";
			reloadTime=0.25;
			burst=120;
			sounds[]=
			{
				"StandardSound"
			};
			class StandardSound
			{
				begin1[]=
				{
					"rhsusf\addons\rhsusf_melb\Sound\ircm_on1",
					2,
					1,
					5
				};
				begin2[]=
				{
					"rhsusf\addons\rhsusf_melb\Sound\ircm_on2",
					2,
					1,
					5
				};
				soundBegin[]=
				{
					"begin1",
					0.5,
					"begin2",
					0.5
				};
				weaponSoundEffect="";
			};
			showToPlayer=1;
			multiplier=1;
			minRange=0;
			maxRange=200;
			soundContinuos=0;
		};
	};
	class IEU_LAIRCMS: RHS_LWIRCM_MELB2
	{
		displayName="LAIRCMS";
		magazineReloadTime=1;
	};
};
class cfgAmmo
{
	class BulletBase;
	class HEIAP_50cal_red: BulletBase
	{
		hit=31.5;
		indirectHit=12;
		indirectHitRange=3;
		explosive=0.30000001;
		explosionEffects="";
		cartridge="FxCartridge_127";
		visibleFire=32;
		audibleFire=32;
		cost=5;
		airLock=1;
		caliber=5.1999998;
		typicalSpeed=1030;
		timeToLive=10;
		model="\A3\Weapons_f\Data\bullettracer\tracer_red";
		tracerScale=1.2;
		tracerStartTime=0.075000003;
		tracerEndTime=1;
		airFriction=-0.00085999997;
		class CamShakeExplode
		{
			power="(13^0.5)";
			duration="((round (13^0.5))*0.2 max 0.2)";
			frequency=20;
			distance="((13^0.5)*3)";
		};
		class CamShakeHit
		{
			power=13;
			duration="((round (13^0.25))*0.2 max 0.2)";
			frequency=20;
			distance=1;
		};
	};
	class HEIAP_50cal_ir: HEIAP_50cal_red
	{
		nvgOnly=1;
	};
	class M_AT;
	class RHS_M151_Ammo_MELB: M_AT
	{
		ace_frag_enabled=1;
		ace_frag_classes[]=
		{
			"ACE_frag_medium",
			"ACE_frag_medium_HD"
		};
		ace_frag_metal=3850;
		ace_frag_charge=1040;
		ace_frag_gurney_c=2700;
		ace_frag_gurney_k="1/2";
	};
	class B_30mm_HE;
	class rhs_ammo_30x113mm_M789_HEDP: B_30mm_HE
	{
		ace_frag_enabled=1;
		ace_frag_classes[]=
		{
			"ACE_frag_tiny_HD"
		};
		ace_frag_metal=362;
		ace_frag_charge=185;
		ace_frag_gurney_c=2843;
		ace_frag_gurney_k="3/5";
		airFriction=-0.00015000001;
	};
	class B_20mm;
	class rhs_ammo_20mm_AP: B_20mm
	{
		ace_frag_enabled=1;
		ace_frag_classes[]=
		{
			"ACE_frag_tiny_HD"
		};
		ace_frag_metal=200;
		ace_frag_charge=45;
		ace_frag_gurney_c=2830;
		ace_frag_gurney_k="1/2";
		airFriction=-0.00015000001;
	};
	class RHS_LWIRCM_Ammo_MELB2: BulletBase
	{
		cost=1;
		visibleFire=0;
		audibleFire=0;
		hit=1;
		indirectHit=0;
		indirectHitRange=0;
		timeToLive=0.1;
		thrustTime=2;
		airFriction=-0.0099999998;
		simulation="shotCM";
		weaponLockSystem="2 + 8";
		model="\A3\weapons_f\empty";
		maxControlRange=-1;
		initTime=0;
		aiAmmoUsageFlags=8;
		soundFly[]=
		{
			"A3\sounds_f\dummysound",
			0.1,
			1
		};
		supersonicCrackNear[]=
		{
			"A3\sounds_f\dummysound",
			0,
			1,
			0
		};
		supersonicCrackFar[]=
		{
			"A3\sounds_f\dummysound",
			0,
			1,
			0
		};
		effectsSmoke="EmptyEffect";
	};
};
class CfgMagazines
{
	class 1000Rnd_20mm_shells;
	class 2000Rnd_HEIAP_red: 1000Rnd_20mm_shells
	{
		scope=2;
		displayName="Mk 300 Red";
		displayNameShort="Mk 300 Red";
		ammo="HEIAP_50cal_red";
		count=2000;
		initSpeed=1030;
		maxLeadSpeed=300;
		tracersEvery=1;
		nameSound="cannon";
	};
	class 1000Rnd_HEIAP_ir: 1000Rnd_20mm_shells
	{
		scope=2;
		displayName="Mk 300 IR";
		displayNameShort="Mk 300 IR";
		ammo="HEIAP_50cal_ir";
		count=1000;
		initSpeed=1030;
		maxLeadSpeed=300;
		tracersEvery=1;
		nameSound="cannon";
	};
	class 5000Rnd_762x51_Belt;
	class RHS_FakeMagazine_MELB2: 5000Rnd_762x51_Belt
	{
		count=0;
		displaynameshort="-";
		displayName="-";
		descriptionShort="-";
		tracersEvery=0;
		weight=0;
	};
	class CA_Magazine;
	class RHS_LWIRCM_Magazine_MELB2: CA_Magazine
	{
		count=120;
		ammo="RHS_LWIRCM_Ammo_MELB2";
		initSpeed=30;
		weight=0;
	};
};
class CfgVehicles
{
	class Car;
	class Car_F: Car
	{
	};
	class Air;
	class Helicopter: Air
	{
		class Turrets;
		class HitPoints
		{
			class HitGlass1;
			class HitGlass2;
			class HitGlass3;
			class HitGlass4;
			class HitGlass5;
			class HitGlass6;
			class HitHull;
			class HitEngine;
			class HitAvionics;
		};
	};
	class Plane: Air
	{
	};
	class Plane_Base_F: Plane
	{
	};
	class Helicopter_Base_F: Helicopter
	{
		class Turrets: Turrets
		{
			class MainTurret;
		};
		class HitPoints: HitPoints
		{
			class HitGlass1;
			class HitGlass2;
			class HitGlass3;
			class HitGlass4;
			class HitGlass5;
			class HitGlass6;
			class HitMissiles;
			class HitHull;
			class HitEngine;
			class HitAvionics;
			class HitVRotor;
			class HitHRotor;
		};
		class AnimationSources;
		class Eventhandlers;
		class CargoTurret;
		class ViewOptics;
	};
	class Helicopter_Base_H: Helicopter_Base_F
	{
		class AnimationSources;
	};
	class Heli_Attack_01_base_F: Helicopter_Base_F
	{
		class HitPoints: HitPoints
		{
			class HitHull;
			class HitFuel;
			class HitAvionics;
			class HitMissiles;
			class HitEngine1;
			class HitEngine2;
			class HitEngine;
			class HitHRotor;
			class HitVRotor;
			class HitGlass1;
			class HitGlass2;
			class HitGlass3;
			class HitGlass4;
			class HitGlass5;
			class HitGlass6;
			class HitGlass7;
			class HitGlass8;
		};
		class Sounds;
	};
	class Heli_Attack_02_base_F: Helicopter_Base_F
	{
	};
	class RHS_MELB_base: Helicopter_Base_H
	{
	};
	class RHS_MELB_AH6M_M: RHS_MELB_base
	{
		weapons[]=
		{
			"RHS_MARM_SAFE_MELB2",
			"RHS_GAU19_MELB",
			"RHS_M261_MELB",
			"RHS_LWIRCM_MELB2"
		};
		magazines[]=
		{
			"rhs_pod_empty",
			"rhs_pod_empty",
			"rhs_pod_FFAR_19",
			"rhs_7_pod_empty",
			"rhs_7_pod_empty",
			"RHS_19Rnd_M151_Magazine_MELB",
			"2000Rnd_HEIAP_red",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2"
		};
	};
	class RHS_MELB_AH6M_H: RHS_MELB_base
	{
		weapons[]=
		{
			"RHS_MARM_SAFE_MELB2",
			"RHS_GAU19_MELB",
			"RHS_LWIRCM_MELB2"
		};
		magazines[]=
		{
			"2000Rnd_HEIAP_red",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2"
		};
		class AnimationSources: AnimationSources
		{
			class GAU_L_Revolving
			{
				source="revolving";
				weapon="RHS_GAU19_MELB";
			};
		};
	};
	class Heli_light_03_base_F: Helicopter_Base_F
	{
		weapons[]=
		{
			"M134_minigun",
			"missiles_DAR",
			"CMFlareLauncher",
			"RHS_LWIRCM_MELB2"
		};
		magazines[]=
		{
			"5000Rnd_762x51_Yellow_Belt",
			"24Rnd_missiles",
			"168Rnd_CMFlare_Chaff_Magazine",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2"
		};
	};
	class RHS_AH64_base: Heli_Attack_01_base_F
	{
	};
	class VTOL_Base_F: Plane_Base_F
	{
	};
	class VTOL_01_base_F: VTOL_Base_F
	{
	};
	class VTOL_01_unarmed_base_F: VTOL_01_base_F
	{
		weapons[]=
		{
			"rhsusf_weap_CMFlareLauncher",
			"IEU_LAIRCMS"
		};
		magazines[]=
		{
			"168Rnd_CMFlare_Chaff_Magazine",
			"168Rnd_CMFlare_Chaff_Magazine",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2"
		};
	};
	class VTOL_01_armed_base_F: VTOL_01_base_F
	{
		weapons[]=
		{
			"rhsusf_weap_CMFlareLauncher",
			"IEU_LAIRCMS"
		};
		magazines[]=
		{
			"168Rnd_CMFlare_Chaff_Magazine",
			"168Rnd_CMFlare_Chaff_Magazine",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2",
			"RHS_LWIRCM_Magazine_MELB2"
		};
	};
	class burnes_husky_base: Car_F
	{
		maximumLoad=4000;
	};
	class RHS_Mi24_base: Heli_Attack_02_base_F
	{
		class RotorLibHelicopterProperties
		{
			RTDconfig="IEU_MELB\MI24\rtd_MI24.xml";
			maxTorque=10000;
			maxMainRotorStress=250000;
			maxTailRotorStress=30000;
			maxHorizontalStabilizerLeftStress=12000;
			maxHorizontalStabilizerRightStress=12000;
			maxVerticalStabilizerStress=8000;
			defaultCollective=0.75;
			autoHoverCorrection[]={6,-1,0};
			retreatBladeStallWarningSpeed=93.056;
			horizontalWingsAngleCollMin=-12.5;
			horizontalWingsAngleCollMax=7.5;
			stressDamagePerSec=0.0033333332;
		};
	};
	class RHS_UH1_Base: Heli_light_03_base_F
	{
	};
	class RHS_UH1Y_base: RHS_UH1_Base
	{
		cyclicAsideForceCoef=2.0999999;
		cyclicForwardForceCoef=2;
		class RotorLibHelicopterProperties
		{
			RTDconfig="IEU_MELB\UH1Y\RTD_UH1Y.xml";
			defaultCollective=0.75;
			autoHoverCorrection[]={5,2.4000001,0};
			maxTorque=1280;
			maxMainRotorStress=130000;
			maxTailRotorStress=50000;
			retreatBladeStallWarningSpeed=71.597;
			horizontalWingsAngleCollMin=0;
			horizontalWingsAngleCollMax=0;
			maxHorizontalStabilizerLeftStress=10000;
			maxHorizontalStabilizerRightStress=10000;
			maxVerticalStabilizerStress=10000;
			stressDamagePerSec=0.0033333332;
		};
	};
	class RHS_AH1Z_base: Heli_Attack_01_base_F
	{
		class RotorLibHelicopterProperties
		{
			RTDconfig="IEU_MELB\AH1Z\RTD_AH1Z.xml";
			defaultCollective=0.75;
			autoHoverCorrection[]={5,2.4000001,0};
			maxTorque=1280;
			maxMainRotorStress=130000;
			maxTailRotorStress=50000;
			retreatBladeStallWarningSpeed=71.597;
			horizontalWingsAngleCollMin=0;
			horizontalWingsAngleCollMax=0;
			maxHorizontalStabilizerLeftStress=10000;
			maxHorizontalStabilizerRightStress=10000;
			maxVerticalStabilizerStress=10000;
			stressDamagePerSec=0.0033333332;
		};
	};
	class Heli_Transport_01_base_F: Helicopter_Base_H
	{
		class RotorLibHelicopterProperties
		{
			RTDconfig="IEU_MELB\UH80\RTD_UH80.xml";
			autoHoverCorrection[]={3,2.45,0};
			defaultCollective=0.625;
			retreatBladeStallWarningSpeed=85.556;
			maxTorque=2500;
			stressDamagePerSec=0.0033333332;
			maxHorizontalStabilizerLeftStress=10000;
			maxHorizontalStabilizerRightStress=10000;
			maxVerticalStabilizerStress=10000;
			horizontalWingsAngleCollMin=0;
			horizontalWingsAngleCollMax=0;
			maxMainRotorStress=200000;
			maxTailRotorStress=25000;
		};
		slingLoadMaxCargoMass=12000;
	};
	class RHS_UH60_Base: Heli_Transport_01_base_F
	{
		class RotorLibHelicopterProperties
		{
			RTDconfig="IEU_MELB\UH60\RTD_UH60M.xml";
			autoHoverCorrection[]={3,2.45,0};
			defaultCollective=0.625;
			retreatBladeStallWarningSpeed=85.556;
			maxTorque=107749;
			stressDamagePerSec=0.0033333332;
			maxHorizontalStabilizerLeftStress=10000;
			maxHorizontalStabilizerRightStress=10000;
			maxVerticalStabilizerStress=10000;
			horizontalWingsAngleCollMin=0;
			horizontalWingsAngleCollMax=0;
			maxMainRotorStress=200000;
			maxTailRotorStress=25000;
		};
		slingLoadMaxCargoMass=12000;
	};
	class rhsusf_CH53E_USMC: Helicopter_Base_H
	{
		simulation="helicopterrtd";
		class RotorLibHelicopterProperties
		{
			RTDconfig="IEU_MELB\CH53E\RTD_CH53E2.xml";
			autoHoverCorrection[]={4.6999998,3.8,0};
			defaultCollective=0.66500002;
			retreatBladeStallWarningSpeed=92.583;
			maxTorque=514126;
			stressDamagePerSec=0.0033333299;
			maxHorizontalStabilizerLeftStress=10000;
			maxHorizontalStabilizerRightStress=10000;
			maxVerticalStabilizerStress=10000;
			horizontalWingsAngleCollMin=0;
			horizontalWingsAngleCollMax=0;
			maxMainRotorStress=570000;
			maxTailRotorStress=120000;
		};
	};
};
