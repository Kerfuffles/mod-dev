class CfgPatches
{
	class Utes
	{
		units[]=
		{
			"Utes"
		};
		weapons[]={};
		requiredVersion=0.1;
		requiredAddons[]={};
	};
};
class CfgVehicles
{
};
class CfgWorlds
{
	class DefaultWorld
	{
		class Weather;
	};
	class CAWorld: DefaultWorld
	{
		class Grid
		{
		};
		class DayLightingBrightAlmost;
		class DayLightingRainy;
		class DefaultClutter;
		class Weather: Weather
		{
			class Lighting;
		};
	};
	class DefaultLighting;
	class utes: CAWorld
	{
		access=3;
		author="$STR_a3_bohemia_interactive";
		worldId=4;
		cutscenes[]=
		{
			"UtesIntro1"
		};
		description="Utes";
		icon="";
		worldName="\ca\utes\utes.wrp";
		pictureMap="\ca\utes\Utes_ca.paa";
		pictureShot="\ca\utes\ui_utes_ca.paa";
		plateFormat="ML$ - #####";
		plateLetters="ABCDEGHIKLMNOPRSTVXZ";
		longitude=30;
		latitude=-45;
		class Grid: Grid
		{
			offsetX=0;
			offsetY=0;
			class Zoom1
			{
				zoomMax=0.15000001;
				format="XY";
				formatX="000";
				formatY="000";
				stepX=100;
				stepY=100;
			};
			class Zoom2
			{
				zoomMax=0.85000002;
				format="XY";
				formatX="00";
				formatY="00";
				stepX=1000;
				stepY=1000;
			};
			class Zoom3
			{
				zoomMax=1e+030;
				format="XY";
				formatX="0";
				formatY="0";
				stepX=10000;
				stepY=10000;
			};
		};
		startTime="14:20";
		startDate="11/10/2008";
		startWeather=0.40000001;
		startFog=0;
		forecastWeather=0.25;
		forecastFog=0;
		centerPosition[]={3500,3500,300};
		seagullPos[]={2560,150,2560};
		ilsPosition[]={3330,3610};
		ilsDirection[]={-1,0.079999998,0};
		ilsTaxiIn[]={3540,3572,3388,3572,3382,3578,3382,3600,3400,3610};
		ilsTaxiOff[]={3520,3610,4040,3610,4050,3600,4050,3580,4045,3575,4040,3572,3540,3572};
		class ReplaceObjects
		{
		};
		class Sounds
		{
			sounds[]={};
		};
		class Animation
		{
			vehicles[]={};
		};
		class Lighting: DefaultLighting
		{
			groundReflection[]={0.059999999,0.059999999,0.029999999};
		};
		class DayLightingBrightAlmost: DayLightingBrightAlmost
		{
			deepNight[]=
			{
				-15,
				{0.050000001,0.050000001,0.059999999},
				{0.001,0.001,0.0020000001},
				{0.02,0.02,0.050000001},
				{0.003,0.003,0.003},
				{9.9999997e-005,9.9999997e-005,0.00019999999},
				{9.9999997e-005,9.9999997e-005,0.00019999999},
				0
			};
			fullNight[]=
			{
				-5,
				{0.050000001,0.050000001,0.050000001},
				{0.02,0.02,0.02},
				{0.039999999,0.039999999,0.039999999},
				{0.039999999,0.039999999,0.039999999},
				{0.0099999998,0.0099999998,0.02},
				{0.079999998,0.059999999,0.059999999},
				0
			};
			sunMoon[]=
			{
				-3.75,
				{0.045000002,0.039999999,0.039999999},
				{0.039999999,0.039999999,0.039999999},
				{0.045000002,0.039999999,0.039999999},
				{0.039999999,0.039999999,0.039999999},
				{0.039999999,0.035,0.039999999},
				{0.1,0.079999998,0.090000004},
				0.5
			};
			earlySun[]=
			{
				-2.5,
				{0.12,0.1,0.1},
				{0.079999998,0.059999999,0.07},
				{0.12,0.1,0.1},
				{0.079999998,0.059999999,0.07},
				{0.079999998,0.07,0.079999998},
				{0.1,0.1,0.12},
				1
			};
			sunrise[]=
			{
				0,
				
				{
					{0.69999999,0.44999999,0.44999999},
					"5.16+(-4)"
				},
				
				{
					{0.07,0.090000004,0.12},
					"4.0+(-4)"
				},
				
				{
					{0.60000002,0.47,0.25},
					"4.66+(-4)"
				},
				
				{
					{0.1,0.090000004,0.1},
					"4.3+(-4)"
				},
				
				{
					{0.5,0.40000001,0.40000001},
					"6.49+(-4)"
				},
				
				{
					{0.88,0.50999999,0.23999999},
					"8.39+(-4)"
				},
				1
			};
			earlyMorning[]=
			{
				3,
				
				{
					{0.64999998,0.55000001,0.55000001},
					"6.04+(-4)"
				},
				
				{
					{0.079999998,0.090000004,0.11},
					"4.5+(-4)"
				},
				
				{
					{0.55000001,0.47,0.25},
					"5.54+(-4)"
				},
				
				{
					{0.1,0.090000004,0.1},
					"5.02+(-4)"
				},
				
				{
					{0.5,0.40000001,0.40000001},
					"7.05+(-4)"
				},
				
				{
					{0.88,0.50999999,0.23999999},
					"8.88+(-4)"
				},
				1
			};
			midMorning[]=
			{
				8,
				
				{
					{0.98000002,0.85000002,0.80000001},
					"8.37+(-4)"
				},
				
				{
					{0.079999998,0.090000004,0.11},
					"6.42+(-4)"
				},
				
				{
					{0.87,0.47,0.25},
					"7.87+(-4)"
				},
				
				{
					{0.090000004,0.090000004,0.1},
					"6.89+(-4)"
				},
				
				{
					{0.5,0.40000001,0.40000001},
					"8.9+(-4)"
				},
				
				{
					{0.88,0.50999999,0.23999999},
					"10.88+(-4)"
				},
				1
			};
			morning[]=
			{
				16,
				
				{
					{1,1,0.89999998},
					"13.17+(-4)"
				},
				
				{
					{0.17,0.18000001,0.19},
					"10.26+(-4)"
				},
				
				{
					{1,1,0.89999998},
					"12.67+(-4)"
				},
				
				{
					{0.17,0.18000001,0.19},
					"11.71+(-4)"
				},
				
				{
					{0.15000001,0.15000001,0.15000001},
					"12.42+(-4)"
				},
				
				{
					{0.17,0.17,0.15000001},
					"14.42+(-4)"
				},
				1
			};
			noon[]=
			{
				45,
				
				{
					{1,1,1},
					"17+(-4)"
				},
				
				{
					{1,1.3,1.55},
					"13.5+(-4)"
				},
				
				{
					{1,1,1},
					"15+(-4)"
				},
				
				{
					{0.36000001,0.37,0.38},
					"13.5+(-4)"
				},
				
				{
					{1,1,1},
					"16+(-4)"
				},
				
				{
					{1,1,1},
					"17+(-4)"
				},
				1
			};
		};
		class DayLightingRainy: DayLightingRainy
		{
			deepNight[]=
			{
				-15,
				{0.0034,0.0034,0.0040000002},
				{0.003,0.003,0.003},
				{0.0034,0.0034,0.0040000002},
				{0.003,0.003,0.003},
				{0.001,0.001,0.0020000001},
				{0.001,0.001,0.0020000001},
				0
			};
			fullNight[]=
			{
				-5,
				{0.023,0.023,0.023},
				{0.02,0.02,0.02},
				{0.023,0.023,0.023},
				{0.02,0.02,0.02},
				{0.0099999998,0.0099999998,0.02},
				{0.079999998,0.059999999,0.059999999},
				0
			};
			sunMoon[]=
			{
				-3.75,
				{0.039999999,0.039999999,0.050000001},
				{0.039999999,0.039999999,0.050000001},
				{0.039999999,0.039999999,0.050000001},
				{0.039999999,0.039999999,0.050000001},
				{0.039999999,0.035,0.039999999},
				{0.11,0.079999998,0.090000004},
				0.5
			};
			earlySun[]=
			{
				-2.5,
				{0.068899997,0.068899997,0.080399998},
				{0.059999999,0.059999999,0.07},
				{0.068899997,0.068899997,0.080399998},
				{0.059999999,0.059999999,0.07},
				{0.079999998,0.07,0.079999998},
				{0.14,0.1,0.12},
				0.5
			};
			earlyMorning[]=
			{
				0,
				
				{
					{1,1,1},
					"(-4)+3.95"
				},
				
				{
					{1,1,1},
					"(-4)+3.0"
				},
				
				{
					{1,1,1},
					"(-4)+3.95"
				},
				
				{
					{1,1,1},
					"(-4)+3.0"
				},
				
				{
					{1,1,1},
					"(-4)+4"
				},
				
				{
					{1,1,1},
					"(-4)+5.5"
				},
				1
			};
			morning[]=
			{
				5,
				
				{
					{1,1,1},
					"(-4)+5.7"
				},
				
				{
					{1,1,1},
					"(-4)+4.5"
				},
				
				{
					{1,1,1},
					"(-4)+5.7"
				},
				
				{
					{1,1,1},
					"(-4)+4.5"
				},
				
				{
					{1,1,1},
					"(-4)+7"
				},
				
				{
					{1,1,1},
					"(-4)+8"
				},
				1
			};
			lateMorning[]=
			{
				25,
				
				{
					{1,1,1},
					"(-4)+10.45"
				},
				
				{
					{1,1,1},
					"(-4)+9.75"
				},
				
				{
					{1,1,1},
					"(-4)+10.45"
				},
				
				{
					{1,1,1},
					"(-4)+9.75"
				},
				
				{
					{1,1,1},
					"(-4)+12"
				},
				
				{
					{1,1,1},
					"(-4)+12.75"
				},
				1
			};
			noon[]=
			{
				70,
				
				{
					{1,1,1},
					"(-4)+12.5"
				},
				
				{
					{1,1,1},
					"(-4)+11"
				},
				
				{
					{1,1,1},
					"(-4)+12"
				},
				
				{
					{1,1,1},
					"(-4)+11"
				},
				
				{
					{1,1,1},
					"(-4)+13.5"
				},
				
				{
					{1,1,1},
					"(-4)+14"
				},
				1
			};
		};
		class Weather: Weather
		{
			class Lighting: Lighting
			{
				class BrightAlmost: DayLightingBrightAlmost
				{
					overcast=0;
				};
				class Rainy: DayLightingRainy
				{
					overcast=1;
				};
			};
		};
		clutterGrid=1;
		clutterDist=125;
		noDetailDist=40;
		fullDetailDist=15;
		midDetailTexture="ca\chernarus\data\cr_trava1_mco.paa";
		minTreesInForestSquare=3;
		minRocksInRockSquare=3;
		class Clutter
		{
			class UTGrassDryBunch: DefaultClutter
			{
				model="ca\plants2\clutter\c_deadGrassBunch.p3d";
				affectedByWind=0.34999999;
				swLighting=1;
				scaleMin=0.5;
				scaleMax=1;
			};
			class UTGrassDryLongBunch: DefaultClutter
			{
				model="ca\plants2\clutter\c_grassDryLongBunch.p3d";
				affectedByWind=0.34999999;
				swLighting=1;
				scaleMin=0.60000002;
				scaleMax=1.1;
			};
			class UTAutumnFlowers: DefaultClutter
			{
				model="ca\plants2\clutter\c_autumn_flowers.p3d";
				affectedByWind=0.40000001;
				swLighting=1;
				scaleMin=0.69999999;
				scaleMax=1;
			};
			class UTHeatherBrush: DefaultClutter
			{
				model="ca\plants2\clutter\c_caluna.p3d";
				affectedByWind=0.15000001;
				swLighting=1;
				scaleMin=0.80000001;
				scaleMax=1.8;
			};
			class UTWeedSedge: DefaultClutter
			{
				model="ca\plants2\clutter\c_weed3.p3d";
				affectedByWind=0.2;
				swLighting=1;
				scaleMin=0.5;
				scaleMax=0.85000002;
			};
			class UTWeedTall: DefaultClutter
			{
				model="ca\plants2\clutter\c_weed2.p3d";
				affectedByWind=0.30000001;
				swLighting=1;
				scaleMin=0.80000001;
				scaleMax=1.1;
			};
			class UTWeedDead: DefaultClutter
			{
				model="ca\plants2\clutter\c_WeedDead.p3d";
				affectedByWind=0.30000001;
				swLighting=1;
				scaleMin=0.75;
				scaleMax=1.1;
			};
			class UTWeedDeadSmall: DefaultClutter
			{
				model="ca\plants2\clutter\c_WeedDead2.p3d";
				affectedByWind=0.30000001;
				swLighting=1;
				scaleMin=0.75;
				scaleMax=0.89999998;
			};
			class UTBlueBerry: DefaultClutter
			{
				model="ca\plants2\clutter\c_BlueBerry.p3d";
				affectedByWind=0.050000001;
				swLighting=1;
				scaleMin=0.85000002;
				scaleMax=1.3;
			};
			class UTFernAutumn: DefaultClutter
			{
				model="ca\plants2\clutter\c_fern.p3d";
				affectedByWind=0.1;
				scaleMin=0.60000002;
				scaleMax=1.2;
			};
			class UTFernAutumnTall: DefaultClutter
			{
				model="ca\plants2\clutter\c_fernTall.p3d";
				affectedByWind=0.15000001;
				scaleMin=0.75;
				scaleMax=1;
			};
			class GrassCrookedForest: DefaultClutter
			{
				model="ca\plants2\clutter\c_GrassCrookedForest.p3d";
				affectedByWind=0.30000001;
				swLighting=1;
				scaleMin=0.80000001;
				scaleMax=1.4;
			};
			class UTMushroomsPrasivka: DefaultClutter
			{
				model="ca\plants2\clutter\c_MushroomPrasivky.p3d";
				affectedByWind=0;
				scaleMin=0.85000002;
				scaleMax=1.25;
			};
		};
		class Subdivision
		{
			class Fractal
			{
				rougness=5;
				maxRoad=0.02;
				maxTrack=0.5;
				maxSlopeFactor=0.050000001;
			};
			class WhiteNoise
			{
				rougness=2;
				maxRoad=0.0099999998;
				maxTrack=0.050000001;
				maxSlopeFactor=0.0024999999;
			};
			minY=-0;
			minSlope=0.02;
		};
		class Ambient
		{
			class Mammals
			{
				radius=200;
				cost="(1 + forest + trees) * (0.5 + (0.5 * night)) * (1 - sea) * (1 - houses)";
				class Species
				{
					class Rabbit
					{
						probability=0.2;
						cost=1;
					};
				};
			};
			class BigBirds
			{
				radius=300;
				cost="((1 + forest + trees) - ((2 * rain)) - houses) * (1 - night) * (1 - sea)";
				class Species
				{
					class Hawk
					{
						probability=0.2;
						cost=1;
					};
				};
			};
			class Birds
			{
				radius=170;
				cost="(1 - night) * ((1 + (3 * sea)) - (2 * rain))";
				class Species
				{
					class Crow
					{
						probability=0.2;
						cost=1;
					};
				};
			};
			class BigInsects
			{
				radius=20;
				cost="(5 - (2 * houses)) * (1 - night) * (1 - rain) * (1 - sea) * (1 - windy)";
				class Species
				{
					class DragonFly
					{
						probability="0.6 - (meadow * 0.5) + (forest * 0.4)";
						cost=1;
					};
					class ButterFly
					{
						probability="0.4 + (meadow * 0.5) - (forest * 0.4)";
						cost=1;
					};
				};
			};
			class BigInsectsAquatic
			{
				radius=20;
				cost="(3 * sea) * (1 - night) * (1 - rain) * (1 - windy)";
				class Species
				{
					class DragonFly
					{
						probability=1;
						cost=1;
					};
				};
			};
			class SmallInsects
			{
				radius=3;
				cost="(12 - 8 * hills) * (1 - night) * (1 - rain) * (1 - sea) * (1 - windy)";
				class Species
				{
					class HouseFly
					{
						probability="deadBody + (1 - deadBody) * (0.5 - forest * 0.1 - meadow * 0.2)";
						cost=1;
					};
					class HoneyBee
					{
						probability="(1 - deadBody) * (0.5 - forest * 0.1 + meadow * 0.2)";
						cost=1;
					};
					class Mosquito
					{
						probability="(1 - deadBody) * (0.2 * forest)";
						cost=1;
					};
				};
			};
			class NightInsects
			{
				radius=3;
				cost="(9 - 8 * hills) * night * (1 - rain) * (1 - sea) * (1 - windy)";
				class Species
				{
					class Mosquito
					{
						probability=1;
						cost=1;
					};
				};
			};
			class WindClutter
			{
				radius=10;
				cost="((20 - 5 * rain) * (3 * (windy factor [0.2, 0.5]))) * (1 - sea)";
				class Species
				{
					class FxWindGrass1
					{
						probability="0.4 - 0.2 * hills - 0.2 * trees";
						cost=1;
					};
					class FxWindGrass2
					{
						probability="0.4 - 0.2 * hills - 0.2 * trees";
						cost=1;
					};
					class FxWindRock1
					{
						probability="0.4 * hills";
						cost=1;
					};
				};
			};
			class NoWindClutter
			{
				radius=15;
				cost=8;
				class Species
				{
					class FxWindPollen1
					{
						probability=1;
						cost=1;
					};
				};
			};
		};
		class Names
		{
			class Utes_Local_LHD
			{
				name="US Carrier";
				position[]={1410.75,1046.23};
				type="NameLocal";
				radiusA=100;
				radiusB=100;
				angle=0;
			};
			class Utes_VillKamenyy
			{
				name="Vill Kamenyy";
				position[]={3334.6799,4445.6201};
				type="NameVillage";
				speech[]=
				{
					"Kamenyy"
				};
				radiusA=100;
				radiusB=100;
				angle=0;
			};
			class Utes_VillStrelka
			{
				name="Vill Strelka";
				position[]={4374.3301,3203.96};
				type="NameVillage";
				speech[]=
				{
					"Strelka"
				};
				radiusA=100;
				radiusB=100;
				angle=0;
			};
			class Utes_FArea01
			{
				name="";
				position[]={2616.6699,3885.9199};
				type="FlatArea";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class Utes_FArea02
			{
				name="";
				position[]={3189.24,4517.1201};
				type="FlatArea";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class Utes_FArea03
			{
				name="";
				position[]={3744.98,3896.3799};
				type="FlatArea";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class Utes_FArea04
			{
				name="";
				position[]={4324,3518.4399};
				type="FlatArea";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class Utes_FArea05
			{
				name="";
				position[]={3524.9199,3083.8601};
				type="FlatArea";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class Utes_SPArea011
			{
				name="";
				position[]={3283.95,4478.2402};
				type="StrongpointArea";
				radiusA=30;
				radiusB=30;
				angle=0;
			};
			class Utes_FAreaC01
			{
				name="";
				position[]={3529.8701,4408.0898};
				type="FlatAreaCity";
				radiusA=40;
				radiusB=40;
				angle=1;
			};
			class Utes_FAreaCS101
			{
				name="";
				position[]={3486.1201,4422.3301};
				type="FlatAreaCitySmall";
				radiusA=20;
				radiusB=20;
				angle=240;
			};
			class Utes_SPArea012
			{
				name="";
				position[]={3391.1799,4304.8701};
				type="StrongpointArea";
				radiusA=30;
				radiusB=30;
				angle=170;
			};
			class Utes_SPArea013
			{
				name="";
				position[]={3605.72,4352.73};
				type="StrongpointArea";
				radiusA=30;
				radiusB=30;
				angle=120;
			};
			class Utes_FAreaC02
			{
				name="";
				position[]={3178.8601,4001.73};
				type="FlatAreaCity";
				radiusA=40;
				radiusB=40;
				angle=120;
			};
			class Utes_FAreaCS201
			{
				name="";
				position[]={3148.3601,4036.4199};
				type="FlatAreaCitySmall";
				radiusA=20;
				radiusB=20;
				angle=260;
			};
			class Utes_SPArea021
			{
				name="";
				position[]={3125,3997.6399};
				type="StrongpointArea";
				radiusA=30;
				radiusB=30;
				angle=200;
			};
			class Utes_FAreaCS102
			{
				name="";
				position[]={3349.1299,4424.75};
				type="FlatAreaCitySmall";
				radiusA=20;
				radiusB=20;
				angle=320;
			};
			class Utes_SPArea022
			{
				name="";
				position[]={3191.49,4094.71};
				type="StrongpointArea";
				radiusA=30;
				radiusB=30;
				angle=300;
			};
			class Utes_SPArea023
			{
				name="";
				position[]={3290.5901,4059.3601};
				type="StrongpointArea";
				radiusA=30;
				radiusB=30;
				angle=80;
			};
			class Utes_SPArea031
			{
				name="";
				position[]={3364.1799,3655.54};
				type="StrongpointArea";
				radiusA=30;
				radiusB=30;
				angle=340;
			};
			class Utes_FAreaC03
			{
				name="";
				position[]={3496.5801,3653.77};
				type="FlatAreaCity";
				radiusA=40;
				radiusB=40;
				angle=0;
			};
			class Utes_FAreaCS301
			{
				name="";
				position[]={3501.6899,3702.1299};
				type="FlatAreaCitySmall";
				radiusA=20;
				radiusB=20;
				angle=350;
			};
			class Utes_SPArea041
			{
				name="";
				position[]={3876.27,3388.3};
				type="StrongpointArea";
				radiusA=30;
				radiusB=30;
				angle=100;
			};
			class Utes_FAreaC04
			{
				name="";
				position[]={3812.75,3321.96};
				type="FlatAreaCity";
				radiusA=40;
				radiusB=40;
				angle=230;
			};
			class Utes_FAreaCS401
			{
				name="";
				position[]={3884.6899,3300.5601};
				type="FlatAreaCitySmall";
				radiusA=20;
				radiusB=20;
				angle=200;
			};
			class Utes_SPArea051
			{
				name="";
				position[]={4233.4702,3233.6899};
				type="StrongpointArea";
				radiusA=30;
				radiusB=30;
				angle=280;
			};
			class Utes_FAreaC05
			{
				name="";
				position[]={4348.1201,3301.79};
				type="FlatAreaCity";
				radiusA=40;
				radiusB=40;
				angle=200;
			};
			class Utes_FAreaCS501
			{
				name="";
				position[]={4261.8198,3137.21};
				type="FlatAreaCitySmall";
				radiusA=20;
				radiusB=20;
				angle=170;
			};
			class Utes_FAreaCS302
			{
				name="";
				position[]={3549.3999,3646.1599};
				type="FlatAreaCitySmall";
				radiusA=20;
				radiusB=20;
				angle=0;
			};
			class Utes_SPArea032
			{
				name="";
				position[]={3570.6799,3744.25};
				type="StrongpointArea";
				radiusA=30;
				radiusB=30;
				angle=0;
			};
			class Utes_SPArea033
			{
				name="";
				position[]={3645.2,3645.5601};
				type="StrongpointArea";
				radiusA=30;
				radiusB=30;
				angle=90;
			};
			class Utes_SPArea034
			{
				name="";
				position[]={3505.0801,3552.52};
				type="StrongpointArea";
				radiusA=30;
				radiusB=30;
				angle=230;
			};
			class Utes_SPArea042
			{
				name="";
				position[]={3757.26,3372.3999};
				type="StrongpointArea";
				radiusA=30;
				radiusB=30;
				angle=240;
			};
			class Utes_SPArea043
			{
				name="";
				position[]={3955.9299,3296.21};
				type="StrongpointArea";
				radiusA=30;
				radiusB=30;
				angle=200;
			};
			class Utes_SPArea052
			{
				name="";
				position[]={4410.8999,3328.74};
				type="StrongpointArea";
				radiusA=30;
				radiusB=30;
				angle=30;
			};
			class Utes_SPArea053
			{
				name="";
				position[]={4272.1699,3095.8101};
				type="StrongpointArea";
				radiusA=30;
				radiusB=30;
				angle=200;
			};
			class Utes_CC01
			{
				name="$STR_LOCATION_KAMENYY";
				position[]={3381.9399,4399.9302};
				type="CityCenter";
				neighbors[]=
				{
					"Utes_CC02"
				};
				demography[]=
				{
					"CIV",
					0,
					"CIV_RU",
					1
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class Utes_CC02
			{
				name="$STR_LOCATION_MILITARY";
				position[]={3203.05,4032.8301};
				type="CityCenter";
				neighbors[]=
				{
					"Utes_CC01",
					"Utes_CC03"
				};
				demography[]=
				{
					"CIV",
					0,
					"CIV_RU",
					0
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class Utes_CC03
			{
				name="$STR_LOCATION_AIRFIELD";
				position[]={3506.23,3660.3401};
				type="CityCenter";
				neighbors[]=
				{
					"Utes_CC02",
					"Utes_CC04"
				};
				demography[]=
				{
					"CIV",
					0,
					"CIV_RU",
					0
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class Utes_CC04
			{
				name="$STR_DN_BARRACKS";
				position[]={3852.79,3324.6799};
				type="CityCenter";
				neighbors[]=
				{
					"Utes_CC03",
					"Utes_CC05"
				};
				demography[]=
				{
					"CIV",
					0,
					"CIV_RU",
					0
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class Utes_CC05
			{
				name="$STR_LOCATION_STRELKA";
				position[]={4326.5,3236.3};
				type="CityCenter";
				neighbors[]=
				{
					"Utes_CC04"
				};
				demography[]=
				{
					"CIV",
					1,
					"CIV_RU",
					0
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class Utes_FAreaCS103
			{
				name="";
				position[]={3416.7,4369.8398};
				type="FlatAreaCitySmall";
				radiusA=20;
				radiusB=20;
				angle=0;
			};
			class Utes_FArea06
			{
				name="";
				position[]={4023.8501,3001.98};
				type="FlatArea";
				radiusA=97.610001;
				radiusB=69.410004;
				angle=0;
			};
		};
		class Armory
		{
			positionAdmin[]={1371.72,986.80298};
			positionStart[]={3553.74,3589.3999};
			positionStartWater[]={2473.0901,4347.4399};
			positionsViewer[]=
			{
				{3505.8899,4422.96},
				{2616,3846.5801},
				{4471.54,3344.4399}
			};
			positionsViewerWater[]=
			{
				{2501.1299,4334.0898}
			};
			positionBlacklist[]=
			{
				
				{
					{2239.5,4697.8198},
					{2672.72,4483.6602}
				},
				
				{
					{2102.1599,3900.5901},
					{2359.27,3721.4199}
				},
				
				{
					{4237.0698,2527.9099},
					{4497.4902,2232.53}
				},
				
				{
					{4285.7998,2620.0601},
					{4354.0601,2561.1899}
				},
				
				{
					{4614.6001,3143.3},
					{4698.0898,3067.8701}
				},
				
				{
					{1875.08,4406.8799},
					{2423.24,3891.4299}
				}
			};
			class Challenges
			{
				class CheckpointRace
				{
					backUpRoute[]=
					{
						{3800.6357,4047.1538},
						{3535.6333,4242.5503},
						{3513.323,4394.3589},
						{3268.0195,4419.6733},
						{3150.375,4353.5679},
						{3123.8372,4299.9341},
						{3235.8428,4066.2512},
						{3134.832,3970.2339}
					};
				};
				class FiringRange
				{
					positionsStart[]=
					{
						{4053.3701,3590.9199}
					};
					directionsStart[]={268};
					positionsStartWater[]=
					{
						{2607.55,4266.3999}
					};
					directionsStartWater[]={301};
				};
				class MobilityRange
				{
					positionStart[]=
					{
						{3678.7864,3497.9763}
					};
					directionStart[]={-39.758896};
					positionStartWater[]=
					{
						{3238.9192,4638.209}
					};
					directionStartWater[]={42.107708};
					objectSets[]=
					{
						"ca\utes\data\scripts\armory\mobilityRange1.sqf"
					};
					positionAnchor[]=
					{
						{3273.1072,4125.916}
					};
					objectSetsWater[]=
					{
						"ca\utes\data\scripts\armory\mobilityRangeWater1.sqf"
					};
					positionAnchorWater[]=
					{
						{2433.8054,4532.8564}
					};
					obstacleSets[]=
					{
						
						{
							
							{
								
								{
									{3654.0247,3527.4666},
									-39.17231,
									5,
									4,
									0,
									""
								},
								
								{
									{3615.0774,3608.9448},
									-63.756039,
									6,
									4,
									10,
									""
								},
								
								{
									{3598.1272,3615.48},
									-6.6272383,
									6,
									4,
									10,
									""
								},
								
								{
									{3593.0957,3689.449},
									-359.19,
									7,
									4,
									4,
									""
								},
								
								{
									{3570.1538,3821.2463},
									-106.53674,
									7,
									3,
									11,
									""
								},
								
								{
									{3565.5093,3835.1851},
									-109.16632,
									7,
									3,
									11,
									""
								},
								
								{
									{3557.6499,3856.6204},
									-109.80223,
									4,
									3,
									11,
									""
								},
								
								{
									{3554.6687,3864.9304},
									-108.53078,
									4,
									3,
									11,
									""
								},
								
								{
									{3534.9268,3917.8879},
									-51.959099,
									4,
									4,
									7,
									""
								},
								
								{
									{3429.7368,3915.8298},
									-78.924423,
									6,
									6,
									7,
									"$STR_LIB_CHAL_MOB_RANGE_HINT_4"
								},
								
								{
									{3242.0691,3954.2791},
									-85.302902,
									4,
									5,
									8,
									""
								},
								
								{
									{3149.6377,3999.1584},
									-309.19617,
									6,
									4,
									4,
									""
								},
								
								{
									{3175.2927,4014.6919},
									-120.07817,
									3,
									3.5,
									1,
									""
								},
								
								{
									{3186.0325,4021.144},
									59.975914,
									3,
									3.5,
									5,
									""
								},
								
								{
									{3210.6699,4038.6213},
									-128.93675,
									3,
									3.5,
									2,
									""
								},
								
								{
									{3220.2671,4047.9055},
									222.48758,
									3,
									3.5,
									6,
									""
								},
								
								{
									{3230.7153,4060.4526},
									-317.3042,
									7,
									4,
									4,
									""
								},
								
								{
									{3183.0984,4158.1152},
									-19.009727,
									3.5,
									5,
									8,
									""
								},
								
								{
									{3129.3765,4313.0503},
									28.179173,
									5,
									4,
									4,
									""
								},
								
								{
									{3075.2744,4387.0654},
									117.74232,
									2.5,
									4,
									3,
									""
								},
								
								{
									{3014.6919,4446.2021},
									-28.666821,
									2.5,
									4,
									3,
									"$STR_LIB_CHAL_MOB_RANGE_HINT_3"
								},
								
								{
									{2979.0586,4494.5835},
									-238.66299,
									7,
									4,
									7,
									"$STR_LIB_CHAL_MOB_RANGE_HINT_1"
								},
								
								{
									{2919.2947,4530.6255},
									-66.229095,
									2.5,
									4,
									3,
									""
								},
								
								{
									{2875.7378,4562.0693},
									-51.317272,
									6,
									4,
									9,
									""
								}
							},
							
							{
								
								{
									{3654.0247,3527.4666},
									-39.17231,
									5,
									4,
									0,
									""
								},
								
								{
									{3615.0774,3608.9448},
									-63.756039,
									6,
									4,
									10,
									""
								},
								
								{
									{3598.1272,3615.48},
									-6.6272383,
									6,
									4,
									10,
									""
								},
								
								{
									{3593.0957,3689.449},
									-359.19,
									7,
									4,
									4,
									""
								},
								
								{
									{3570.1538,3821.2463},
									-106.53674,
									7,
									3,
									11,
									""
								},
								
								{
									{3565.5093,3835.1851},
									-109.16632,
									7,
									3,
									11,
									""
								},
								
								{
									{3557.6499,3856.6204},
									-109.80223,
									4,
									3,
									11,
									""
								},
								
								{
									{3554.6687,3864.9304},
									-108.53078,
									4,
									3,
									11,
									""
								},
								
								{
									{3534.9268,3917.8879},
									-51.959099,
									4,
									4,
									7,
									""
								},
								
								{
									{3429.7368,3915.8298},
									-78.924423,
									6,
									6,
									7,
									"$STR_LIB_CHAL_MOB_RANGE_HINT_4"
								},
								
								{
									{3242.0691,3954.2791},
									-85.302902,
									4,
									5,
									8,
									""
								},
								
								{
									{3149.6377,3999.1584},
									-309.19617,
									6,
									4,
									4,
									""
								},
								
								{
									{3175.2927,4014.6919},
									-120.07817,
									3,
									3.5,
									1,
									""
								},
								
								{
									{3186.0325,4021.144},
									59.975914,
									3,
									3.5,
									5,
									""
								},
								
								{
									{3210.6699,4038.6213},
									-128.93675,
									3,
									3.5,
									2,
									""
								},
								
								{
									{3220.2671,4047.9055},
									222.48758,
									3,
									3.5,
									6,
									""
								},
								
								{
									{3230.7153,4060.4526},
									-317.3042,
									7,
									4,
									4,
									""
								},
								
								{
									{3183.0984,4158.1152},
									-19.009727,
									3.5,
									5,
									8,
									""
								},
								
								{
									{3129.3765,4313.0503},
									28.179173,
									5,
									4,
									4,
									""
								},
								
								{
									{3075.2744,4387.0654},
									117.74232,
									2.5,
									4,
									3,
									""
								},
								
								{
									{3014.6919,4446.2021},
									-28.666821,
									2.5,
									4,
									3,
									"$STR_LIB_CHAL_MOB_RANGE_HINT_3"
								},
								
								{
									{2979.0586,4494.5835},
									-238.66299,
									7,
									4,
									7,
									"$STR_LIB_CHAL_MOB_RANGE_HINT_1"
								},
								
								{
									{2919.2947,4530.6255},
									-66.229095,
									2.5,
									4,
									3,
									""
								},
								
								{
									{2828.304,4533.7485},
									-443.78128,
									5,
									4,
									4,
									""
								},
								
								{
									{2539.7815,4585.7358},
									-81.990799,
									6,
									4,
									9,
									""
								}
							}
						}
					};
					obstacleSetsWater[]=
					{
						
						{
							
							{
								{3111.9023,4750.6836},
								-70.67804,
								13,
								4,
								0,
								""
							},
							
							{
								{2836.8989,4783.5952},
								-82.992744,
								9,
								5,
								8,
								""
							},
							
							{
								{2682.1924,4535.8105},
								180.47932,
								13,
								4,
								4,
								""
							},
							
							{
								{2656.0906,4399.668},
								201.95113,
								13,
								4,
								7,
								"$STR_LIB_CHAL_MOB_RANGE_HINT_1"
							},
							
							{
								{2618.2991,4322.6646},
								208.59201,
								13,
								4,
								7,
								"$STR_LIB_CHAL_MOB_RANGE_HINT_1"
							},
							
							{
								{2317.3977,4327.0625},
								-138.16682,
								12,
								3,
								11,
								""
							},
							
							{
								{2297.8533,4344.6738},
								-138.16682,
								12,
								3,
								11,
								""
							},
							
							{
								{2277.8901,4361.9702},
								-138.16701,
								12,
								3,
								11,
								""
							},
							
							{
								{1832.7701,4436.4683},
								54.45274,
								6.5,
								4,
								7,
								""
							},
							
							{
								{1807.2694,4210.064},
								5.0792484,
								5,
								4,
								3,
								""
							},
							
							{
								{1847.9224,4078.2559},
								139.13478,
								13,
								4,
								10,
								""
							},
							
							{
								{1865.746,4059.2993},
								92.960304,
								10,
								4,
								10,
								""
							},
							
							{
								{1892.8416,4079.0095},
								101.54845,
								10,
								4,
								10,
								""
							},
							
							{
								{1914.6152,4051.7095},
								141.74243,
								13,
								4,
								10,
								""
							},
							
							{
								{2236.6572,3909.2988},
								-71.417564,
								13,
								5,
								9,
								""
							}
						}
					};
					positionOncomingTraffic[]=
					{
						{3301.8403,4416.397}
					};
					directionOncomingTraffic[]={-69.226257};
					waypointsOncomingTraffic[]=
					{
						
						{
							{3136.7576,4330.5205},
							{3169.1538,4201.293},
							{3465.5542,4014.345}
						}
					};
					positionTriggerOncomingTraffic[]=
					{
						{3148.2014,3997.9011}
					};
					radiusTriggerOncomingTraffic[]={10};
				};
				class FitnessTrack
				{
					positionStart[]=
					{
						{3501.6101,4172.04}
					};
					directionStart[]={22.540001};
					objectSets[]=
					{
						"ca\utes\data\scripts\armory\fitnessTrack1.sqf"
					};
					positionAnchor[]=
					{
						{3613.29,4303.6499}
					};
					obstacleSets[]=
					{
						
						{
							
							{
								{3504.03,4177.4199},
								21.030001,
								3,
								2,
								0,
								""
							},
							
							{
								{3527.6001,4229.0098},
								28.24,
								2,
								2,
								3,
								""
							},
							
							{
								{3585.1799,4214.5},
								133.41,
								1.4,
								1.5,
								4,
								""
							},
							
							{
								{3665.53,4158.3999},
								131.16901,
								3,
								2,
								12,
								""
							},
							
							{
								{3720.5701,4166.7202},
								26.659,
								2,
								2,
								5,
								""
							},
							
							{
								{3733.97,4212.27},
								-20.1054,
								1.6,
								2,
								1,
								""
							},
							
							{
								{3728.5701,4254.3501},
								19.1091,
								1.8,
								2,
								2,
								""
							},
							
							{
								{3689.0601,4287.79},
								-80.436798,
								3,
								2,
								7,
								""
							},
							
							{
								{3669.5801,4327.1001},
								-1.1495399,
								3,
								2,
								6,
								""
							},
							
							{
								{3657.4099,4375.1001},
								0.672324,
								1.8,
								2,
								9,
								""
							},
							
							{
								{3631.6599,4476.0098},
								0.672324,
								2,
								2,
								8,
								""
							},
							
							{
								{3589.8301,4462.6802},
								-105.064,
								4,
								2,
								11,
								""
							},
							
							{
								{3534.6001,4451.77},
								-94.841003,
								4,
								2,
								10,
								""
							}
						}
					};
					positionMachineguns[]=
					{
						
						{
							{3591.1201,4225.2402}
						}
					};
					directionMachineguns[]=
					{
						{-146.416}
					};
					positionStartAnimal[]=
					{
						{2522.99,3818.72}
					};
					directionStartAnimal[]={50.139999};
					objectSetsAnimal[]=
					{
						"ca\utes\data\scripts\armory\fitnessTrackAnimals1.sqf"
					};
					positionAnchorAnimal[]=
					{
						{2534.6299,3855.1799}
					};
					obstacleSetsAnimal[]=
					{
						
						{
							
							{
								{2526.9299,3821.4299},
								56.0742,
								3,
								2,
								0,
								""
							},
							
							{
								{2550.8501,3835.8},
								67.667297,
								3.5,
								2,
								7,
								""
							},
							
							{
								{2574.0901,3859.8701},
								23.9655,
								3.5,
								2,
								11,
								""
							},
							
							{
								{2545.26,3855.6101},
								-125.516,
								1.5,
								2,
								2,
								""
							},
							
							{
								{2531.51,3844.03,0.308052},
								-131.47099,
								2.5,
								2,
								3,
								""
							},
							
							{
								{2516.6499,3855.0901},
								31.016001,
								5,
								2,
								11,
								""
							},
							
							{
								{2545.52,3865.04},
								57.768902,
								2,
								2,
								9,
								""
							},
							
							{
								{2558.3401,3878.3601},
								46.828999,
								2.5,
								2,
								10,
								""
							}
						}
					};
				};
				class KillHouse
				{
					class Small
					{
						positionStart[]=
						{
							{4372.123,3268.4604}
						};
						directionStart[]={-150};
						positionEnd[]=
						{
							{4398.9243,3247.886}
						};
						objectSet[]=
						{
							"ca\utes\data\scripts\armory\killHouseSmall1.sqf"
						};
						positionAnchor[]=
						{
							{4353.3188,3220.5522}
						};
						waypoints[]=
						{
							
							{
								{4347.7397,3229.2974},
								{4316.4814,3257.6619},
								{4300.6387,3227.7827},
								{4326.6592,3201.9578},
								{4373.0527,3162.8604},
								{4392.04,3199.3225},
								{4367.5381,3220.0015},
								{4364.999,3237.8804}
							}
						};
						class Targets
						{
							class Set1
							{
								class T1
								{
									position[]={4363.8101,3236.6633};
									direction=-195;
									upTime=4;
									positionMove[]={};
									moveTime=0;
									type=0;
									side=0;
									size=0;
									spawn=0;
									positionTrigger[]={4361.3359,3247.6973};
									radiusTrigger=10;
								};
								class T2: T1
								{
									position[]={4343.3081,3239.8955};
									direction=-139;
									upTime=5;
									moveTime=1;
									positionMove[]={4346.0093,3237.6501};
									positionTrigger[]={4357.4517,3242.0537};
									type=1;
									spawn=1;
								};
								class T3: T1
								{
									position[]={4326.0371,3235.4163};
									direction=-79;
									upTime=5;
									positionTrigger[]={4341.4048,3232.2617};
								};
								class T3A: T1
								{
									position[]={4325.3657,3234.845,-0.30000001};
									direction=-79;
									positionTrigger[]={};
									side=1;
									spawn=1;
								};
								class T4: T1
								{
									position[]={4335.2192,3266.187};
									direction=0;
									positionTrigger[]={4327.8101,3248.469};
								};
								class T4A: T1
								{
									position[]={4335.4424,3265.5388};
									direction=2;
									positionTrigger[]={};
									side=1;
									spawn=1;
								};
								class T5: T1
								{
									position[]={4315.5664,3250.3601};
									direction=-52;
									upTime=7;
									moveTime=1;
									positionMove[]={4317.4131,3252.7659};
									positionTrigger[]={4333.0977,3243.4441};
									type=1;
								};
								class T7: T1
								{
									position[]={4284.1553,3250.3081};
									direction=-94;
									positionTrigger[]={4310.8149,3254.2078};
									side=2;
								};
								class T8: T1
								{
									position[]={4305.0723,3236.8074};
									direction=-143;
									moveTime=1;
									positionMove[]={4303.3984,3238.28};
									positionTrigger[]={4304.8379,3245.543};
									type=1;
									spawn=1;
								};
								class T9: T1
								{
									position[]={4288.0747,3231.6367};
									direction=-127;
									positionTrigger[]={4303.251,3243.3743};
								};
								class T10: T1
								{
									position[]={4292.7227,3207.3728};
									direction=-150;
									moveTime=2;
									positionMove[]={4289.8604,3209.3259};
									positionTrigger[]={4300.3945,3224.7551};
									side=2;
									type=1;
									spawn=1;
								};
								class T11: T1
								{
									position[]={4338.437,3206.4187};
									direction=-254;
									positionTrigger[]={4318.3901,3208.5281};
									spawn=1;
								};
								class T12: T1
								{
									position[]={4319.6577,3198.4287};
									direction=-168;
									positionTrigger[]={4317.5742,3208.4312};
									side=2;
								};
								class T13: T1
								{
									position[]={4347.1763,3201.1753,13};
									direction=-254;
									positionTrigger[]={4314.147,3211.1079};
								};
								class T15: T1
								{
									position[]={4368.1675,3185.939,1.5};
									direction=-329;
									positionTrigger[]={4354.4087,3179.9197};
									side=2;
								};
								class T16: T1
								{
									position[]={4364.856,3156.364};
									direction=-193;
									positionTrigger[]={4360.7881,3170.8186};
								};
								class T17: T1
								{
									position[]={4400.792,3161.0552};
									direction=-256;
									positionTrigger[]={4372.8926,3162.8469};
									spawn=1;
								};
								class T18: T1
								{
									position[]={4377.582,3180.7805};
									direction=-411;
									moveTime=0.5;
									positionMove[]={4380.2642,3178.5195};
									positionTrigger[]={4385.2651,3180.3909};
									type=1;
									spawn=1;
								};
								class T20: T1
								{
									position[]={4375.5518,3220.2212,0.5};
									direction=-399;
									upTime=5;
									positionTrigger[]={4380.8862,3209.4778};
								};
								class T21: T1
								{
									position[]={4378.5098,3224.3145,0.5};
									direction=-379;
									upTime=5;
									positionTrigger[]={4382.0474,3210.6389};
									side=2;
								};
								class T22: T1
								{
									position[]={4372.0762,3220.9128,0.30000001};
									direction=-327;
									positionTrigger[]={4368.5898,3219.0383};
									side=2;
									spawn=1;
									radiusTrigger=5;
								};
								class T23: T1
								{
									position[]={4370.6426,3224.3228,3};
									direction=-327;
									positionTrigger[]={4365.8232,3221.4814};
									radiusTrigger=5;
								};
								class T24: T1
								{
									position[]={4352.689,3224.1221};
									direction=-100;
									positionTrigger[]={4359.2837,3227.043};
									radiusTrigger=5;
									spawn=1;
								};
								class T25: T1
								{
									position[]={4360.7651,3234.2227};
									direction=-9;
									positionTrigger[]={4359.5352,3227.0071};
									radiusTrigger=5;
									spawn=1;
								};
								class T26: T1
								{
									position[]={4385.7339,3252.9546};
									direction=52;
									positionTrigger[]={4372.8525,3242.4895};
								};
								class T27: T1
								{
									position[]={4405.8013,3234.9004};
									direction=-328;
									positionTrigger[]={4391.7524,3198.9734};
									spawn=1;
								};
								class T28: T1
								{
									position[]={4332.1582,3205.0896};
									direction=91;
									positionTrigger[]={};
									side=1;
								};
								class T29: T1
								{
									position[]={4332.6387,3203.8176};
									direction=91;
									positionTrigger[]={};
									side=1;
								};
								class T30: T1
								{
									position[]={4408.2285,3214.5266};
									direction=55;
									positionTrigger[]={};
									side=1;
								};
							};
						};
					};
					class Large
					{
						positionStart[]=
						{
							{3606.1731,4333.7759}
						};
						directionStart[]={-50};
						positionEnd[]=
						{
							{3239.6494,4617.4443}
						};
						objectSet[]=
						{
							"ca\utes\data\scripts\armory\killHouseLarge1.sqf"
						};
						positionAnchor[]=
						{
							{3380.2207,4466.2979}
						};
						waypoints[]=
						{
							
							{
								{3514.4084,4393.6108},
								{3418.9998,4407.3745},
								{3328.1802,4408.8535},
								{3268.8545,4418.5986},
								{3266.6401,4493.979},
								{3284.0579,4583.2437}
							}
						};
						class Targets
						{
							class Set1
							{
								class T1
								{
									position[]={3577.5486,4366.1035};
									direction=306;
									upTime=6;
									positionMove[]={3576.6074,4365.0112};
									moveTime=0.5;
									type=1;
									side=0;
									size=0;
									spawn=0;
									positionTrigger[]={3582.9922,4352.229};
									radiusTrigger=15;
								};
								class T2: T1
								{
									position[]={3548.2246,4368.3691};
									direction=273;
									upTime=8;
									positionMove[]={};
									type=0;
									spawn=1;
									positionTrigger[]={3559.0652,4369.3208};
								};
								class T3: T1
								{
									position[]={3546.7126,4369.2358};
									direction=273;
									upTime=7;
									positionMove[]={};
									type=0;
									positionTrigger[]={3558.55,4368.6509};
								};
								class T4: T1
								{
									position[]={3546.4238,4370.481};
									direction=273;
									upTime=8;
									positionMove[]={};
									type=0;
									positionTrigger[]={3557.9836,4367.9819};
								};
								class T5: T1
								{
									position[]={3546.3037,4368.1265};
									direction=-89;
									positionMove[]={};
									type=0;
									side=1;
									spawn=1;
									positionTrigger[]={};
								};
								class T6: T1
								{
									position[]={3545.4014,4369.8062};
									direction=-89;
									positionMove[]={};
									type=0;
									side=1;
									spawn=1;
									positionTrigger[]={};
								};
								class T7: T1
								{
									position[]={3512.4414,4383.4355};
									direction=290;
									upTime=7;
									positionMove[]={3513.9778,4388.8442};
									moveTime=2;
									size=1;
									positionTrigger[]={3528.541,4387.6987};
								};
								class T8: T1
								{
									position[]={3535.6216,4413.4858};
									direction=321;
									upTime=8;
									positionMove[]={3516.3611,4407.6953};
									moveTime=2;
									spawn=1;
									side=2;
									positionTrigger[]={3539.5764,4381.7188};
								};
								class T9: T1
								{
									position[]={3535.467,4414.207};
									direction=321;
									upTime=8;
									positionMove[]={3515.6274,4408.2783};
									moveTime=2;
									side=2;
									positionTrigger[]={3539.9343,4382.4048};
								};
								class T10: T1
								{
									position[]={3487.5708,4389.0176};
									direction=-138;
									positionMove[]={};
									type=0;
									side=1;
									positionTrigger[]={};
								};
								class T11: T1
								{
									position[]={3464.2327,4417.0649};
									direction=302;
									upTime=7;
									positionMove[]={};
									type=0;
									positionTrigger[]={3472.0525,4407.3027};
								};
								class T12: T1
								{
									position[]={3462.7991,4416.332};
									direction=273;
									upTime=8;
									positionMove[]={};
									type=0;
									positionTrigger[]={3472.1736,4408.3154};
								};
								class T13: T1
								{
									position[]={3462.1624,4417.187};
									direction=301;
									upTime=8;
									positionMove[]={};
									type=0;
									size=1;
									positionTrigger[]={3472.1738,4409.3281};
								};
								class T14: T1
								{
									position[]={3439.0381,4387.4663};
									direction=-135;
									positionMove[]={};
									type=0;
									side=1;
									spawn=1;
									positionTrigger[]={};
								};
								class T15: T1
								{
									position[]={3438.3779,4387.3457};
									direction=232;
									upTime=9;
									positionMove[]={};
									type=0;
									positionTrigger[]={3452.7944,4410.4443};
								};
								class T16: T1
								{
									position[]={3414.2185,4415.2959};
									direction=-13;
									positionMove[]={};
									type=0;
									side=1;
									spawn=1;
									positionTrigger[]={};
								};
								class T17: T1
								{
									position[]={3413.9592,4398.873};
									direction=205;
									upTime=7;
									positionMove[]={};
									type=0;
									spawn=1;
									positionTrigger[]={3416.6992,4407.876};
								};
								class T18: T1
								{
									position[]={3412.3118,4398.4614};
									direction=217;
									upTime=6;
									positionMove[]={};
									type=0;
									spawn=1;
									positionTrigger[]={3416.99,4406.1338};
								};
								class T19: T1
								{
									position[]={3394.8223,4409.4277};
									direction=260;
									upTime=6;
									positionMove[]={3395.0764,4407.4956};
									moveTime=1;
									size=1;
									positionTrigger[]={3400.2688,4405.2046};
								};
								class T20: T1
								{
									position[]={3393.4194,4409.3774};
									direction=259;
									upTime=7;
									positionMove[]={3394.2012,4406.0625};
									moveTime=1;
									spawn=1;
									positionTrigger[]={3400.5588,4403.811};
								};
								class T21: T1
								{
									position[]={3373.1465,4435.1523};
									direction=331;
									upTime=10;
									positionMove[]={};
									type=0;
									side=2;
									spawn=1;
									positionTrigger[]={3383.8691,4403.3589};
								};
								class T22: T1
								{
									position[]={3368.1272,4435.0781};
									direction=331;
									upTime=9;
									positionMove[]={};
									type=0;
									side=2;
									spawn=1;
									positionTrigger[]={3384.2297,4401.5566};
								};
								class T23: T1
								{
									position[]={3323.6411,4390.3774};
									direction=213;
									upTime=7;
									positionMove[]={};
									type=0;
									positionTrigger[]={3333.0859,4406.252};
								};
								class T24: T1
								{
									position[]={3321.8696,4393.5513};
									direction=243;
									upTime=7;
									positionMove[]={};
									type=0;
									positionTrigger[]={3333.1262,4407.5752};
								};
								class T25: T1
								{
									position[]={3304.6829,4430.0347};
									direction=22;
									positionMove[]={};
									type=0;
									side=1;
									positionTrigger[]={};
								};
								class T26: T1
								{
									position[]={3303.71,4429.8877};
									direction=0;
									positionMove[]={};
									type=0;
									side=1;
									positionTrigger[]={};
								};
								class T27: T1
								{
									position[]={3249.2603,4427.4946,4.6999998};
									direction=279;
									upTime=7;
									positionMove[]={};
									type=0;
									positionTrigger[]={3277.8914,4420.6108};
								};
								class T28: T1
								{
									position[]={3237.5056,4419.0625};
									direction=-93;
									positionMove[]={};
									type=0;
									side=1;
									spawn=1;
									positionTrigger[]={};
								};
								class T29: T1
								{
									position[]={3289.8564,4451.7041};
									direction=358;
									upTime=15;
									positionMove[]={3276.7927,4451.4141};
									moveTime=2;
									size=1;
									spawn=1;
									positionTrigger[]={3266.2544,4441.8745};
									radiusTrigger=25;
								};
								class T30: T1
								{
									position[]={3275.345,4548.2476};
									direction=368;
									upTime=15;
									positionMove[]={3270.0256,4523.9385};
									moveTime=4;
									size=1;
									positionTrigger[]={3267.8113,4509.0122};
									radiusTrigger=25;
								};
								class T31: T1
								{
									position[]={3307.4443,4527.4482};
									direction=391;
									upTime=8;
									positionMove[]={};
									type=0;
									positionTrigger[]={3262.4653,4482.9307};
								};
								class T32: T1
								{
									position[]={3310.5972,4529.7251};
									direction=391;
									upTime=7;
									positionMove[]={};
									type=0;
									positionTrigger[]={3263.8813,4482.8394};
								};
								class T33: T1
								{
									position[]={3304.4612,4532.7046};
									direction=370;
									upTime=8;
									positionMove[]={};
									type=0;
									positionTrigger[]={3265.2969,4482.748};
								};
								class T34: T1
								{
									position[]={3239.022,4585.207,5.1999998};
									direction=273;
									upTime=9;
									positionMove[]={};
									type=0;
									spawn=1;
									positionTrigger[]={3265.2969,4482.748};
								};
								class T35: T1
								{
									position[]={3234.925,4590.0215,5.1999998};
									direction=273;
									upTime=8;
									positionMove[]={};
									type=0;
									positionTrigger[]={3270.3784,4584.5957};
								};
								class T36: T1
								{
									position[]={3231.2439,4594.8281};
									direction=313;
									upTime=6;
									positionMove[]={3232.5088,4596.7363};
									moveTime=0.5;
									spawn=1;
									positionTrigger[]={3270.6687,4587.2085};
								};
								class T37: T1
								{
									position[]={3261.3203,4627.1055,-0.5};
									direction=375;
									upTime=15;
									positionMove[]={};
									type=0;
									size=1;
									positionTrigger[]={3245.9016,4609.0723};
								};
								class T38: T1
								{
									position[]={3251.9712,4639.0049,3.3};
									direction=364;
									upTime=8;
									positionMove[]={};
									type=0;
									spawn=1;
									side=2;
									positionTrigger[]={3245.2471,4607.959};
								};
								class T39: T1
								{
									position[]={3258.4185,4641.499,3.3};
									direction=356;
									upTime=8;
									positionMove[]={};
									type=0;
									side=2;
									positionTrigger[]={3244.5925,4606.6499};
								};
							};
						};
					};
					class Water
					{
						positionStart[]=
						{
							{3779.9541,4578.1982}
						};
						directionStart[]={-129};
						positionEnd[]=
						{
							{3291.175,4748.0737}
						};
						objectSet[]=
						{
							"ca\utes\data\scripts\armory\killHouseWater1.sqf"
						};
						positionAnchor[]=
						{
							{3472.1089,4612.9517}
						};
						waypoints[]=
						{
							
							{
								{3665.5872,4485.3652},
								{3649.0391,4630.166},
								{3480.4502,4749.1089},
								{3294.2783,4637.4053}
							}
						};
						class Targets
						{
							class Set1
							{
								class T1
								{
									position[]={3631.9194,4453.937,1.3};
									direction=199;
									upTime=10;
									positionMove[]={};
									moveTime=0;
									type=0;
									side=0;
									size=0;
									spawn=0;
									positionTrigger[]={3653.3789,4476.6929};
									radiusTrigger=60;
								};
								class T2: T1
								{
									position[]={3631.9353,4446.0527,-0.60000002};
									direction=199;
									upTime=15;
									positionMove[]={3636.9346,4462.3921,-0.60000002};
									moveTime=2;
									type=1;
									size=1;
								};
								class T3: T1
								{
									position[]={3642.5334,4438.5225,1.25};
									direction=191;
									upTime=13;
								};
								class T4: T1
								{
									position[]={3644.7942,4438.6182,1.25};
									direction=191;
									upTime=14;
									spawn=1;
								};
								class T5: T1
								{
									position[]={3648.4897,4437.7334,1.25};
									direction=182;
									upTime=14;
								};
								class T6: T1
								{
									position[]={3626.4241,4452.6665,1.3};
									direction=204;
									side=2;
								};
								class T7: T1
								{
									position[]={3625.1296,4452.7886,1.3};
									direction=204;
									side=2;
								};
								class T8: T1
								{
									position[]={3617.9077,4454.6797,1.38};
									direction=196;
									upTime=14;
								};
								class T9: T1
								{
									position[]={3613.9377,4456.7051,1.45};
									direction=196;
									upTime=14;
								};
								class T10: T1
								{
									position[]={3566.3008,4547.979};
									direction=274;
									positionTrigger[]={3641.8259,4588.8159};
									spawn=1;
								};
								class T11: T1
								{
									position[]={3538.8254,4558.5181};
									direction=260;
									positionTrigger[]={3641.8259,4588.8159};
									size=1;
								};
								class T12: T1
								{
									position[]={3548.0427,4577.3579};
									direction=286;
									positionTrigger[]={3641.8259,4588.8159};
									type=1;
									positionMove[]={3547.0793,4570.1279};
									moveTime=1;
								};
								class T13: T1
								{
									position[]={3496.9111,4645.8369};
									direction=222;
									positionTrigger[]={3542.2529,4694.1929};
								};
								class T14: T1
								{
									position[]={3494.9409,4653.8125};
									direction=210;
									positionTrigger[]={3542.2529,4694.1929};
								};
								class T15: T1
								{
									position[]={3493.0642,4655.4072};
									direction=222;
									positionTrigger[]={3542.2529,4694.1929};
									spawn=1;
								};
								class T16: T1
								{
									position[]={3484.7134,4653.0615};
									direction=209;
									positionTrigger[]={3542.2529,4694.1929};
								};
								class T17: T1
								{
									position[]={3268.0027,4624.6851,-0.60000002};
									direction=223;
									upTime=15;
									positionMove[]={3291.7334,4656.2925,-0.60000002};
									moveTime=2;
									type=1;
									size=1;
									positionTrigger[]={3326.3044,4673.1729};
								};
								class T18: T1
								{
									position[]={3272.3445,4655.9805,3.3};
									direction=268;
									positionTrigger[]={3326.3044,4673.1729};
								};
								class T19: T1
								{
									position[]={3273.4316,4659.5791,3.3};
									direction=271;
									positionTrigger[]={3326.3044,4673.1729};
								};
								class T20: T1
								{
									position[]={3244.1997,4609.6875,3.3};
									direction=223;
									positionTrigger[]={3269.2769,4615.353};
								};
								class T21: T1
								{
									position[]={3269.1843,4599.3359,3.3};
									direction=202;
									positionTrigger[]={3269.2769,4615.353};
									spawn=1;
								};
								class T22: T1
								{
									position[]={3266.3005,4598.8726,3.3};
									direction=208;
									positionTrigger[]={3269.2769,4615.353};
								};
								class T23: T1
								{
									position[]={3270.9441,4594.9844,3.3};
									direction=191;
									positionTrigger[]={3269.2769,4615.353};
								};
								class T24: T1
								{
									position[]={3234.4922,4589.5386,5.1999998};
									direction=226;
									positionTrigger[]={3269.2769,4615.353};
								};
								class T25: T1
								{
									position[]={3388.2332,4620.0986};
									direction=191;
									positionTrigger[]={};
									side=1;
								};
								class T26: T1
								{
									position[]={3397.3,4619.6089};
									direction=178;
									positionTrigger[]={};
									side=1;
								};
								class T27: T1
								{
									position[]={3695.667,4394.0786};
									direction=193;
									positionTrigger[]={3653.3789,4476.6929};
									side=1;
									type=1;
									positionMove[]={3666.6494,4399.1563};
									moveTime=1;
								};
							};
						};
					};
				};
			};
		};
		safePositionAnchor[]={3448.1101,3627.1799};
		safePositionRadius=2000;
	};
};
class CfgWorldList
{
	class utes
	{
	};
};
class CfgMissions
{
	class Cutscenes
	{
		class UtesIntro1
		{
			directory="a3\map_stratis_scenes_f\scenes\intro1.Stratis";
		};
	};
};
class CfgSurfaces
{
	class Default
	{
	};
	class Water
	{
	};
	class UTGravel: Default
	{
		access=2;
		files="ut_sterk_*";
		rough=0.1;
		dust=0.34999999;
		soundEnviron="gravel";
		character="Empty";
		soundHit="hard_ground";
	};
	class UTRock: Default
	{
		access=2;
		files="ut_skala_*";
		rough=0.2;
		dust=0.07;
		soundEnviron="rock";
		character="Empty";
		soundHit="hard_ground";
	};
	class UTConcrete: Default
	{
		access=2;
		files="ut_beton_*";
		rough=0.079999998;
		dust=0.050000001;
		soundEnviron="concrete_ext";
		character="Empty";
		soundHit="hard_ground";
	};
	class UTBoulders: Default
	{
		access=2;
		files="ut_valouny_*";
		rough=0.1;
		dust=0.07;
		soundEnviron="rock";
		character="UTSparseGrassClutter";
		soundHit="hard_ground";
	};
	class UTGround: Default
	{
		access=2;
		files="ut_hlina_*";
		rough=0.1;
		dust=0.2;
		soundEnviron="dirt";
		character="UTSparseGrassClutter";
		soundHit="soft_ground";
	};
	class UTGrass: Default
	{
		access=2;
		files="ut_trava_*";
		rough=0.11;
		dust=0.1;
		soundEnviron="grass";
		character="UTGrassClutter";
		soundHit="soft_ground";
	};
	class UTHeather: Default
	{
		access=2;
		files="ut_vres_*";
		rough=0.14;
		dust=0.1;
		soundEnviron="drygrass";
		character="UTHeatherClutter";
		soundHit="soft_ground";
	};
	class UTWeeds: Default
	{
		access=2;
		files="ut_plevel_*";
		rough=0.11;
		dust=0.1;
		soundEnviron="drygrass";
		character="UTWeedsClutter";
		soundHit="soft_ground";
	};
	class UTForest: Default
	{
		access=2;
		files="ut_les_*";
		rough=0.2;
		dust=0.15000001;
		soundEnviron="forest";
		character="UTPineForestClutter";
		soundHit="soft_ground";
	};
};
class CfgSurfaceCharacters
{
	class UTSparseGrassClutter
	{
		names[]=
		{
			"UTGrassDryBunch"
		};
		probability[]={0.07};
	};
	class UTGrassClutter
	{
		probability[]={0.94999999,0.02,0.029999999};
		names[]=
		{
			"UTGrassDryBunch",
			"UTGrassDryLongBunch",
			"UTAutumnFlowers"
		};
	};
	class UTHeatherClutter
	{
		probability[]={0.1,0.25,0.07,0.029999999};
		names[]=
		{
			"UTBlueBerry",
			"UTHeatherBrush",
			"UTGrassDryBunch",
			"UTWeedSedge"
		};
	};
	class UTWeedsClutter
	{
		probability[]={0.60000002,0.30000001,0.050000001,0.029999999,0.02};
		names[]=
		{
			"UTAutumnFlowers",
			"UTGrassDryLongBunch",
			"UTWeedTall",
			"UTWeedDead",
			"UTWeedDeadSmall"
		};
	};
	class UTPineForestClutter
	{
		probability[]={0.1,0.15000001,0.2,0.050000001,0.001};
		names[]=
		{
			"UTBlueBerry",
			"UTFernAutumn",
			"UTFernAutumnTall",
			"UTGrassDryBunch",
			"UTMushroomsPrasivka"
		};
	};
};
