/*
	File: killHouseWater1.sqf
	Author: Joris-Jan van 't Land

	Description:
	Object set for the water-based Kill House on Utes.
	
	Returns:
	Object set array	
*/

private ["_objs"];
_objs =
[
	["Land_BoatSmall_2a",[25.9502,34.8223,0.0789905],185.124,1,0],
	["Land_BoatSmall_2b",[22.8723,42.8594,-0.00503349],253.548,1,0],
	["SKODAWreck",[-203.204,-15.5513,3.3],126.986,1,0]
];

_objs