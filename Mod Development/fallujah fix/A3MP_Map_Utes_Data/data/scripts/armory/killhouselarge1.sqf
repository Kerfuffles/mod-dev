/*
	File: killHouseSmall1.sqf
	Author: Joris-Jan van 't Land

	Description:
	Object set for the large land-based Kill House on Utes.
	
	Returns:
	Object set array	
*/

private ["_objs"];
_objs =
[
	["hiluxWreck",[-9.43091,-30.1558,0.0351219],243.329,1,0],
	["Wooden_barrels",[-127.077,172.382,3.3],11.785,1,0],
	["Land_Ind_BoardsPack2",[-123.969,172.998,3.3],18.2442,1,0],
	["Wooden_barrel",[-126.964,170.981,3.3],12.4241,1,0],
	["Wooden_barrel",[-126.11,171.811,3.3],12.7617,1,0],
	["Land_Ind_BoardsPack1",[-125.113,175.669,3.3],49.6325,1,0],
	["Land_Ind_BoardsPack1",[-122.708,178.376,3.3],315.619,1,0]
];

_objs