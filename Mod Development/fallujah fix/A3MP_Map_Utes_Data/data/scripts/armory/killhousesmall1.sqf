/*
	File: killHouseSmall1.sqf
	Author: Joris-Jan van 't Land

	Description:
	Object set for the small land-based Kill House on Utes.
	
	Returns:
	Object set array	
*/

private ["_objs"];
_objs =
[
	["Land_CncBlock_D",[-5.95996,-4.74512,0],202.724,1,0],
	["Land_CncBlock",[-9.66016,-2.48242,0],33.674,1,0],
	["Land_CncBlock",[-12.8574,0.356201,0],228.031,1,0],
	["datsun02Wreck",[-12.0981,-5.11621,0],104.334,1,0],
	["hiluxWreck",[-16.5645,0.69043,0],339.349,1,0]
];

_objs