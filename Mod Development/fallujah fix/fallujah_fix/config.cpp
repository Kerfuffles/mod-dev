class CfgPatches
{
	class fallujah_v1_0_fix
	{
		units[]=
		{
			"fallujah"
		};
		weapons[]={};
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"Utes"
		};
	};
};
class CfgVehicles
{
};
class CfgWorlds
{
	class DefaultWorld
	{
		class Weather;
	};
	class CAWorld: DefaultWorld
	{
		class Grid
		{
		};
		class DayLightingBrightAlmost;
		class DayLightingRainy;
		class DefaultClutter;
		class Weather: Weather
		{
			class Lighting;
		};
	};
	class DefaultLighting;
	class Utes: CAWorld
	{
	};
	class fallujah: Utes
	{
		author="Shezan74 + Fixed by Alduric for ArmA 3";
		access=3;
		worldId=4;
		cutscenes[]={};
		description="Fallujah";
		icon="";
		worldName="shez\fallujah\fallujah.wrp";
		pictureMap="fallujah_fix\image_map.paa";
		pictureShot="fallujah_fix\image_mission.paa";
		plateFormat="ML$ - #####";
		plateLetters="ABCDEGHIKLMNOPRSTVXZ";
		longitude=25;
		latitude=-36;
		class Grid: Grid
		{
			class Zoom1
			{
				zoomMax=9.9999997e-005;
				format="XY";
				formatX="0000";
				formatY="0000";
				stepX=10;
				stepY=-10;
			};
			class Zoom2
			{
				zoomMax=0.15000001;
				format="XY";
				formatX="000";
				formatY="000";
				stepX=100;
				stepY=-100;
			};
			class Zoom3
			{
				zoomMax=1;
				format="XY";
				formatX="00";
				formatY="00";
				stepX=1000;
				stepY=-1000;
			};
		};
		startTime="12:00";
		startDate="11/01/2010";
		startWeather=0;
		startFog=0;
		forecastWeather=0;
		forecastFog=0;
		centerPosition[]={8554,1055,20};
		seagullPos[]={8554,1055,20};
		ilsPosition[]={8447,1354};
		ilsDirection[]={0.5,0.079999998,-0.866};
		ilsTaxiIn[]={8136,1751,8300,1488,8336,1486,8366,1492};
		ilsTaxiOff[]={8447,1354,7965,2184,7923,2160,7976,2028,8136,1751};
		drawTaxiway="true";
		class ReplaceObjects
		{
		};
		class Sounds
		{
			sounds[]={};
		};
		class Animation
		{
			vehicles[]={};
		};
		class Lighting: DefaultLighting
		{
			groundReflection[]={0.059999999,0.059999999,0.029999999};
		};
		class DayLightingBrightAlmost: DayLightingBrightAlmost
		{
			deepNight[]=
			{
				-15,
				{0.050000001,0.050000001,0.059999999},
				{0.001,0.001,0.0020000001},
				{0.02,0.02,0.050000001},
				{0.003,0.003,0.003},
				{9.9999997e-005,9.9999997e-005,0.00019999999},
				{9.9999997e-005,9.9999997e-005,0.00019999999},
				0
			};
			fullNight[]=
			{
				-5,
				{0.050000001,0.050000001,0.050000001},
				{0.02,0.02,0.02},
				{0.039999999,0.039999999,0.039999999},
				{0.039999999,0.039999999,0.039999999},
				{0.0099999998,0.0099999998,0.02},
				{0.079999998,0.059999999,0.059999999},
				0
			};
			sunMoon[]=
			{
				-3.75,
				{0.045000002,0.039999999,0.039999999},
				{0.039999999,0.039999999,0.039999999},
				{0.045000002,0.039999999,0.039999999},
				{0.039999999,0.039999999,0.039999999},
				{0.039999999,0.035,0.039999999},
				{0.1,0.079999998,0.090000004},
				0.5
			};
			earlySun[]=
			{
				-2.5,
				{0.12,0.1,0.1},
				{0.079999998,0.059999999,0.07},
				{0.12,0.1,0.1},
				{0.079999998,0.059999999,0.07},
				{0.079999998,0.07,0.079999998},
				{0.1,0.1,0.12},
				1
			};
			sunrise[]=
			{
				0,
				
				{
					{0.69999999,0.44999999,0.44999999},
					"5.16+(-4)"
				},
				
				{
					{0.07,0.090000004,0.12},
					"4.0+(-4)"
				},
				
				{
					{0.60000002,0.47,0.25},
					"4.66+(-4)"
				},
				
				{
					{0.1,0.090000004,0.1},
					"4.3+(-4)"
				},
				
				{
					{0.5,0.40000001,0.40000001},
					"6.49+(-4)"
				},
				
				{
					{0.88,0.50999999,0.23999999},
					"8.39+(-4)"
				},
				1
			};
			earlyMorning[]=
			{
				3,
				
				{
					{0.64999998,0.55000001,0.55000001},
					"6.04+(-4)"
				},
				
				{
					{0.079999998,0.090000004,0.11},
					"4.5+(-4)"
				},
				
				{
					{0.55000001,0.47,0.25},
					"5.54+(-4)"
				},
				
				{
					{0.1,0.090000004,0.1},
					"5.02+(-4)"
				},
				
				{
					{0.5,0.40000001,0.40000001},
					"7.05+(-4)"
				},
				
				{
					{0.88,0.50999999,0.23999999},
					"8.88+(-4)"
				},
				1
			};
			midMorning[]=
			{
				8,
				
				{
					{0.98000002,0.85000002,0.80000001},
					"8.37+(-4)"
				},
				
				{
					{0.079999998,0.090000004,0.11},
					"6.42+(-4)"
				},
				
				{
					{0.87,0.47,0.25},
					"7.87+(-4)"
				},
				
				{
					{0.090000004,0.090000004,0.1},
					"6.89+(-4)"
				},
				
				{
					{0.5,0.40000001,0.40000001},
					"8.9+(-4)"
				},
				
				{
					{0.88,0.50999999,0.23999999},
					"10.88+(-4)"
				},
				1
			};
			morning[]=
			{
				16,
				
				{
					{1,1,0.89999998},
					"13.17+(-4)"
				},
				
				{
					{0.17,0.18000001,0.19},
					"10.26+(-4)"
				},
				
				{
					{1,1,0.89999998},
					"12.67+(-4)"
				},
				
				{
					{0.17,0.18000001,0.19},
					"11.71+(-4)"
				},
				
				{
					{0.15000001,0.15000001,0.15000001},
					"12.42+(-4)"
				},
				
				{
					{0.17,0.17,0.15000001},
					"14.42+(-4)"
				},
				1
			};
			noon[]=
			{
				45,
				
				{
					{1,1,1},
					"17+(-4)"
				},
				
				{
					{1,1.3,1.55},
					"13.5+(-4)"
				},
				
				{
					{1,1,1},
					"15+(-4)"
				},
				
				{
					{0.36000001,0.37,0.38},
					"13.5+(-4)"
				},
				
				{
					{1,1,1},
					"16+(-4)"
				},
				
				{
					{1,1,1},
					"17+(-4)"
				},
				1
			};
		};
		class DayLightingRainy: DayLightingRainy
		{
			deepNight[]=
			{
				-15,
				{0.0034,0.0034,0.0040000002},
				{0.003,0.003,0.003},
				{0.0034,0.0034,0.0040000002},
				{0.003,0.003,0.003},
				{0.001,0.001,0.0020000001},
				{0.001,0.001,0.0020000001},
				0
			};
			fullNight[]=
			{
				-5,
				{0.023,0.023,0.023},
				{0.02,0.02,0.02},
				{0.023,0.023,0.023},
				{0.02,0.02,0.02},
				{0.0099999998,0.0099999998,0.02},
				{0.079999998,0.059999999,0.059999999},
				0
			};
			sunMoon[]=
			{
				-3.75,
				{0.039999999,0.039999999,0.050000001},
				{0.039999999,0.039999999,0.050000001},
				{0.039999999,0.039999999,0.050000001},
				{0.039999999,0.039999999,0.050000001},
				{0.039999999,0.035,0.039999999},
				{0.11,0.079999998,0.090000004},
				0.5
			};
			earlySun[]=
			{
				-2.5,
				{0.068899997,0.068899997,0.080399998},
				{0.059999999,0.059999999,0.07},
				{0.068899997,0.068899997,0.080399998},
				{0.059999999,0.059999999,0.07},
				{0.079999998,0.07,0.079999998},
				{0.14,0.1,0.12},
				0.5
			};
			earlyMorning[]=
			{
				0,
				
				{
					{1,1,1},
					"(-4)+3.95"
				},
				
				{
					{1,1,1},
					"(-4)+3.0"
				},
				
				{
					{1,1,1},
					"(-4)+3.95"
				},
				
				{
					{1,1,1},
					"(-4)+3.0"
				},
				
				{
					{1,1,1},
					"(-4)+4"
				},
				
				{
					{1,1,1},
					"(-4)+5.5"
				},
				1
			};
			morning[]=
			{
				5,
				
				{
					{1,1,1},
					"(-4)+5.7"
				},
				
				{
					{1,1,1},
					"(-4)+4.5"
				},
				
				{
					{1,1,1},
					"(-4)+5.7"
				},
				
				{
					{1,1,1},
					"(-4)+4.5"
				},
				
				{
					{1,1,1},
					"(-4)+7"
				},
				
				{
					{1,1,1},
					"(-4)+8"
				},
				1
			};
			lateMorning[]=
			{
				25,
				
				{
					{1,1,1},
					"(-4)+10.45"
				},
				
				{
					{1,1,1},
					"(-4)+9.75"
				},
				
				{
					{1,1,1},
					"(-4)+10.45"
				},
				
				{
					{1,1,1},
					"(-4)+9.75"
				},
				
				{
					{1,1,1},
					"(-4)+12"
				},
				
				{
					{1,1,1},
					"(-4)+12.75"
				},
				1
			};
			noon[]=
			{
				70,
				
				{
					{1,1,1},
					"(-4)+12.5"
				},
				
				{
					{1,1,1},
					"(-4)+11"
				},
				
				{
					{1,1,1},
					"(-4)+12"
				},
				
				{
					{1,1,1},
					"(-4)+11"
				},
				
				{
					{1,1,1},
					"(-4)+13.5"
				},
				
				{
					{1,1,1},
					"(-4)+14"
				},
				1
			};
		};
		class Weather: Weather
		{
			class Lighting: Lighting
			{
				class BrightAlmost: DayLightingBrightAlmost
				{
					overcast=0;
				};
				class Rainy: DayLightingRainy
				{
					overcast=1;
				};
			};
		};
		clutterGrid=1;
		clutterDist=150;
		noDetailDist=95;
		fullDetailDist=55;
		midDetailTexture="shez\fallujah\data\si_middle_mco.paa";
		minTreesInForestSquare=5;
		minRocksInRockSquare=4;
		class Clutter
		{
			class si_GrassTall: DefaultClutter
			{
				model="ca\plants2\clutter\c_GrassTall.p3d";
				affectedByWind=0.40000001;
				swLighting=1;
				scaleMin=0.69999999;
				scaleMax=0.89999998;
			};
			class si_StubbleClutter: DefaultClutter
			{
				model="ca\plants2\clutter\c_stubble.p3d";
				affectedByWind=0.1;
				swLighting=1;
				scaleMin=0.89999998;
				scaleMax=1.5;
			};
			class si_AutumnFlowers: DefaultClutter
			{
				model="ca\plants2\clutter\c_autumn_flowers.p3d";
				affectedByWind=0.40000001;
				swLighting=1;
				scaleMin=0.2;
				scaleMax=0.5;
			};
			class si_GrassBunch: DefaultClutter
			{
				model="ca\plants2\clutter\c_GrassBunch.p3d";
				affectedByWind=0.55000001;
				swLighting=1;
				scaleMin=0.60000002;
				scaleMax=1.5;
			};
			class si_GrassCrooked: DefaultClutter
			{
				model="ca\plants2\clutter\c_GrassCrooked.p3d";
				affectedByWind=0.30000001;
				swLighting=1;
				scaleMin=0.69999999;
				scaleMax=2.5;
			};
			class si_GrassCrookedGreen: DefaultClutter
			{
				model="ca\plants2\clutter\c_GrassCrookedGreen.p3d";
				affectedByWind=0.30000001;
				swLighting=1;
				scaleMin=0.89999998;
				scaleMax=2.5;
			};
			class si_GrassCrookedForest: DefaultClutter
			{
				model="ca\plants2\clutter\c_GrassCrookedForest.p3d";
				affectedByWind=0.30000001;
				swLighting=1;
				scaleMin=0.80000001;
				scaleMax=1.4;
			};
			class si_WeedDead: DefaultClutter
			{
				model="ca\plants2\clutter\c_WeedDead.p3d";
				affectedByWind=0.30000001;
				swLighting=1;
				scaleMin=0.75;
				scaleMax=1.1;
			};
			class si_WeedDeadSmall: DefaultClutter
			{
				model="ca\plants2\clutter\c_WeedDead2.p3d";
				affectedByWind=0.30000001;
				swLighting=1;
				scaleMin=0.75;
				scaleMax=0.89999998;
			};
			class si_HeatherBrush: DefaultClutter
			{
				model="ca\plants2\clutter\c_caluna.p3d";
				affectedByWind=0.15000001;
				swLighting=1;
				scaleMin=0.89999998;
				scaleMax=1.8;
				surfaceColor[]={0.52999997,0.5,0.37,1};
			};
			class si_WeedSedge: DefaultClutter
			{
				model="ca\plants2\clutter\c_weed3.p3d";
				affectedByWind=0.2;
				swLighting=1;
				scaleMin=0.5;
				scaleMax=0.85000002;
			};
			class si_WeedTall: DefaultClutter
			{
				model="ca\plants2\clutter\c_weed2.p3d";
				affectedByWind=0.30000001;
				swLighting=1;
				scaleMin=0.80000001;
				scaleMax=1.1;
			};
			class si_BlueBerry: DefaultClutter
			{
				model="ca\plants2\clutter\c_BlueBerry.p3d";
				affectedByWind=0.050000001;
				swLighting=1;
				scaleMin=0.85000002;
				scaleMax=1.3;
			};
			class si_RaspBerry: DefaultClutter
			{
				model="ca\plants2\clutter\c_raspBerry.p3d";
				affectedByWind=0;
				swLighting=1;
				scaleMin=0.80000001;
				scaleMax=1.2;
			};
			class si_FernAutumn: DefaultClutter
			{
				model="ca\plants2\clutter\c_fern.p3d";
				affectedByWind=0.1;
				scaleMin=0.60000002;
				scaleMax=1.2;
			};
			class si_FernAutumnTall: DefaultClutter
			{
				model="ca\plants2\clutter\c_fernTall.p3d";
				affectedByWind=0.15000001;
				scaleMin=0.25;
				scaleMax=0.5;
			};
			class si_SmallPicea: DefaultClutter
			{
				model="ca\plants2\clutter\c_picea.p3d";
				affectedByWind=0.050000001;
				scaleMin=0.75;
				scaleMax=1.25;
			};
			class si_PlantWideLeaf: DefaultClutter
			{
				model="ca\plants2\clutter\c_WideLeafPlant.p3d";
				affectedByWind=0.1;
				scaleMin=1;
				scaleMax=1;
			};
			class si_MushroomsHorcak: DefaultClutter
			{
				model="ca\plants2\clutter\c_MushroomHorcak.p3d";
				affectedByWind=0;
				scaleMin=0.85000002;
				scaleMax=1.25;
			};
			class si_MushroomsPrasivka: si_MushroomsHorcak
			{
				model="ca\plants2\clutter\c_MushroomPrasivky.p3d";
			};
			class si_MushroomsBabka: si_MushroomsHorcak
			{
				model="ca\plants2\clutter\c_MushroomBabka.p3d";
			};
			class si_MushroomsMuchomurka: si_MushroomsHorcak
			{
				model="ca\plants2\clutter\c_MushroomMuchomurka.p3d";
			};
		};
		class Subdivision
		{
			class Fractal
			{
				rougness=5;
				maxRoad=0.02;
				maxTrack=0.5;
				maxSlopeFactor=0.050000001;
			};
			class WhiteNoise
			{
				rougness=2;
				maxRoad=0.0099999998;
				maxTrack=0.050000001;
				maxSlopeFactor=0.0024999999;
			};
			minY=0;
			minSlope=0.02;
		};
		class Ambient
		{
			class Mammals
			{
				radius=200;
				cost="(1 + forest + trees) * (0.5 + (0.5 * night)) * (1 - sea) * (1 - houses)";
				class Species
				{
					class Rabbit
					{
						probability=0.2;
						cost=1;
					};
				};
			};
			class BigBirds
			{
				radius=300;
				cost="((1 + forest + trees) - ((2 * rain)) - houses) * (1 - night) * (1 - sea)";
				class Species
				{
					class Hawk
					{
						probability=0.2;
						cost=1;
					};
				};
			};
			class Birds
			{
				radius=170;
				cost="(1 - night) * ((1 + (3 * sea)) - (2 * rain))";
				class Species
				{
					class Crow
					{
						probability=0.2;
						cost=1;
					};
				};
			};
			class BigInsects
			{
				radius=20;
				cost="(5 - (2 * houses)) * (1 - night) * (1 - rain) * (1 - sea) * (1 - windy)";
				class Species
				{
					class DragonFly
					{
						probability="0.6 - (meadow * 0.5) + (forest * 0.4)";
						cost=1;
					};
					class ButterFly
					{
						probability="0.4 + (meadow * 0.5) - (forest * 0.4)";
						cost=1;
					};
				};
			};
			class BigInsectsAquatic
			{
				radius=20;
				cost="(3 * sea) * (1 - night) * (1 - rain) * (1 - windy)";
				class Species
				{
					class DragonFly
					{
						probability=1;
						cost=1;
					};
				};
			};
			class SmallInsects
			{
				radius=3;
				cost="(12 - 8 * hills) * (1 - night) * (1 - rain) * (1 - sea) * (1 - windy)";
				class Species
				{
					class HouseFly
					{
						probability="deadBody + (1 - deadBody) * (0.5 - forest * 0.1 - meadow * 0.2)";
						cost=1;
					};
					class HoneyBee
					{
						probability="(1 - deadBody) * (0.5 - forest * 0.1 + meadow * 0.2)";
						cost=1;
					};
					class Mosquito
					{
						probability="(1 - deadBody) * (0.2 * forest)";
						cost=1;
					};
				};
			};
			class NightInsects
			{
				radius=3;
				cost="(9 - 8 * hills) * night * (1 - rain) * (1 - sea) * (1 - windy)";
				class Species
				{
					class Mosquito
					{
						probability=1;
						cost=1;
					};
				};
			};
			class WindClutter
			{
				radius=10;
				cost="((20 - 5 * rain) * (3 * (windy factor [0.2, 0.5]))) * (1 - sea)";
				class Species
				{
					class FxWindGrass1
					{
						probability="0.4 - 0.2 * hills - 0.2 * trees";
						cost=1;
					};
					class FxWindGrass2
					{
						probability="0.4 - 0.2 * hills - 0.2 * trees";
						cost=1;
					};
					class FxWindRock1
					{
						probability="0.4 * hills";
						cost=1;
					};
				};
			};
			class NoWindClutter
			{
				radius=15;
				cost=8;
				class Species
				{
					class FxWindPollen1
					{
						probability=1;
						cost=1;
					};
				};
			};
		};
		class Armory
		{
		};
		safePositionAnchor[]={10000,10000};
		safePositionRadius=2000;
		class Names
		{
			class Fallujah_text
			{
				name="Al-Fallujah";
				position[]={5479.2202,5103.3101};
				type="NameCity";
				radiusA=100;
				radiusB=100;
				angle=0;
			};
			class Jolan_text
			{
				name="Jolan";
				position[]={3314.3799,5335.3301};
				type="NameCity";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class Mualimeen_text
			{
				name="Mualimeen";
				position[]={4311.1201,6338.8701};
				type="NameCity";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class Muhandisin_text
			{
				name="Muhandisin";
				position[]={4387.96,5428.25};
				type="NameCity";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class Shurta_text
			{
				name="Shurta";
				position[]={5273.6802,6270.6602};
				type="NameCity";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class Jeghaifi_text
			{
				name="Jeghaifi";
				position[]={6014.5698,6381.3501};
				type="NameCity";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class Askari_text
			{
				name="Askari";
				position[]={6691.4199,5709.5601};
				type="NameCity";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class Resafa_text
			{
				name="Resafa";
				position[]={3866.0801,3968.8701};
				type="NameCity";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class Nazal_text
			{
				name="Nazal Old City";
				position[]={4668.6802,4416.8398};
				type="NameCity";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class Industrial_text
			{
				name="Industrial Park";
				position[]={5945.9199,4683.8101};
				type="NameCity";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class Sinai_text
			{
				name="Sinai";
				position[]={5325.2998,3169.46};
				type="NameCity";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class Shuhada_text
			{
				name="Shuhada";
				position[]={5591.6001,2971.24};
				type="NameCity";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class Euphrate_text
			{
				name="Euphrate River";
				position[]={2919.8101,3540.1699};
				type="NameMarine";
				radiusA=50;
				radiusB=50;
				angle=90;
			};
			class Barrage_text
			{
				name="Fallujah Barrage";
				position[]={3579.99,749.26501};
				type="ViewPoint";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class Dam1_text
			{
				name="Dam";
				position[]={3893.95,634.479};
				type="NameMarine";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class Dam2_text
			{
				name="Dam";
				position[]={3507.4299,627.37598};
				type="NameMarine";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class Airport_text
			{
				name="Airport";
				position[]={7901.8799,1740.11};
				type="ViewPoint";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class TrainStation_text
			{
				name="Train Station";
				position[]={3561.1001,6771.7402};
				type="ViewPoint";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class FOB_text
			{
				name="FOB";
				position[]={5613.5801,9850.3496};
				type="ViewPoint";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class Highway_text
			{
				name="Highway 11";
				position[]={8937.0098,5339.6299};
				type="ViewPoint";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class GasStation_text
			{
				name="Gas Station";
				position[]={8300,3393.8501};
				type="ViewPoint";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class Old_Bridge_text
			{
				name="Old Bridge";
				position[]={2729.4099,4815.8398};
				type="ViewPoint";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class New_Bridge_text
			{
				name="New Bridge";
				position[]={2861.74,4245.6802};
				type="ViewPoint";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class Hospital_text
			{
				name="Hospital";
				position[]={2425.1299,4912.0801};
				type="ViewPoint";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class Jolan_Park_text
			{
				name="Park";
				position[]={3617.48,5231.02};
				type="VegetationPalm";
				radiusA=50;
				radiusB=50;
				angle=0;
			};
			class fal_a1
			{
				name="";
				position[]={3063.78,6021.6499};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a2",
					"fal_a7",
					"fal_a8"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class fal_a2
			{
				name="";
				position[]={3891.8999,6126.04};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a1",
					"fal_a3",
					"fal_a7",
					"fal_a8",
					"fal_a9"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class fal_a3
			{
				name="";
				position[]={4575.8501,6191.2998};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a2",
					"fal_a4",
					"fal_a8",
					"fal_a9",
					"fal_a10"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class fal_a4
			{
				name="";
				position[]={5367.3501,6269.2002};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a3",
					"fal_a5",
					"fal_a9",
					"fal_a10",
					"fal_a11"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class fal_a5
			{
				name="";
				position[]={6129.3398,6222.9102};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a4",
					"fal_a6",
					"fal_a10",
					"fal_a11",
					"fal_a12"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class fal_a6
			{
				name="";
				position[]={6854.7202,6149.23};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a5",
					"fal_a11",
					"fal_a12"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class fal_a7
			{
				name="";
				position[]={3292.8301,5300.9399};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a1",
					"fal_a2",
					"fal_a8",
					"fal_a13",
					"fal_a14"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class fal_a8
			{
				name="";
				position[]={3956.5,5376.98};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a1",
					"fal_a2",
					"fal_a3",
					"fal_a7",
					"fal_a9",
					"fal_a13",
					"fal_a14",
					"fal_a15"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class fal_a9
			{
				name="";
				position[]={4650.6602,5458.9302};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a2",
					"fal_a3",
					"fal_a4",
					"fal_a8",
					"fal_a10",
					"fal_a14",
					"fal_a15",
					"fal_a16"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class fal_a10
			{
				name="";
				position[]={5327.71,5500.9902};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a3",
					"fal_a4",
					"fal_a5",
					"fal_a9",
					"fal_a11",
					"fal_a15",
					"fal_a16",
					"fal_a17"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class fal_a11
			{
				name="";
				position[]={6043.4902,5551.8701};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a4",
					"fal_a5",
					"fal_a6",
					"fal_a10",
					"fal_a12",
					"fal_a16",
					"fal_a17"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class fal_a12
			{
				name="";
				position[]={6758.9102,5468.5};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a5",
					"fal_a6",
					"fal_a11",
					"fal_a16",
					"fal_a17"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class fal_a13
			{
				name="";
				position[]={3500.77,4463.9399};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a7",
					"fal_a8",
					"fal_a14",
					"fal_a18"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class fal_a14
			{
				name="";
				position[]={4224.04,4555.1499};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a7",
					"fal_a8",
					"fal_a9",
					"fal_a13",
					"fal_a15",
					"fal_a18",
					"fal_a19"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class fal_a15
			{
				name="";
				position[]={4875.02,4639.2998};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a8",
					"fal_a9",
					"fal_a10",
					"fal_a14",
					"fal_a16",
					"fal_a19",
					"fal_a20"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class fal_a16
			{
				name="";
				position[]={5610.2202,4745.5};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a9",
					"fal_a10",
					"fal_a11",
					"fal_a15",
					"fal_a17",
					"fal_a19",
					"fal_a20"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class fal_a17
			{
				name="";
				position[]={6256.5298,4655.6602};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a9",
					"fal_a10",
					"fal_a11",
					"fal_a16",
					"fal_a20"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class fal_a18
			{
				name="";
				position[]={4077.0901,3630.3701};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a13",
					"fal_a14",
					"fal_a19",
					"fal_a21",
					"fal_a22"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class fal_a19
			{
				name="";
				position[]={4804.1401,3861.21};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a14",
					"fal_a15",
					"fal_a16",
					"fal_a18",
					"fal_a20",
					"fal_a21",
					"fal_a22",
					"fal_a23"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class fal_a20
			{
				name="";
				position[]={5634.7798,4013.73};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a15",
					"fal_a16",
					"fal_a17",
					"fal_a19",
					"fal_a22",
					"fal_a23"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class fal_a21
			{
				name="";
				position[]={3755.73,3021.51};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a18",
					"fal_a22"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class fal_a22
			{
				name="";
				position[]={4693.29,3057.4199};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a18",
					"fal_a19",
					"fal_a20",
					"fal_a21",
					"fal_a23",
					"fal_a24"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class fal_a23
			{
				name="";
				position[]={5659.27,3227.3201};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a19",
					"fal_a20",
					"fal_a22",
					"fal_a24"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class fal_a24
			{
				name="";
				position[]={5426.0498,2598.98};
				type="CityCenter";
				neighbors[]=
				{
					"fal_a22",
					"fal_a23"
				};
				radiusA=10;
				radiusB=10;
				angle=0;
			};
			class FlatArea1
			{
				name="";
				position[]={4766.2402,2838.28};
				type="FlatArea";
				radiusA=200;
				radiusB=200;
			};
			class FlatArea2
			{
				name="";
				position[]={4481.4199,2548.3899};
				type="FlatArea";
				radiusA=200;
				radiusB=200;
			};
			class FlatArea3
			{
				name="";
				position[]={4230.3999,1597.9399};
				type="FlatArea";
				radiusA=100;
				radiusB=100;
			};
			class FlatArea4
			{
				name="";
				position[]={5869.1602,411.35199};
				type="FlatArea";
				radiusA=200;
				radiusB=200;
			};
			class FlatArea5
			{
				name="";
				position[]={6516.2202,537.55603};
				type="FlatArea";
				radiusA=100;
				radiusB=100;
			};
			class FlatArea6
			{
				name="";
				position[]={6500.0498,877};
				type="FlatArea";
				radiusA=100;
				radiusB=100;
			};
			class FlatArea7
			{
				name="";
				position[]={8813.9697,3277.6101};
				type="FlatArea";
				radiusA=100;
				radiusB=100;
			};
			class FlatArea8
			{
				name="";
				position[]={8578.4502,4855.2798};
				type="FlatArea";
				radiusA=100;
				radiusB=100;
			};
			class FlatArea9
			{
				name="";
				position[]={9068.8496,7404.0698};
				type="FlatArea";
				radiusA=100;
				radiusB=100;
			};
			class FlatArea10
			{
				name="";
				position[]={9210.8096,8330.0195};
				type="FlatArea";
				radiusA=150;
				radiusB=150;
			};
			class FlatArea11
			{
				name="";
				position[]={6845.9199,9139.8301};
				type="FlatArea";
				radiusA=150;
				radiusB=150;
			};
			class FlatArea12
			{
				name="";
				position[]={8681.6904,8820.4199};
				type="FlatArea";
				radiusA=200;
				radiusB=200;
			};
			class FlatArea13
			{
				name="";
				position[]={5064.9902,9514.0801};
				type="FlatArea";
				radiusA=100;
				radiusB=100;
			};
			class FlatArea14
			{
				name="";
				position[]={1667.6801,7968.6699};
				type="FlatArea";
				radiusA=100;
				radiusB=100;
			};
			class FlatArea15
			{
				name="";
				position[]={741.73102,7562.1602};
				type="FlatArea";
				radiusA=100;
				radiusB=100;
			};
			class FlatArea16
			{
				name="";
				position[]={2119.3701,4151.9399};
				type="FlatArea";
				radiusA=100;
				radiusB=100;
			};
			class FlatArea17
			{
				name="";
				position[]={1261.17,1932.24};
				type="FlatArea";
				radiusA=100;
				radiusB=100;
			};
			class FlatArea18
			{
				name="";
				position[]={325.53699,325.53601};
				type="FlatArea";
				radiusA=100;
				radiusB=100;
			};
			class FlatArea19
			{
				name="";
				position[]={4490.71,848.19897};
				type="FlatArea";
				radiusA=200;
				radiusB=200;
			};
			class FlatArea20
			{
				name="";
				position[]={3832.54,2016.12};
				type="FlatArea";
				radiusA=200;
				radiusB=200;
			};
			class FlatAreaCity1
			{
				name="";
				position[]={3904.9299,3837.6499};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity2
			{
				name="";
				position[]={3767.1399,4007.52};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity3
			{
				name="";
				position[]={3405.5801,4018.04};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity4
			{
				name="";
				position[]={3409.53,4614.8901};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity5
			{
				name="";
				position[]={2969.21,4606.2002};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity6
			{
				name="";
				position[]={2758.1399,5061.54};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity7
			{
				name="";
				position[]={2844.3,5734.2798};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity8
			{
				name="";
				position[]={2926.52,5772.23};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity9
			{
				name="";
				position[]={2871.97,5658.3901};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity10
			{
				name="";
				position[]={2702.01,5862.3501};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity11
			{
				name="";
				position[]={2525.72,5929.54};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity12
			{
				name="";
				position[]={2549.4399,6296.3501};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity13
			{
				name="";
				position[]={1926.5,6602.2798};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity14
			{
				name="";
				position[]={3159.72,6570.6602};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity15
			{
				name="";
				position[]={3502.8101,6610.1899};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity16
			{
				name="";
				position[]={3901.24,6667.8999};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity17
			{
				name="";
				position[]={4200.8501,6308.21};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity18
			{
				name="";
				position[]={5205.6099,6312.1602};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity19
			{
				name="";
				position[]={5581.9102,6009.3901};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity20
			{
				name="";
				position[]={5512.3398,5628.3501};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity21
			{
				name="";
				position[]={5725.1899,5715.6899};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity22
			{
				name="";
				position[]={6143.1802,5470.2402};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity23
			{
				name="";
				position[]={6665.7202,5354.04};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity24
			{
				name="";
				position[]={6735.29,5675.7798};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity25
			{
				name="";
				position[]={6651.4902,5643.3701};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity26
			{
				name="";
				position[]={6858.0498,5734.4502};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity27
			{
				name="";
				position[]={7374.9502,5641.2998};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity28
			{
				name="";
				position[]={6723.4302,6202.27};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity29
			{
				name="";
				position[]={6668.8799,6195.9502};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity30
			{
				name="";
				position[]={6721.8501,6292.3901};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity31
			{
				name="";
				position[]={6810.3901,6378.5601};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity32
			{
				name="";
				position[]={6626.1899,6269.4702};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity33
			{
				name="";
				position[]={6848.3301,6213.3398};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity34
			{
				name="";
				position[]={6006.4199,6519.2798};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity35
			{
				name="";
				position[]={6238.8398,6532.7202};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity37
			{
				name="";
				position[]={5241.98,6818.8901};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity36
			{
				name="";
				position[]={6418.29,6724.02};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity38
			{
				name="";
				position[]={4222.8501,6798.3301};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity39
			{
				name="";
				position[]={4289.9502,6908.4702};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity40
			{
				name="";
				position[]={3540.76,6925.6099};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity41
			{
				name="";
				position[]={2530.3501,7037.5898};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity42
			{
				name="";
				position[]={904.021,6367.9702};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity43
			{
				name="";
				position[]={1661.71,7570.1602};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity44
			{
				name="";
				position[]={637.31403,8196.5195};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity45
			{
				name="";
				position[]={328.17801,8715.79};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity46
			{
				name="";
				position[]={4183.3999,9695.5596};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity47
			{
				name="";
				position[]={3044.51,8950.2803};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity48
			{
				name="";
				position[]={7374.46,6560.1099};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity49
			{
				name="";
				position[]={7886.5801,5371.54};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity50
			{
				name="";
				position[]={8085.1899,3824.5701};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity51
			{
				name="";
				position[]={8650.5498,3520.46};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity52
			{
				name="";
				position[]={6821.7998,2418.3101};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity53
			{
				name="";
				position[]={6015.6001,660.98901};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity54
			{
				name="";
				position[]={9399.6104,1093.6801};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity55
			{
				name="";
				position[]={8805.6699,2265.23};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity56
			{
				name="";
				position[]={5651.52,1486.4301};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatAreaCity57
			{
				name="";
				position[]={651.11603,2076.6399};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatCityArea58
			{
				name="";
				position[]={2290.5901,2932.45};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatCityArea59
			{
				name="";
				position[]={513.40002,3352.1599};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class FlatCityArea60
			{
				name="";
				position[]={493.72699,5109.6699};
				type="FlatAreaCity";
				radiusA=50;
				radiusB=50;
			};
			class StrongPointArea1
			{
				name="";
				position[]={492.00201,8370.2998};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea2
			{
				name="";
				position[]={729.92297,8213.1504};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea3
			{
				name="";
				position[]={255.79201,8667.3604};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea4
			{
				name="";
				position[]={1035.12,7767.71};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea5
			{
				name="";
				position[]={1385.74,7471.27};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea6
			{
				name="";
				position[]={1712.45,7317.48};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea7
			{
				name="";
				position[]={1557.0601,7157.3101};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea8
			{
				name="";
				position[]={1530.77,6534.96};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea9
			{
				name="";
				position[]={2058.29,6550.8999};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea10
			{
				name="";
				position[]={2307.7,6456.8701};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea11
			{
				name="";
				position[]={2691.79,6491.9302};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea12
			{
				name="";
				position[]={2545.1699,5990.71};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea13
			{
				name="";
				position[]={3021.6899,5599.4502};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea14
			{
				name="";
				position[]={2761.9099,5681.5298};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea15
			{
				name="";
				position[]={2727.6499,5369.1602};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea16
			{
				name="";
				position[]={2982.6399,5285.4902};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea17
			{
				name="";
				position[]={3053.5601,5252.02};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea18
			{
				name="";
				position[]={2771.48,5119.7402};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea19
			{
				name="";
				position[]={2867.1001,4809.7598};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea20
			{
				name="";
				position[]={2628.8401,4777.8901};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea21
			{
				name="";
				position[]={2677.45,4623.2998};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea22
			{
				name="";
				position[]={2699.76,4367.5098};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea23
			{
				name="";
				position[]={2672.6699,4241.6001};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea24
			{
				name="";
				position[]={2691.79,4175.46};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea25
			{
				name="";
				position[]={3358.76,4349.1802};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea26
			{
				name="";
				position[]={3118.9099,4071.8701};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea27
			{
				name="";
				position[]={3350.79,4034.4199};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea28
			{
				name="";
				position[]={3703,4025.6499};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea29
			{
				name="";
				position[]={3694.6201,4100};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea30
			{
				name="";
				position[]={3775.71,3875.1201};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea31
			{
				name="";
				position[]={4054.8201,3879.1001};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea32
			{
				name="";
				position[]={3977.21,3712.9299};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea33
			{
				name="";
				position[]={3995.1201,3592.53};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea34
			{
				name="";
				position[]={4028.1201,3376.21};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea35
			{
				name="";
				position[]={3948.8501,3183.5601};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea36
			{
				name="";
				position[]={4100.6401,2931.5701};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea37
			{
				name="";
				position[]={4361.21,2985.75};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea38
			{
				name="";
				position[]={3793.8501,2670.99};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea39
			{
				name="";
				position[]={4513.4102,2837.54};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea40
			{
				name="";
				position[]={4636.1299,2949.1001};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea41
			{
				name="";
				position[]={4710.23,3113.25};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea42
			{
				name="";
				position[]={4635.3301,2589.71};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea43
			{
				name="";
				position[]={4484.7202,2304.4399};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea44
			{
				name="";
				position[]={4989.1299,2524.3701};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea45
			{
				name="";
				position[]={5229.79,2799.29};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea46
			{
				name="";
				position[]={5279.9902,2562.6201};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea47
			{
				name="";
				position[]={5850.54,2859.05};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea48
			{
				name="";
				position[]={5045.71,3015.24};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea49
			{
				name="";
				position[]={5886.3999,2510.03};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea50
			{
				name="";
				position[]={5741.3701,2381.73};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea51
			{
				name="";
				position[]={6249.77,2740.3201};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea52
			{
				name="";
				position[]={5533.9302,3317.6799};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea53
			{
				name="";
				position[]={5388.7002,3669.29};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea54
			{
				name="";
				position[]={4723.71,3819.6101};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea55
			{
				name="";
				position[]={4342.8101,3340.6101};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea56
			{
				name="";
				position[]={4616.7002,4271.8501};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea57
			{
				name="";
				position[]={4316.0601,4280.77};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea58
			{
				name="";
				position[]={4276.5698,4837.48};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea59
			{
				name="";
				position[]={4735.1802,4673.1401};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea60
			{
				name="";
				position[]={5071.5,4880.79};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea61
			{
				name="";
				position[]={5136.46,4218.3501};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea62
			{
				name="";
				position[]={5378.5098,4536.8301};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea63
			{
				name="";
				position[]={5579.79,4822.1899};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea64
			{
				name="";
				position[]={5674.0601,4589.0601};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea65
			{
				name="";
				position[]={5575.9702,4278.2202};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea66
			{
				name="";
				position[]={5387.4302,4333};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea67
			{
				name="";
				position[]={5595.0801,4107.52};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea68
			{
				name="";
				position[]={5830.75,4066.75};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea69
			{
				name="";
				position[]={5973.4302,4306.25};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea70
			{
				name="";
				position[]={5968.3398,4683.3301};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea71
			{
				name="";
				position[]={5894.4502,4893.5298};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea72
			{
				name="";
				position[]={5848.5898,4390.3301};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea73
			{
				name="";
				position[]={6384.9102,4366.1201};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea74
			{
				name="";
				position[]={6607.8501,4670.5898};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea75
			{
				name="";
				position[]={6270.2598,4885.8901};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea76
			{
				name="";
				position[]={6732.6899,4964.8701};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea77
			{
				name="";
				position[]={5519.9199,4545.75};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea78
			{
				name="";
				position[]={5569.6001,5012};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea79
			{
				name="";
				position[]={5248.5698,5186.5298};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea80
			{
				name="";
				position[]={5572.1499,5232.3901};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea81
			{
				name="";
				position[]={4670.21,5371.25};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea82
			{
				name="";
				position[]={4309.6899,5627.3101};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea83
			{
				name="";
				position[]={4221.79,5727.9502};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea84
			{
				name="";
				position[]={4941.5498,5659.1602};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea85
			{
				name="";
				position[]={3711.4399,5779.5098};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea86
			{
				name="";
				position[]={3151.6201,5625.1201};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea87
			{
				name="";
				position[]={3117.96,5746.8501};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea88
			{
				name="";
				position[]={3009.3999,5909.8398};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea89
			{
				name="";
				position[]={3401.0801,6149.2998};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea90
			{
				name="";
				position[]={3398.0901,6252.29};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea91
			{
				name="";
				position[]={3565.26,6265.23};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea92
			{
				name="";
				position[]={3629.4399,6248.3101};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea93
			{
				name="";
				position[]={3524.96,6460.2598};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea94
			{
				name="";
				position[]={3617.5,6343.8398};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea95
			{
				name="";
				position[]={3698.1001,6460.2598};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea96
			{
				name="";
				position[]={2969.72,6118.9502};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea97
			{
				name="";
				position[]={2775.1899,5835.3701};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea98
			{
				name="";
				position[]={2713,5748.7998};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea99
			{
				name="";
				position[]={2304.04,5664.2202};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea100
			{
				name="";
				position[]={1844.73,5374.7402};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea101
			{
				name="";
				position[]={1272.58,5157.1899};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea102
			{
				name="";
				position[]={1053.45,4792.23};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea103
			{
				name="";
				position[]={593.65997,4973.1201};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea104
			{
				name="";
				position[]={1424.25,3492.21};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea105
			{
				name="";
				position[]={766.90302,2874.3601};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea106
			{
				name="";
				position[]={1323.61,2204.27};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea107
			{
				name="";
				position[]={589.82703,2218.29};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea108
			{
				name="";
				position[]={1439.54,1213.16};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea109
			{
				name="";
				position[]={1101.9399,755.82098};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea110
			{
				name="";
				position[]={2089.24,656.45502};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea111
			{
				name="";
				position[]={2275.23,1261.5699};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea112
			{
				name="";
				position[]={1729.99,169.815};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea113
			{
				name="";
				position[]={3627.3,646.97198};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea114
			{
				name="";
				position[]={3362.75,620.67603};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea115
			{
				name="";
				position[]={4041.6699,654.14398};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea116
			{
				name="";
				position[]={5941.5898,810.599};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea117
			{
				name="";
				position[]={6365.7998,690.85101};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea118
			{
				name="";
				position[]={6740.3398,302.30301};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea119
			{
				name="";
				position[]={7458.8301,1028.4399};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea120
			{
				name="";
				position[]={8265.2197,1033.54};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea121
			{
				name="";
				position[]={7322.52,1781.33};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea122
			{
				name="";
				position[]={6712.3101,2173.7};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea123
			{
				name="";
				position[]={6331.4102,1256.47};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea124
			{
				name="";
				position[]={9309.0898,1186.24};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea125
			{
				name="";
				position[]={10143.9,1633.23};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea126
			{
				name="";
				position[]={9588.71,3217.0601};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea127
			{
				name="";
				position[]={8287.3301,3608.8999};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea128
			{
				name="";
				position[]={8384.5498,3236.76};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea129
			{
				name="";
				position[]={6044.77,3451.45};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea130
			{
				name="";
				position[]={5996.5098,3835.49};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea131
			{
				name="";
				position[]={7102.75,4880.4902};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea132
			{
				name="";
				position[]={7078.25,5284.6099};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea133
			{
				name="";
				position[]={6657.8101,5253.9902};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea134
			{
				name="";
				position[]={6227.1499,5188.6802};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea135
			{
				name="";
				position[]={8325.3203,5390.7402};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea136
			{
				name="";
				position[]={8735.5596,5253.9902};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea137
			{
				name="";
				position[]={8721.2803,6723.5298};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea138
			{
				name="";
				position[]={7978.3398,6807.21};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea139
			{
				name="";
				position[]={8968.2402,7527.6899};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea140
			{
				name="";
				position[]={9160.0898,8846.1904};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea141
			{
				name="";
				position[]={9200.9199,9342.1602};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea142
			{
				name="";
				position[]={7398.6899,9968.75};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea143
			{
				name="";
				position[]={6649.6401,9685.0498};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea144
			{
				name="";
				position[]={5398.5,9229.9004};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class StrongPointArea145
			{
				name="";
				position[]={3751.3899,9605.4502};
				type="StrongPointArea";
				radiusA=20;
				radiusB=20;
			};
			class FlatAreaCitySmall1
			{
				name="";
				position[]={2648.27,6363.29};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall2
			{
				name="";
				position[]={2595.5601,6069.7202};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall3
			{
				name="";
				position[]={2910.99,6102.0601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall4
			{
				name="";
				position[]={2677.6499,5945.3398};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall5
			{
				name="";
				position[]={2778.6499,5867.73};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall6
			{
				name="";
				position[]={2829.3999,5647.3301};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall7
			{
				name="";
				position[]={3045.3201,5655.7798};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall8
			{
				name="";
				position[]={2899.55,5474.6899};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall9
			{
				name="";
				position[]={2899.55,5323.9399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall10
			{
				name="";
				position[]={2912.48,5290.6001};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall11
			{
				name="";
				position[]={3007.51,5206.52};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall12
			{
				name="";
				position[]={2768.7,5166.2202};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall13
			{
				name="";
				position[]={2802.53,5006.5098};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall14
			{
				name="";
				position[]={2854.77,4933.3799};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall15
			{
				name="";
				position[]={2998.5601,4955.77};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall16
			{
				name="";
				position[]={2909,5113.48};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall17
			{
				name="";
				position[]={3216.97,4977.1602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall18
			{
				name="";
				position[]={2919.45,4695.0601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall19
			{
				name="";
				position[]={2944.8201,4629.3901};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall20
			{
				name="";
				position[]={3091.5901,4575.1602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall21
			{
				name="";
				position[]={3052.79,4794.5698};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall22
			{
				name="";
				position[]={3097.0701,4670.6899};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall23
			{
				name="";
				position[]={3155.28,4495.0601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall24
			{
				name="";
				position[]={3045.8201,4374.1602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall25
			{
				name="";
				position[]={3253.29,4208.98};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall26
			{
				name="";
				position[]={3412.5,4306};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall27
			{
				name="";
				position[]={3392.1001,4431.8701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall28
			{
				name="";
				position[]={3322.9399,4155.25};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall29
			{
				name="";
				position[]={3186.1201,4065.7};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall30
			{
				name="";
				position[]={3121.4399,4173.1602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall31
			{
				name="";
				position[]={3415.98,4174.1602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall32
			{
				name="";
				position[]={3442.3501,4079.1299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall33
			{
				name="";
				position[]={3632.3999,4064.2};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall34
			{
				name="";
				position[]={3628.4199,4197.04};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall35
			{
				name="";
				position[]={3630.9099,4332.8701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall36
			{
				name="";
				position[]={3598.0701,4247.29};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall37
			{
				name="";
				position[]={3535.8799,4357.25};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall38
			{
				name="";
				position[]={3498.5701,4208.98};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall39
			{
				name="";
				position[]={3744.3401,4178.1401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall40
			{
				name="";
				position[]={3863.75,4296.5498};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall41
			{
				name="";
				position[]={3824.45,4395.0601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall42
			{
				name="";
				position[]={3958.78,4266.7002};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall43
			{
				name="";
				position[]={3998.0801,4263.71};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall44
			{
				name="";
				position[]={4079.1799,4289.5801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall45
			{
				name="";
				position[]={3987.1399,4142.3101};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall46
			{
				name="";
				position[]={4102.0601,4128.3799};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall47
			{
				name="";
				position[]={3883.6499,4086.5901};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall48
			{
				name="";
				position[]={3700.0701,4297.04};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall49
			{
				name="";
				position[]={4140.3799,4290.5801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall50
			{
				name="";
				position[]={4164.2598,4163.21};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall51
			{
				name="";
				position[]={4227.4399,4144.2998};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall52
			{
				name="";
				position[]={4193.6099,4319.4302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall53
			{
				name="";
				position[]={4159.2798,4456.25};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall54
			{
				name="";
				position[]={4145.8501,4522.4199};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall55
			{
				name="";
				position[]={3979.6699,4543.8198};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall56
			{
				name="";
				position[]={3998.0801,4373.6602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall57
			{
				name="";
				position[]={4337.3901,4399.54};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall58
			{
				name="";
				position[]={4367.7402,4499.54};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall59
			{
				name="";
				position[]={4387.6401,4444.3101};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall60
			{
				name="";
				position[]={4407.54,4343.3101};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall61
			{
				name="";
				position[]={4241.3701,4492.5698};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall62
			{
				name="";
				position[]={4272.7202,4551.2798};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall63
			{
				name="";
				position[]={4212.02,4533.3701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall64
			{
				name="";
				position[]={4448.3398,4481.1299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall65
			{
				name="";
				position[]={4477.2002,4420.9302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall66
			{
				name="";
				position[]={4565.7598,4404.0098};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall67
			{
				name="";
				position[]={4291.6201,4285.6001};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall68
			{
				name="";
				position[]={4582.6699,4265.2002};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall69
			{
				name="";
				position[]={4634.4199,4171.6699};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall70
			{
				name="";
				position[]={4733.4199,4244.3101};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall71
			{
				name="";
				position[]={4722.98,4375.1602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall72
			{
				name="";
				position[]={4625.46,4365.21};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall73
			{
				name="";
				position[]={4714.52,4527.3999};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall74
			{
				name="";
				position[]={4847.3599,4278.6401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall75
			{
				name="";
				position[]={4816.5098,4122.4102};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall76
			{
				name="";
				position[]={4732.9302,4105.5};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall77
			{
				name="";
				position[]={4625.96,4211.9702};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall78
			{
				name="";
				position[]={5098.6099,4208.4902};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall79
			{
				name="";
				position[]={4993.1299,4193.5601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall80
			{
				name="";
				position[]={4992.6299,4310.98};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall81
			{
				name="";
				position[]={4812.0298,4338.3398};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall82
			{
				name="";
				position[]={5092.1401,4372.1699};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall83
			{
				name="";
				position[]={5039.8999,4361.23};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall84
			{
				name="";
				position[]={5232.9399,4268.1899};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall85
			{
				name="";
				position[]={5129.4502,4036.8401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall86
			{
				name="";
				position[]={5513.54,4136.3398};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall87
			{
				name="";
				position[]={5219.0098,4084.6001};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall88
			{
				name="";
				position[]={5291.6499,4004.5};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall89
			{
				name="";
				position[]={5135.4199,3999.03};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall90
			{
				name="";
				position[]={5096.6201,3964.2};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall91
			{
				name="";
				position[]={5042.8799,3911.96};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall92
			{
				name="";
				position[]={4941.8901,3946.79};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall93
			{
				name="";
				position[]={5141.3901,3883.1001};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall94
			{
				name="";
				position[]={5268.2598,3819.9199};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall95
			{
				name="";
				position[]={5366.77,3904.5};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall97
			{
				name="";
				position[]={5132.4399,3720.9099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall98
			{
				name="";
				position[]={5011.54,3751.26};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall99
			{
				name="";
				position[]={4862.7798,3697.03};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1000
			{
				name="";
				position[]={4712.5298,3574.6399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall101
			{
				name="";
				position[]={4658.2998,3730.8601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall102
			{
				name="";
				position[]={4689.6401,3843.8};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall103
			{
				name="";
				position[]={4724.9702,3769.6699};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall104
			{
				name="";
				position[]={4774.7202,3890.0701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall105
			{
				name="";
				position[]={4945.3701,3897.53};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall106
			{
				name="";
				position[]={4543.3701,3817.9299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall107
			{
				name="";
				position[]={4453.8101,3682.1001};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall108
			{
				name="";
				position[]={4404.5601,3611.46};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall109
			{
				name="";
				position[]={4343.8599,3534.3401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall110
			{
				name="";
				position[]={4290.6299,3559.71};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall112
			{
				name="";
				position[]={4298.0898,3616.4299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall111
			{
				name="";
				position[]={4263.2598,3592.05};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall113
			{
				name="";
				position[]={4139.8799,3675.6399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall114
			{
				name="";
				position[]={4188.1401,3606.98};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall115
			{
				name="";
				position[]={4138.8799,3517.4199};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall116
			{
				name="";
				position[]={3918.48,3374.1399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall117
			{
				name="";
				position[]={3862.76,3396.03};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall118
			{
				name="";
				position[]={3768.72,3421.3999};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall119
			{
				name="";
				position[]={3796.0901,3506.48};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall120
			{
				name="";
				position[]={3695.5901,3573.1499};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall121
			{
				name="";
				position[]={3607.53,3439.3101};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall122
			{
				name="";
				position[]={3614.99,3568.1699};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall123
			{
				name="";
				position[]={3813.5,3600.01};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall124
			{
				name="";
				position[]={4016.75,3438.5901};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall125
			{
				name="";
				position[]={4266.1699,3831.4399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall126
			{
				name="";
				position[]={4011.97,3903.1599};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall127
			{
				name="";
				position[]={3937.0601,3931.05};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall128
			{
				name="";
				position[]={4092.45,4051.3701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall129
			{
				name="";
				position[]={4022.3301,4009.1399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall130
			{
				name="";
				position[]={3794.4199,3971.6899};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall131
			{
				name="";
				position[]={4433.5,4009.9399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall132
			{
				name="";
				position[]={4304.4102,4025.0801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall133
			{
				name="";
				position[]={4168.1499,4049.78};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall134
			{
				name="";
				position[]={4070.9299,3842.6001};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall135
			{
				name="";
				position[]={4517.1802,3974.0801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall136
			{
				name="";
				position[]={4667.7798,4102.3701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall137
			{
				name="";
				position[]={4455.02,4301.5898};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall138
			{
				name="";
				position[]={4853.4502,4021.0901};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall139
			{
				name="";
				position[]={4773.7598,3930.25};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall140
			{
				name="";
				position[]={5201.6802,4505.5801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall141
			{
				name="";
				position[]={5121.2002,4556.5801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall142
			{
				name="";
				position[]={5090.9102,4641.0498};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall143
			{
				name="";
				position[]={4984.1401,4410.7598};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall144
			{
				name="";
				position[]={5072.5898,4501.6001};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall145
			{
				name="";
				position[]={4866.2002,4478.4902};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall146
			{
				name="";
				position[]={4861.4199,4642.6401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall147
			{
				name="";
				position[]={5351.4902,4543.8301};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall148
			{
				name="";
				position[]={5595.3301,4514.3501};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall149
			{
				name="";
				position[]={5639.9502,4592.4399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall150
			{
				name="";
				position[]={5759.48,4567.7402};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall151
			{
				name="";
				position[]={5377.7798,4639.46};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall152
			{
				name="";
				position[]={5543.5298,4739.0601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall153
			{
				name="";
				position[]={5489.3398,4903.2202};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall154
			{
				name="";
				position[]={5314.04,4889.6699};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall155
			{
				name="";
				position[]={5041.5098,4836.2798};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall156
			{
				name="";
				position[]={4975.3701,4643.4399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall157
			{
				name="";
				position[]={5694.9302,4762.9702};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall158
			{
				name="";
				position[]={5829.6001,4647.4199};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall159
			{
				name="";
				position[]={5882.2002,4511.96};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall160
			{
				name="";
				position[]={5812.0698,4377.29};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall161
			{
				name="";
				position[]={6081.4102,4317.52};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall162
			{
				name="";
				position[]={5684.5698,4359.7598};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall163
			{
				name="";
				position[]={5742.75,4204.3701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall164
			{
				name="";
				position[]={5571.4199,4252.1802};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall165
			{
				name="";
				position[]={5630.3901,4043.3999};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall166
			{
				name="";
				position[]={5400.8901,4121.5};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall167
			{
				name="";
				position[]={5486.1602,4294.4199};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall168
			{
				name="";
				position[]={5922.04,4112.73};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall169
			{
				name="";
				position[]={5943.5498,4303.98};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall170
			{
				name="";
				position[]={6061.4902,4199.5898};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall171
			{
				name="";
				position[]={6254.3301,4502.3999};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall172
			{
				name="";
				position[]={6059.8901,4492.8301};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall173
			{
				name="";
				position[]={6364.29,4578.1001};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall174
			{
				name="";
				position[]={6287,4674.52};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall175
			{
				name="";
				position[]={6130.02,4757.3901};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall176
			{
				name="";
				position[]={5986.5801,4715.9502};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall177
			{
				name="";
				position[]={6530.8398,4719.9399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall178
			{
				name="";
				position[]={6443.1802,4623.52};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall179
			{
				name="";
				position[]={6574.6699,4619.5298};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall180
			{
				name="";
				position[]={6255.1299,4599.6099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall181
			{
				name="";
				position[]={6660.73,4786.0801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall182
			{
				name="";
				position[]={6805.75,4850.6201};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall183
			{
				name="";
				position[]={6796.1899,4952.6201};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall184
			{
				name="";
				position[]={6576.2598,4996.4502};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall185
			{
				name="";
				position[]={6090.1802,4796.4399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall186
			{
				name="";
				position[]={6012.0801,4894.4502};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall187
			{
				name="";
				position[]={5861.48,4944.6499};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall188
			{
				name="";
				position[]={6051.1299,5008.3999};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall189
			{
				name="";
				position[]={6218.4702,5111.2002};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall190
			{
				name="";
				position[]={6357.1201,5162.9902};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall191
			{
				name="";
				position[]={6462.3101,5068.1699};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall192
			{
				name="";
				position[]={5795.3398,5157.4102};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall193
			{
				name="";
				position[]={5616.04,5130.3198};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall194
			{
				name="";
				position[]={5435.1602,5109.6001};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall195
			{
				name="";
				position[]={5345.1099,5007.6001};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall196
			{
				name="";
				position[]={5307.6602,5080.1201};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall197
			{
				name="";
				position[]={5451.0898,4996.4502};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall198
			{
				name="";
				position[]={5171.3999,4900.8198};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall199
			{
				name="";
				position[]={5259.0498,4892.8599};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall200
			{
				name="";
				position[]={4887.71,5037.8799};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall201
			{
				name="";
				position[]={4544.27,4965.3701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall202
			{
				name="";
				position[]={4524.3501,4775.7202};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall203
			{
				name="";
				position[]={4714,4996.4502};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall204
			{
				name="";
				position[]={4743.48,4722.3301};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall205
			{
				name="";
				position[]={4956.25,4790.0601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall206
			{
				name="";
				position[]={4474.9399,4637.8599};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall207
			{
				name="";
				position[]={4305.21,4732.6899};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall208
			{
				name="";
				position[]={4455.02,4776.5098};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall209
			{
				name="";
				position[]={4153.8101,4739.8599};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall210
			{
				name="";
				position[]={3942.6399,4613.96};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall211
			{
				name="";
				position[]={3850.3201,4645.8101};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall212
			{
				name="";
				position[]={3958.28,4716.96};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall213
			{
				name="";
				position[]={4033.4099,4780.6401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall214
			{
				name="";
				position[]={3921.96,4761.23};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall215
			{
				name="";
				position[]={3775.6899,4777.1602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall216
			{
				name="";
				position[]={3712.01,4796.5601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall217
			{
				name="";
				position[]={3723.95,4705.5098};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall218
			{
				name="";
				position[]={3845.3401,4722.4302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall219
			{
				name="";
				position[]={3721.46,4727.8999};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall220
			{
				name="";
				position[]={3648.3201,4708};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall221
			{
				name="";
				position[]={3521.45,4697.5498};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall222
			{
				name="";
				position[]={3508.02,4783.6201};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall223
			{
				name="";
				position[]={3593.5901,4787.1099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall224
			{
				name="";
				position[]={3442.8401,4773.6699};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall225
			{
				name="";
				position[]={3358.76,4772.6802};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall226
			{
				name="";
				position[]={3412.99,4740.8398};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall227
			{
				name="";
				position[]={3295.5801,4672.1802};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall228
			{
				name="";
				position[]={3327.9199,4621.4302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall229
			{
				name="";
				position[]={3345.8301,4579.6401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall230
			{
				name="";
				position[]={3355.78,4521.4302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall231
			{
				name="";
				position[]={3344.3301,4487.6001};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall232
			{
				name="";
				position[]={3413.49,4548.29};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall233
			{
				name="";
				position[]={3435.3799,4671.1802};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall234
			{
				name="";
				position[]={3639.8601,4586.1099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall235
			{
				name="";
				position[]={3580.6599,4522.9199};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall236
			{
				name="";
				position[]={3682.6499,4553.77};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall237
			{
				name="";
				position[]={3537.3701,4587.1001};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall238
			{
				name="";
				position[]={3757.28,4605.0098};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall239
			{
				name="";
				position[]={3754.79,4573.6699};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall240
			{
				name="";
				position[]={3754.3,4664.2202};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall241
			{
				name="";
				position[]={3893.1001,4690.5898};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall242
			{
				name="";
				position[]={3540.8601,4472.1699};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall243
			{
				name="";
				position[]={3484.6399,4461.23};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall244
			{
				name="";
				position[]={3454.29,4483.6201};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall245
			{
				name="";
				position[]={3377.1699,4455.2598};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall246
			{
				name="";
				position[]={3310.5,4744.3198};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall247
			{
				name="";
				position[]={3247.8201,4663.7202};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall248
			{
				name="";
				position[]={3177.6599,4655.2598};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall249
			{
				name="";
				position[]={3226.9199,4485.6099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall250
			{
				name="";
				position[]={3204.53,4427.8901};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall251
			{
				name="";
				position[]={3061.74,4600.5298};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall252
			{
				name="";
				position[]={3038.3601,4701.5298};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall253
			{
				name="";
				position[]={3024.4299,4537.8501};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall254
			{
				name="";
				position[]={3068.71,4736.8599};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall255
			{
				name="";
				position[]={3155.28,4407};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall256
			{
				name="";
				position[]={3148.8101,4802.0298};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall257
			{
				name="";
				position[]={3180.1499,4836.8599};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall258
			{
				name="";
				position[]={3214.48,4794.5698};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall259
			{
				name="";
				position[]={2914.47,4886.1099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall260
			{
				name="";
				position[]={3024.4299,4903.0298};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall261
			{
				name="";
				position[]={2970.2,4992.0898};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall262
			{
				name="";
				position[]={3121.9399,4931.3901};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall263
			{
				name="";
				position[]={3209.51,5045.8198};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall264
			{
				name="";
				position[]={3244.3301,5048.7998};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall265
			{
				name="";
				position[]={3302.54,5098.5601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall266
			{
				name="";
				position[]={3300.55,4951.29};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall267
			{
				name="";
				position[]={3343.8401,4985.6201};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall268
			{
				name="";
				position[]={3372.6899,5044.3301};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall269
			{
				name="";
				position[]={3395.5801,5098.0601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall270
			{
				name="";
				position[]={3415.48,4983.1299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall271
			{
				name="";
				position[]={3406.53,4862.23};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall272
			{
				name="";
				position[]={3327.9199,4810.4902};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall273
			{
				name="";
				position[]={3494.0901,4958.25};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall274
			{
				name="";
				position[]={3550.8101,5024.4302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall275
			{
				name="";
				position[]={3607.03,4987.6099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall276
			{
				name="";
				position[]={3693.6001,5094.5801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall277
			{
				name="";
				position[]={3689.6201,4973.6802};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall278
			{
				name="";
				position[]={3529.9099,4857.2598};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall279
			{
				name="";
				position[]={3644.8401,4872.6802};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall280
			{
				name="";
				position[]={3723.95,4881.1401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall281
			{
				name="";
				position[]={3513.99,5177.1699};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall282
			{
				name="";
				position[]={3467.22,5166.2202};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall283
			{
				name="";
				position[]={3852.8101,5096.0698};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall284
			{
				name="";
				position[]={3911.02,4994.0801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall285
			{
				name="";
				position[]={3976.1899,4952.7798};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall286
			{
				name="";
				position[]={4013.01,4996.0698};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall287
			{
				name="";
				position[]={4058.78,5057.2598};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall288
			{
				name="";
				position[]={3918.98,4908.5};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall289
			{
				name="";
				position[]={4013.51,4909.5};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall290
			{
				name="";
				position[]={4116.4902,4921.9399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall291
			{
				name="";
				position[]={4130.4199,4968.7002};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall292
			{
				name="";
				position[]={4115.5,4869.2002};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall293
			{
				name="";
				position[]={4081.6699,4799.0498};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall294
			{
				name="";
				position[]={4175.2002,4953.7798};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall295
			{
				name="";
				position[]={4117.4902,5031.8901};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall296
			{
				name="";
				position[]={4164.75,5057.7598};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall297
			{
				name="";
				position[]={4214.5,5046.8101};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall298
			{
				name="";
				position[]={4260.77,5041.3398};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall299
			{
				name="";
				position[]={4334.4102,5048.7998};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall300
			{
				name="";
				position[]={4369.2402,5085.6201};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall301
			{
				name="";
				position[]={4296.6001,4969.7002};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall302
			{
				name="";
				position[]={4396.1001,4994.5698};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall303
			{
				name="";
				position[]={4102.0601,5173.1899};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall304
			{
				name="";
				position[]={3959.28,5143.3301};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall305
			{
				name="";
				position[]={4190.6201,5113.48};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall306
			{
				name="";
				position[]={4299.0801,5157.2598};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall307
			{
				name="";
				position[]={4350.3301,5199.0601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall308
			{
				name="";
				position[]={4389.1401,5138.8599};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall309
			{
				name="";
				position[]={4473.2202,5220.9502};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall310
			{
				name="";
				position[]={4469.7402,5094.5801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall311
			{
				name="";
				position[]={4564.27,5068.21};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall312
			{
				name="";
				position[]={4628.9399,5093.5801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall313
			{
				name="";
				position[]={4682.6802,5138.3599};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall314
			{
				name="";
				position[]={4669.7402,5238.3599};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall315
			{
				name="";
				position[]={4757.7998,5212.9902};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall316
			{
				name="";
				position[]={4756.8101,5269.21};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall317
			{
				name="";
				position[]={4824.4702,5315.98};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall318
			{
				name="";
				position[]={4944.3701,5218.96};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall319
			{
				name="";
				position[]={4819,5254.7798};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall320
			{
				name="";
				position[]={4890.6401,5368.71};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall321
			{
				name="";
				position[]={4813.0298,5381.1499};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall322
			{
				name="";
				position[]={4741.3799,5324.9302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall323
			{
				name="";
				position[]={4811.5298,5133.8799};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall324
			{
				name="";
				position[]={4948.8501,5142.8398};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall325
			{
				name="";
				position[]={5025.4702,5180.1499};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall326
			{
				name="";
				position[]={5032.4399,5050.2998};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall327
			{
				name="";
				position[]={5026.46,4988.1099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall328
			{
				name="";
				position[]={5027.96,4919.4502};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall329
			{
				name="";
				position[]={5113.0298,4954.27};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall330
			{
				name="";
				position[]={5172.2402,5046.8101};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall331
			{
				name="";
				position[]={5232.4399,5081.6401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall332
			{
				name="";
				position[]={5144.8799,4981.1401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall333
			{
				name="";
				position[]={5113.0298,5098.5601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall334
			{
				name="";
				position[]={5013.0298,5276.6699};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall335
			{
				name="";
				position[]={5147.3599,5305.5298};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall336
			{
				name="";
				position[]={5231.9399,5296.5698};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall337
			{
				name="";
				position[]={5297.1201,5325.9302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall338
			{
				name="";
				position[]={5400.1099,5320.9502};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall339
			{
				name="";
				position[]={5477.7202,5202.04};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall340
			{
				name="";
				position[]={5494.6401,5271.2002};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall341
			{
				name="";
				position[]={5455.8301,5375.1802};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall342
			{
				name="";
				position[]={5473.7402,5425.9302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall343
			{
				name="";
				position[]={5384.6802,5494.5898};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall344
			{
				name="";
				position[]={5305.5801,5485.1299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall345
			{
				name="";
				position[]={5205.5698,5387.1201};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall346
			{
				name="";
				position[]={5160.2998,5386.1299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall347
			{
				name="";
				position[]={5194.6299,5487.1201};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall348
			{
				name="";
				position[]={5588.1699,5396.0801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall349
			{
				name="";
				position[]={5785.6899,5260.75};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall350
			{
				name="";
				position[]={5930.4702,5272.1899};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall351
			{
				name="";
				position[]={5827.48,5312.9902};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall352
			{
				name="";
				position[]={5755.3398,5343.8398};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall353
			{
				name="";
				position[]={5907.5801,5360.25};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall354
			{
				name="";
				position[]={6010.5698,5300.0498};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall355
			{
				name="";
				position[]={6143.9102,5311.5};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall356
			{
				name="";
				position[]={6234.4502,5313.4902};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall357
			{
				name="";
				position[]={6262.8101,5376.1802};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall358
			{
				name="";
				position[]={6139.4302,5370.7002};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall359
			{
				name="";
				position[]={6001.1201,5348.8101};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall360
			{
				name="";
				position[]={6356.8501,5388.1201};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall361
			{
				name="";
				position[]={6440.9302,5329.4102};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall362
			{
				name="";
				position[]={6530.98,5333.8901};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall363
			{
				name="";
				position[]={6442.9199,5376.6699};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall364
			{
				name="";
				position[]={6503.1201,5450.3101};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall365
			{
				name="";
				position[]={6407.1001,5473.1899};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall366
			{
				name="";
				position[]={6497.6499,5551.2998};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall367
			{
				name="";
				position[]={6417.54,5611.5};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall368
			{
				name="";
				position[]={6271.27,5505.5298};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall369
			{
				name="";
				position[]={6221.02,5455.2798};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall370
			{
				name="";
				position[]={6190.6699,5496.0801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall371
			{
				name="";
				position[]={6123.5098,5517.4702};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall372
			{
				name="";
				position[]={6084.7002,5480.6602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall373
			{
				name="";
				position[]={6350.8799,5446.3301};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall374
			{
				name="";
				position[]={6445.8999,5559.2598};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall375
			{
				name="";
				position[]={6330.9702,5577.6699};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall376
			{
				name="";
				position[]={6249.3799,5579.1699};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall377
			{
				name="";
				position[]={6655.8599,5580.6602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall378
			{
				name="";
				position[]={6749.8901,5567.23};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall379
			{
				name="";
				position[]={6479.73,5648.3198};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall380
			{
				name="";
				position[]={6546.3999,5605.5298};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall381
			{
				name="";
				position[]={6800.6401,5532.3999};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall382
			{
				name="";
				position[]={6686.21,5474.1899};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall383
			{
				name="";
				position[]={6894.1699,5500.0601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall384
			{
				name="";
				position[]={6847.3999,5455.2798};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall385
			{
				name="";
				position[]={6861.8301,5598.0698};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall386
			{
				name="";
				position[]={6869.79,5658.77};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall387
			{
				name="";
				position[]={6955.8701,5440.3599};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall388
			{
				name="";
				position[]={7001.6401,5516.98};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall389
			{
				name="";
				position[]={6981.7402,5554.29};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall400
			{
				name="";
				position[]={7084.23,5562.25};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall401
			{
				name="";
				position[]={7122.04,5499.5601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall403
			{
				name="";
				position[]={7130.9902,5586.1299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall404
			{
				name="";
				position[]={7196.1699,5552.7998};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall405
			{
				name="";
				position[]={7070.79,5134.8799};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall406
			{
				name="";
				position[]={7108.1099,5411};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall407
			{
				name="";
				position[]={7271.79,5698.5698};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall408
			{
				name="";
				position[]={7434.98,5841.8599};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall409
			{
				name="";
				position[]={7466.8198,5526.9302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall410
			{
				name="";
				position[]={7022.5298,5764.7402};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall411
			{
				name="";
				position[]={7102.6401,5726.9302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall412
			{
				name="";
				position[]={7140.9399,5739.3701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall413
			{
				name="";
				position[]={7107.1099,5666.23};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall414
			{
				name="";
				position[]={7179.75,5678.1699};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall415
			{
				name="";
				position[]={6955.3701,5748.8198};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall416
			{
				name="";
				position[]={6775.2598,5726.9302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall417
			{
				name="";
				position[]={6608.1001,5664.7402};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall418
			{
				name="";
				position[]={6532.4702,5715.4902};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall419
			{
				name="";
				position[]={6360.3301,5667.7202};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall420
			{
				name="";
				position[]={6179.23,5616.98};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall421
			{
				name="";
				position[]={6289.1802,5709.02};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall422
			{
				name="";
				position[]={6416.5498,5770.71};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall423
			{
				name="";
				position[]={6345.3999,5778.6699};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall424
			{
				name="";
				position[]={6406.1001,5833.3999};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall425
			{
				name="";
				position[]={6464.3101,5859.27};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall426
			{
				name="";
				position[]={6508.0898,5826.4399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall427
			{
				name="";
				position[]={6594.1699,5813};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall428
			{
				name="";
				position[]={6669.79,5829.9199};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall429
			{
				name="";
				position[]={6662.3301,5877.1802};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall430
			{
				name="";
				position[]={6569.79,5906.04};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall431
			{
				name="";
				position[]={6744.9199,5888.6299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall432
			{
				name="";
				position[]={6796.1602,5856.7798};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall433
			{
				name="";
				position[]={6877.2598,5907.0298};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall434
			{
				name="";
				position[]={6907.6099,5820.96};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall435
			{
				name="";
				position[]={6979.25,5861.2598};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall436
			{
				name="";
				position[]={7075.27,5856.7798};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall437
			{
				name="";
				position[]={7124.5298,5890.1201};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall438
			{
				name="";
				position[]={7023.0298,5923.4502};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall439
			{
				name="";
				position[]={7107.6099,5930.4199};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall440
			{
				name="";
				position[]={7242.4399,5974.2002};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall441
			{
				name="";
				position[]={7045.4199,5986.6401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall442
			{
				name="";
				position[]={6899.1499,5978.1802};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall443
			{
				name="";
				position[]={6802.6299,5910.52};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall444
			{
				name="";
				position[]={6745.9102,5947.8301};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall445
			{
				name="";
				position[]={6751.8799,5969.2202};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall446
			{
				name="";
				position[]={6713.0698,6028.4302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall447
			{
				name="";
				position[]={6644.9102,6047.3398};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall448
			{
				name="";
				position[]={6851.3901,6060.77};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall449
			{
				name="";
				position[]={6896.1602,6120.9702};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall450
			{
				name="";
				position[]={7114.0801,6059.77};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall451
			{
				name="";
				position[]={7067.8101,6112.0098};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall452
			{
				name="";
				position[]={7000.1401,6109.0298};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall453
			{
				name="";
				position[]={6997.6602,6185.1499};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall454
			{
				name="";
				position[]={7128.5098,6207.54};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall455
			{
				name="";
				position[]={7049.8999,6222.46};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall456
			{
				name="";
				position[]={7011.0898,6257.29};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall457
			{
				name="";
				position[]={6813.5698,6197.5898};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall458
			{
				name="";
				position[]={6958.8501,6313.0098};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall459
			{
				name="";
				position[]={7005.1201,6363.2598};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall460
			{
				name="";
				position[]={6950.8901,6412.52};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall461
			{
				name="";
				position[]={7107.1099,6347.3398};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall462
			{
				name="";
				position[]={7074.2798,6464.7598};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall463
			{
				name="";
				position[]={7052.8799,6524.96};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall464
			{
				name="";
				position[]={6988.7002,6546.3501};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall465
			{
				name="";
				position[]={7005.1201,6484.6602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall466
			{
				name="";
				position[]={6922.5298,6478.1899};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall467
			{
				name="";
				position[]={6916.0601,6525.46};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall469
			{
				name="";
				position[]={6859.8398,6549.3398};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall470
			{
				name="";
				position[]={6815.5601,6521.48};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall471
			{
				name="";
				position[]={6742.4302,6472.7202};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall472
			{
				name="";
				position[]={6650.8799,6450.8301};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall473
			{
				name="";
				position[]={6604.6099,6497.6001};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall474
			{
				name="";
				position[]={6625.5098,6406.5498};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall475
			{
				name="";
				position[]={6708.6001,6104.5498};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall476
			{
				name="";
				position[]={6664.8101,6081.1699};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall477
			{
				name="";
				position[]={6622.0298,6141.8701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall478
			{
				name="";
				position[]={6741.9302,6154.7998};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall479
			{
				name="";
				position[]={6526.5,6302.5698};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall480
			{
				name="";
				position[]={6539.9302,6179.1802};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall481
			{
				name="";
				position[]={6458.8398,6179.6802};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall482
			{
				name="";
				position[]={6439.4399,6254.7998};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall483
			{
				name="";
				position[]={6547.8999,6111.52};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall484
			{
				name="";
				position[]={6562.8198,6015.9902};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall485
			{
				name="";
				position[]={6485.7002,5983.1602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall486
			{
				name="";
				position[]={6463.3198,6083.6602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall487
			{
				name="";
				position[]={6405.1099,6306.5498};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall488
			{
				name="";
				position[]={6335.4502,6288.6401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall489
			{
				name="";
				position[]={6304.6099,6232.4199};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall490
			{
				name="";
				position[]={6297.6401,6147.8398};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall491
			{
				name="";
				position[]={6433.46,5963.25};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall492
			{
				name="";
				position[]={6452.3701,5907.5298};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall493
			{
				name="";
				position[]={6367.29,5946.8398};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall494
			{
				name="";
				position[]={6342.9199,6004.0498};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall495
			{
				name="";
				position[]={6304.1099,6007.5298};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall496
			{
				name="";
				position[]={6263.3101,5885.6401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall497
			{
				name="";
				position[]={6394.1602,5876.1899};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall498
			{
				name="";
				position[]={6224.0098,5802.5498};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall499
			{
				name="";
				position[]={6165.7998,5731.4102};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall500
			{
				name="";
				position[]={6104.6001,5762.75};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall501
			{
				name="";
				position[]={6191.6699,5828.4302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall502
			{
				name="";
				position[]={6139.4302,5880.1699};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall503
			{
				name="";
				position[]={6188.1899,5907.5298};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall504
			{
				name="";
				position[]={6203.1099,5950.8198};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall506
			{
				name="";
				position[]={6388.1899,6052.3101};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall507
			{
				name="";
				position[]={6245.3999,6033.4102};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall508
			{
				name="";
				position[]={6108.0801,5972.71};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall509
			{
				name="";
				position[]={6082.21,5944.8501};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall510
			{
				name="";
				position[]={6028.98,5961.7598};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall511
			{
				name="";
				position[]={5999.1299,5754.29};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall512
			{
				name="";
				position[]={5947.8799,5692.6001};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall513
			{
				name="";
				position[]={6078.73,5666.23};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall514
			{
				name="";
				position[]={6091.6699,5623.4399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall515
			{
				name="";
				position[]={6069.2798,5556.2798};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall516
			{
				name="";
				position[]={6034.9502,5520.46};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall517
			{
				name="";
				position[]={5970.77,5399.0601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall518
			{
				name="";
				position[]={5976.7402,5532.3999};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall519
			{
				name="";
				position[]={5847.8799,5498.0698};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall520
			{
				name="";
				position[]={5777.23,5427.4199};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall521
			{
				name="";
				position[]={5759.8198,5518.96};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall522
			{
				name="";
				position[]={5834.4399,5668.7202};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall523
			{
				name="";
				position[]={5873.75,5797.0801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall524
			{
				name="";
				position[]={5819.52,5634.8901};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall525
			{
				name="";
				position[]={5953.3501,5739.3701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall526
			{
				name="";
				position[]={5953.3501,5894.6001};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall527
			{
				name="";
				position[]={5878.7202,5933.8999};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall528
			{
				name="";
				position[]={5774.2402,5969.2202};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall529
			{
				name="";
				position[]={5808.0801,5834.3999};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall530
			{
				name="";
				position[]={5688.1699,5936.3901};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall531
			{
				name="";
				position[]={5709.5698,5802.0601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall532
			{
				name="";
				position[]={5664.79,5796.5801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall533
			{
				name="";
				position[]={5617.52,5878.1802};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall534
			{
				name="";
				position[]={5645.3901,5943.3501};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall535
			{
				name="";
				position[]={5562.2998,5945.8398};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall536
			{
				name="";
				position[]={5531.4502,5788.1299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall537
			{
				name="";
				position[]={5464.29,5807.0298};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall538
			{
				name="";
				position[]={5401.6001,5924.4502};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall539
			{
				name="";
				position[]={5337.9199,5899.0698};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall540
			{
				name="";
				position[]={5284.1802,5823.9502};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall541
			{
				name="";
				position[]={5291.1499,5735.8901};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall542
			{
				name="";
				position[]={5332.9399,5826.9302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall543
			{
				name="";
				position[]={5221,5781.6602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall544
			{
				name="";
				position[]={5118.5098,5855.29};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall545
			{
				name="";
				position[]={5149.8501,5725.4399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall546
			{
				name="";
				position[]={5076.2202,5761.2598};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall547
			{
				name="";
				position[]={5078.21,5851.3101};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall548
			{
				name="";
				position[]={5020,5845.8398};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall549
			{
				name="";
				position[]={5002.0898,5757.2798};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall550
			{
				name="";
				position[]={5440.8999,5719.96};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall551
			{
				name="";
				position[]={5416.0298,5676.6802};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall552
			{
				name="";
				position[]={5335.4302,5690.6099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall553
			{
				name="";
				position[]={5284.6802,5691.1099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall554
			{
				name="";
				position[]={5355.8301,5586.1299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall555
			{
				name="";
				position[]={5446.8701,5570.21};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall556
			{
				name="";
				position[]={5267.27,5591.1099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall557
			{
				name="";
				position[]={5210.0498,5553.79};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall558
			{
				name="";
				position[]={5192.1401,5609.02};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall559
			{
				name="";
				position[]={5101.0898,5550.8101};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall560
			{
				name="";
				position[]={5019.5,5471.2002};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall561
			{
				name="";
				position[]={5027.46,5540.8599};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall562
			{
				name="";
				position[]={5017.0098,5656.7798};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall563
			{
				name="";
				position[]={4994.1299,5788.1299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall564
			{
				name="";
				position[]={4893.1299,5817.48};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall565
			{
				name="";
				position[]={4882.1802,5586.6299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall566
			{
				name="";
				position[]={4723.9702,5503.04};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall567
			{
				name="";
				position[]={4846.8599,5467.7202};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall568
			{
				name="";
				position[]={4831.9302,5734.8901};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall569
			{
				name="";
				position[]={4705.0601,5760.7598};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall570
			{
				name="";
				position[]={6137.9399,6099.5801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall571
			{
				name="";
				position[]={5991.1699,6121.4702};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall572
			{
				name="";
				position[]={6122.0098,6219.98};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall573
			{
				name="";
				position[]={6260.3301,6270.23};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall574
			{
				name="";
				position[]={6037.4399,6271.7202};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall575
			{
				name="";
				position[]={5967.2798,6232.9102};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall576
			{
				name="";
				position[]={5862.3101,6195.6001};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall577
			{
				name="";
				position[]={5824.9902,6169.23};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall578
			{
				name="";
				position[]={6553.8701,6416.5};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall579
			{
				name="";
				position[]={6445.4102,6466.25};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall580
			{
				name="";
				position[]={6368.29,6373.71};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall581
			{
				name="";
				position[]={6290.6699,6427.9399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall582
			{
				name="";
				position[]={6156.3398,6432.9199};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall583
			{
				name="";
				position[]={5991.6602,6376.2002};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall584
			{
				name="";
				position[]={5970.27,6329.4302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall585
			{
				name="";
				position[]={6525.5098,6557.7998};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall586
			{
				name="";
				position[]={6408.5898,6673.7202};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall587
			{
				name="";
				position[]={6403.1201,6561.2798};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall588
			{
				name="";
				position[]={6321.02,6518.9902};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall589
			{
				name="";
				position[]={6170.27,6532.4199};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall590
			{
				name="";
				position[]={6103.1099,6508.54};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall591
			{
				name="";
				position[]={5915.54,6467.7402};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall592
			{
				name="";
				position[]={5941.4102,6588.6401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall593
			{
				name="";
				position[]={6006.5898,6630.9302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall594
			{
				name="";
				position[]={5945.8901,6663.77};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall595
			{
				name="";
				position[]={5856.8301,6660.7798};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall596
			{
				name="";
				position[]={5849.8701,6606.0601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall597
			{
				name="";
				position[]={5868.2798,6390.6299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall598
			{
				name="";
				position[]={5857.8301,6315};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall599
			{
				name="";
				position[]={5752.3501,6266.7402};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall600
			{
				name="";
				position[]={5748.3701,6360.2798};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall601
			{
				name="";
				position[]={5704.0898,6409.04};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall602
			{
				name="";
				position[]={5656.8301,6347.8398};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall603
			{
				name="";
				position[]={5616.0298,6241.8701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall604
			{
				name="";
				position[]={5507.0698,6316};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall605
			{
				name="";
				position[]={5600.1099,6248.8301};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall606
			{
				name="";
				position[]={5580.21,6144.8501};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall607
			{
				name="";
				position[]={5639.9102,6120.9702};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall608
			{
				name="";
				position[]={5697.1299,6143.8599};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall609
			{
				name="";
				position[]={5759.3198,6141.8701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall610
			{
				name="";
				position[]={5677.23,6064.75};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall611
			{
				name="";
				position[]={5541.3999,6045.8398};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall612
			{
				name="";
				position[]={5490.1602,6118.48};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall613
			{
				name="";
				position[]={5480.21,6026.4399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall614
			{
				name="";
				position[]={5430.46,6054.2998};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall615
			{
				name="";
				position[]={5386.6699,6100.0698};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall616
			{
				name="";
				position[]={5324.98,6132.4102};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall617
			{
				name="";
				position[]={5393.6401,6192.6099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall618
			{
				name="";
				position[]={5330.9502,6230.9199};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall619
			{
				name="";
				position[]={5539.4102,6215};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall620
			{
				name="";
				position[]={5609.0698,6331.9199};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall621
			{
				name="";
				position[]={5438.9102,6369.23};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall622
			{
				name="";
				position[]={5695.6401,6634.4199};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall623
			{
				name="";
				position[]={5692.6499,6559.29};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall624
			{
				name="";
				position[]={5608.0698,6551.3301};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall625
			{
				name="";
				position[]={5490.1602,6551.8301};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall626
			{
				name="";
				position[]={5442.3999,6472.2202};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall627
			{
				name="";
				position[]={5352.3398,6500.0801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall628
			{
				name="";
				position[]={5269.75,6395.1099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall629
			{
				name="";
				position[]={5262.79,6545.8599};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall630
			{
				name="";
				position[]={5305.5801,6606.0601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall631
			{
				name="";
				position[]={5401.1001,6661.7798};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall632
			{
				name="";
				position[]={5445.3799,6774.2202};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall633
			{
				name="";
				position[]={5515.5298,6769.2402};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall634
			{
				name="";
				position[]={5554.3398,6744.8701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall635
			{
				name="";
				position[]={5699.1201,6732.4302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall636
			{
				name="";
				position[]={5561.7998,6625.46};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall637
			{
				name="";
				position[]={5434.4399,6609.54};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall638
			{
				name="";
				position[]={5216.52,6422.4702};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall639
			{
				name="";
				position[]={5041.3901,6414.0098};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall640
			{
				name="";
				position[]={5043.8799,6467.7402};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall641
			{
				name="";
				position[]={5109.0498,6513.52};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall643
			{
				name="";
				position[]={5188.6602,6535.4102};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall644
			{
				name="";
				position[]={5092.1401,6581.1802};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall645
			{
				name="";
				position[]={5064.77,6626.9502};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall646
			{
				name="";
				position[]={5073.73,6696.6099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall647
			{
				name="";
				position[]={5146.3701,6686.6602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall648
			{
				name="";
				position[]={5241.3999,6675.71};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall649
			{
				name="";
				position[]={4933.4302,6502.5698};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall650
			{
				name="";
				position[]={4892.1299,6530.4302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall651
			{
				name="";
				position[]={4843.8701,6543.3701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall652
			{
				name="";
				position[]={4806.0601,6475.21};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall653
			{
				name="";
				position[]={4874.7202,6374.71};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall654
			{
				name="";
				position[]={4941.8901,6297.0898};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall655
			{
				name="";
				position[]={4825.96,6256.79};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall656
			{
				name="";
				position[]={4929.9502,6409.5298};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall657
			{
				name="";
				position[]={4786.1602,6388.6401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall658
			{
				name="";
				position[]={4736.9102,6429.9302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall659
			{
				name="";
				position[]={4796.6099,6320.9702};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall660
			{
				name="";
				position[]={4772.73,6268.73};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall661
			{
				name="";
				position[]={4982.6802,6193.6099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall662
			{
				name="";
				position[]={4993.1299,6110.52};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall663
			{
				name="";
				position[]={4863.2798,6119.9702};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall664
			{
				name="";
				position[]={4859.79,6022.46};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall665
			{
				name="";
				position[]={4936.4102,5986.6401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall666
			{
				name="";
				position[]={5005.0698,5951.3101};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall667
			{
				name="";
				position[]={4785.6602,6050.8198};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall668
			{
				name="";
				position[]={4826.96,5927.4302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall669
			{
				name="";
				position[]={4676.21,5917.48};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall670
			{
				name="";
				position[]={4608.0498,5958.2798};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall671
			{
				name="";
				position[]={4518.4902,5904.5498};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall672
			{
				name="";
				position[]={4415.0098,5882.6602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall673
			{
				name="";
				position[]={4491.6299,5991.6099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall674
			{
				name="";
				position[]={4459.29,6017.98};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall675
			{
				name="";
				position[]={4514.02,6083.1602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall676
			{
				name="";
				position[]={4591.1299,6149.3301};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall677
			{
				name="";
				position[]={4536.3999,6171.2202};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall678
			{
				name="";
				position[]={4406.0498,6044.8501};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall679
			{
				name="";
				position[]={4581.1802,6244.8501};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall680
			{
				name="";
				position[]={4740.3901,6325.4502};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall681
			{
				name="";
				position[]={4683.6699,6382.1699};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall682
			{
				name="";
				position[]={4618.5,6338.3901};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall683
			{
				name="";
				position[]={4585.1602,6406.5498};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall684
			{
				name="";
				position[]={4476.2002,6096.5898};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall685
			{
				name="";
				position[]={4362.27,5885.6401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall686
			{
				name="";
				position[]={4286.6499,5927.4302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall687
			{
				name="";
				position[]={4234.8999,5882.1602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall688
			{
				name="";
				position[]={4144.8501,5877.1802};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall689
			{
				name="";
				position[]={4088.1299,5941.8599};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall690
			{
				name="";
				position[]={4046.8401,5881.6602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall691
			{
				name="";
				position[]={4033.8999,5822.9502};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall692
			{
				name="";
				position[]={3966.24,5872.21};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall693
			{
				name="";
				position[]={3870.72,5885.1401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall694
			{
				name="";
				position[]={3884.6499,5759.27};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall695
			{
				name="";
				position[]={3681.6599,5794.5898};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall696
			{
				name="";
				position[]={3704.05,5848.8198};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall697
			{
				name="";
				position[]={3579.6599,5945.8398};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall698
			{
				name="";
				position[]={3707.03,6087.6401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall699
			{
				name="";
				position[]={3811.01,6059.77};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall700
			{
				name="";
				position[]={3763.25,6158.2798};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall701
			{
				name="";
				position[]={3608.52,6117.9902};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall702
			{
				name="";
				position[]={3777.1799,6267.2402};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall703
			{
				name="";
				position[]={3907.53,6315.5};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall704
			{
				name="";
				position[]={4001.5601,6360.2798};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall705
			{
				name="";
				position[]={3980.6699,6296.1001};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall706
			{
				name="";
				position[]={3989.1299,6422.4702};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall707
			{
				name="";
				position[]={4169.73,6461.2798};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall708
			{
				name="";
				position[]={4067.24,6418.4902};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall709
			{
				name="";
				position[]={4025.45,6484.1602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall710
			{
				name="";
				position[]={4058.78,6573.7202};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall711
			{
				name="";
				position[]={4107.54,6523.9702};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall712
			{
				name="";
				position[]={4017.98,6521.9702};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall713
			{
				name="";
				position[]={4298.0898,6542.8701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall714
			{
				name="";
				position[]={4354.3101,6437.3999};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall715
			{
				name="";
				position[]={4403.0698,6564.7598};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall716
			{
				name="";
				position[]={4444.8599,6452.8198};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall717
			{
				name="";
				position[]={4455.7998,6605.0601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall718
			{
				name="";
				position[]={4352.8198,6643.3701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall719
			{
				name="";
				position[]={4249.3301,6640.8799};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall720
			{
				name="";
				position[]={4423.4702,6507.0498};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall721
			{
				name="";
				position[]={4656.7998,6689.6401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall722
			{
				name="";
				position[]={4583.1699,6588.6401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall723
			{
				name="";
				position[]={4763.27,6635.9102};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall724
			{
				name="";
				position[]={4849.3501,6687.6499};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall725
			{
				name="";
				position[]={4881.1899,6658.79};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall727
			{
				name="";
				position[]={4953.3301,6717};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall728
			{
				name="";
				position[]={4985.1699,6630.9302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall729
			{
				name="";
				position[]={5020.9902,6550.3301};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall730
			{
				name="";
				position[]={4506.0498,6590.1401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall731
			{
				name="";
				position[]={4235.8999,6565.7598};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall732
			{
				name="";
				position[]={4215.5,6205.0498};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall733
			{
				name="";
				position[]={4139.3799,6264.2598};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall734
			{
				name="";
				position[]={4027.4399,6216.9902};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall735
			{
				name="";
				position[]={4119.48,6168.2402};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall736
			{
				name="";
				position[]={3921.46,6185.6499};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall737
			{
				name="";
				position[]={4139.3799,6091.6201};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall738
			{
				name="";
				position[]={4186.6401,6031.9102};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall739
			{
				name="";
				position[]={4258.29,6083.6602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall740
			{
				name="";
				position[]={4235.8999,6035.3999};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall741
			{
				name="";
				position[]={4218.4902,6144.8501};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall742
			{
				name="";
				position[]={4019.48,6004.5498};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall743
			{
				name="";
				position[]={3929.4199,6099.0801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall744
			{
				name="";
				position[]={3849.8201,5994.1001};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall745
			{
				name="";
				position[]={3719.47,5922.46};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall746
			{
				name="";
				position[]={3597.0801,5990.1201};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall747
			{
				name="";
				position[]={3296.5701,5805.04};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall748
			{
				name="";
				position[]={3268.21,5678.1699};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall749
			{
				name="";
				position[]={3112.99,5718.4702};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall750
			{
				name="";
				position[]={3138.8601,5855.29};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall751
			{
				name="";
				position[]={3276.1699,6038.3799};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall752
			{
				name="";
				position[]={3200.55,6102.5601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall753
			{
				name="";
				position[]={3382.6399,6050.8198};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall754
			{
				name="";
				position[]={3412,6111.02};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall755
			{
				name="";
				position[]={3366.72,6251.8198};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall756
			{
				name="";
				position[]={3301.05,6152.3101};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall757
			{
				name="";
				position[]={3151.3,6278.6899};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall758
			{
				name="";
				position[]={3410.51,6330.9302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall759
			{
				name="";
				position[]={3534.8899,6233.4102};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall760
			{
				name="";
				position[]={3539.8601,6316};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall761
			{
				name="";
				position[]={3463.24,6379.1899};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall762
			{
				name="";
				position[]={3489.6101,6468.7402};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall763
			{
				name="";
				position[]={3640.8601,6501.5801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall764
			{
				name="";
				position[]={3765.24,6440.3799};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall765
			{
				name="";
				position[]={3850.8101,6508.04};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall766
			{
				name="";
				position[]={3834.3999,6379.1899};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall767
			{
				name="";
				position[]={3715.99,6204.0601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall768
			{
				name="";
				position[]={3554.29,6189.6299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall769
			{
				name="";
				position[]={3589.1201,6056.29};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall770
			{
				name="";
				position[]={3003.03,5889.1201};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall771
			{
				name="";
				position[]={3065.72,5746.3301};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall772
			{
				name="";
				position[]={3126.4199,5780.6602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall773
			{
				name="";
				position[]={3235.3799,5760.7598};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall774
			{
				name="";
				position[]={3338.3601,5711.5098};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall775
			{
				name="";
				position[]={3029.8999,5581.1602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall777
			{
				name="";
				position[]={2820.4399,5514.9902};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall778
			{
				name="";
				position[]={2799.55,5343.3398};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall779
			{
				name="";
				position[]={2967.21,5374.1899};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall780
			{
				name="";
				position[]={2904.03,5216.9702};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall781
			{
				name="";
				position[]={3047.3101,5173.6802};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall782
			{
				name="";
				position[]={3177.1699,5161.7402};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall783
			{
				name="";
				position[]={3269.71,5158.7598};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall784
			{
				name="";
				position[]={3102.04,5102.54};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall785
			{
				name="";
				position[]={2915.47,5030.3999};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall786
			{
				name="";
				position[]={3057.26,5124.4302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall787
			{
				name="";
				position[]={2806.51,5226.9199};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall788
			{
				name="";
				position[]={2888.1001,5362.25};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall789
			{
				name="";
				position[]={3168.21,5456.77};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall790
			{
				name="";
				position[]={3142.8401,5361.25};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall791
			{
				name="";
				position[]={3087.1101,5498.0698};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall792
			{
				name="";
				position[]={3247.8201,5442.8398};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall793
			{
				name="";
				position[]={3236.3701,5517.4702};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall794
			{
				name="";
				position[]={3371.7,5507.02};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall795
			{
				name="";
				position[]={3430.8999,5367.2202};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall796
			{
				name="";
				position[]={3275.1799,5329.9102};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall797
			{
				name="";
				position[]={3375.1799,5230.3999};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall798
			{
				name="";
				position[]={3409.01,5202.04};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall799
			{
				name="";
				position[]={3480.1599,5307.52};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall800
			{
				name="";
				position[]={3196.5701,5278.6602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall801
			{
				name="";
				position[]={2951.29,5636.8799};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall802
			{
				name="";
				position[]={2853.28,5834.3999};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall803
			{
				name="";
				position[]={2969.7,5939.3701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall804
			{
				name="";
				position[]={3070.2,5998.0801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall805
			{
				name="";
				position[]={2905.52,6036.8901};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall806
			{
				name="";
				position[]={2827.8999,6073.21};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall807
			{
				name="";
				position[]={2706.51,5995.5898};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall808
			{
				name="";
				position[]={2662.73,5979.1699};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall809
			{
				name="";
				position[]={2606.51,5896.5898};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall810
			{
				name="";
				position[]={2601.03,5789.1201};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall811
			{
				name="";
				position[]={2680.1399,5748.3198};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall812
			{
				name="";
				position[]={2549.29,6087.1401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall813
			{
				name="";
				position[]={2616.95,6179.6802};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall814
			{
				name="";
				position[]={2754.27,6290.6299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall815
			{
				name="";
				position[]={2745.8101,6225.4502};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall816
			{
				name="";
				position[]={2935.3701,6136.8901};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall817
			{
				name="";
				position[]={3017.46,6050.8198};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall818
			{
				name="";
				position[]={3103.53,6227.4399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall819
			{
				name="";
				position[]={3156.27,6366.25};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall820
			{
				name="";
				position[]={3212.49,6430.9302};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall821
			{
				name="";
				position[]={3202.04,6502.0698};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall822
			{
				name="";
				position[]={3502.05,6556.2998};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall823
			{
				name="";
				position[]={3422.1399,5746.5601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall824
			{
				name="";
				position[]={3484.97,5699.8999};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall825
			{
				name="";
				position[]={3414.05,5590.73};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall826
			{
				name="";
				position[]={3440.8,5503.6299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall827
			{
				name="";
				position[]={3593.22,5504.25};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall828
			{
				name="";
				position[]={3539.72,5632.4102};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall829
			{
				name="";
				position[]={3614.99,5630.23};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall830
			{
				name="";
				position[]={3625.8799,5531.9399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall831
			{
				name="";
				position[]={3584.51,5419.96};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall832
			{
				name="";
				position[]={3759.01,5455.73};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall833
			{
				name="";
				position[]={3836.46,5401.9199};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall834
			{
				name="";
				position[]={3714.8401,5314.5098};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall835
			{
				name="";
				position[]={3782.6499,5208.1401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall836
			{
				name="";
				position[]={3950.6101,5243.9102};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall837
			{
				name="";
				position[]={4036.77,5214.3599};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall838
			{
				name="";
				position[]={4019.6599,5406.27};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall839
			{
				name="";
				position[]={3926.3501,5401.9199};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall840
			{
				name="";
				position[]={4136,5305.4902};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall841
			{
				name="";
				position[]={4216.5601,5319.4902};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall842
			{
				name="";
				position[]={4296.8101,5354.9502};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall843
			{
				name="";
				position[]={4328.54,5254.79};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall844
			{
				name="";
				position[]={4236.1499,5192.8901};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall845
			{
				name="";
				position[]={4459.1802,5331.3101};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall846
			{
				name="";
				position[]={4372.0801,5323.5298};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall847
			{
				name="";
				position[]={4404.1201,5432.71};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall848
			{
				name="";
				position[]={4411.8999,5487.46};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall849
			{
				name="";
				position[]={4505.8301,5503.6299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall850
			{
				name="";
				position[]={4526.9902,5554.96};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall851
			{
				name="";
				position[]={4496.1899,5640.7998};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall852
			{
				name="";
				position[]={4311.1201,5598.5};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall853
			{
				name="";
				position[]={4265.7002,5482.79};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall854
			{
				name="";
				position[]={4036.1499,5500.21};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall855
			{
				name="";
				position[]={3927.5901,5474.0801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall856
			{
				name="";
				position[]={3975.5,5531.6299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall857
			{
				name="";
				position[]={3693.6899,5400.3599};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall858
			{
				name="";
				position[]={3648.5801,5261.0098};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall859
			{
				name="";
				position[]={3600.3701,5364.8999};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall861
			{
				name="";
				position[]={4322.9702,3188.0601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall862
			{
				name="";
				position[]={4210.5298,3084.0801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall863
			{
				name="";
				position[]={4293.1099,3004.97};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall864
			{
				name="";
				position[]={4158.7798,2903.48};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall865
			{
				name="";
				position[]={4056.29,2872.1299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall866
			{
				name="";
				position[]={3898.0801,2909.45};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall867
			{
				name="";
				position[]={3819.47,2986.0701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall868
			{
				name="";
				position[]={3941.8601,2841.79};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall869
			{
				name="";
				position[]={3821.46,2732.3301};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall870
			{
				name="";
				position[]={3752.8,2786.5601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall871
			{
				name="";
				position[]={3765.24,2858.2};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall872
			{
				name="";
				position[]={3801.5601,2943.78};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall873
			{
				name="";
				position[]={3912.01,2706.46};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall874
			{
				name="";
				position[]={4188.1401,2769.1499};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall875
			{
				name="";
				position[]={4234.8999,2829.8401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall876
			{
				name="";
				position[]={4277.1899,2775.1201};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall877
			{
				name="";
				position[]={4301.0801,2613.9199};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall878
			{
				name="";
				position[]={4367.25,2815.4199};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall879
			{
				name="";
				position[]={4451.3301,2943.78};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall880
			{
				name="";
				position[]={4583.6699,2865.6699};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall881
			{
				name="";
				position[]={4632.4302,2793.53};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall882
			{
				name="";
				position[]={4287.6401,2656.71};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall883
			{
				name="";
				position[]={4853.8198,2619.3899};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall884
			{
				name="";
				position[]={4847.8501,2696.51};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall885
			{
				name="";
				position[]={4809.0498,2566.1599};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall886
			{
				name="";
				position[]={4913.0298,2515.4099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall887
			{
				name="";
				position[]={4889.6499,2414.4099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall888
			{
				name="";
				position[]={5104.0801,2393.02};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall889
			{
				name="";
				position[]={5145.8701,2433.3201};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall890
			{
				name="";
				position[]={5271.25,2436.3};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall891
			{
				name="";
				position[]={5198.1099,2496.01};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall892
			{
				name="";
				position[]={5063.7798,2645.76};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall893
			{
				name="";
				position[]={4932.4302,2631.3301};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall894
			{
				name="";
				position[]={5157.3101,2654.22};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall895
			{
				name="";
				position[]={5311.5498,2617.3999};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall896
			{
				name="";
				position[]={5271.25,2526.3601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall897
			{
				name="";
				position[]={5377.7202,2409.9299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall898
			{
				name="";
				position[]={5463.29,2511.4299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall899
			{
				name="";
				position[]={5458.3198,2583.5701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall900
			{
				name="";
				position[]={5568.27,2537.8};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall901
			{
				name="";
				position[]={5544.3901,2435.8101};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall902
			{
				name="";
				position[]={5651.3599,2488.54};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall903
			{
				name="";
				position[]={5701.1099,2452.72};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall904
			{
				name="";
				position[]={5683.2002,2582.5801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall905
			{
				name="";
				position[]={5636.9302,2655.71};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall906
			{
				name="";
				position[]={5559.8101,2648.75};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall907
			{
				name="";
				position[]={5461.2998,2682.0801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall908
			{
				name="";
				position[]={5556.3301,2699.99};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall909
			{
				name="";
				position[]={5629.96,2726.8601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall910
			{
				name="";
				position[]={5698.1201,2757.7};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall911
			{
				name="";
				position[]={5809.0698,2744.27};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall912
			{
				name="";
				position[]={5754.3398,2838.3};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall913
			{
				name="";
				position[]={5609.5601,2844.27};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall914
			{
				name="";
				position[]={5469.2598,2822.8799};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall915
			{
				name="";
				position[]={5345.3799,2738.3};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall916
			{
				name="";
				position[]={5250.3501,2717.8999};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall917
			{
				name="";
				position[]={5285.6802,2826.8601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall918
			{
				name="";
				position[]={5342.8901,2854.72};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall919
			{
				name="";
				position[]={5389.1602,2861.1899};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall920
			{
				name="";
				position[]={5152.8398,2765.6599};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall921
			{
				name="";
				position[]={5031.9399,2793.03};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall922
			{
				name="";
				position[]={5154.8301,2848.25};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall923
			{
				name="";
				position[]={4986.6602,2722.8799};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall924
			{
				name="";
				position[]={4907.5601,2876.6101};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall925
			{
				name="";
				position[]={4969.25,2894.03};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall926
			{
				name="";
				position[]={4919,2957.71};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall927
			{
				name="";
				position[]={5020.5,2984.0801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall928
			{
				name="";
				position[]={5074.73,2928.8501};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall929
			{
				name="";
				position[]={5150.8501,3005.97};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall930
			{
				name="";
				position[]={5216.52,2914.9199};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall931
			{
				name="";
				position[]={5128.46,2885.0701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall932
			{
				name="";
				position[]={5412.54,3000.99};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall933
			{
				name="";
				position[]={5477.2202,2996.02};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall934
			{
				name="";
				position[]={5516.0298,3018.8999};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall935
			{
				name="";
				position[]={5431.9502,2920.8899};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall936
			{
				name="";
				position[]={5640.9102,2992.04};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall937
			{
				name="";
				position[]={5455.8301,3117.9099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall938
			{
				name="";
				position[]={5876.73,3118.4099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall939
			{
				name="";
				position[]={5946.3901,3212.9399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall940
			{
				name="";
				position[]={6103.1099,3098.01};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall941
			{
				name="";
				position[]={6090.1699,3204.48};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall942
			{
				name="";
				position[]={6122.5098,3271.1499};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall943
			{
				name="";
				position[]={6172.2598,3181.1001};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall944
			{
				name="";
				position[]={6199.1299,3089.55};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall945
			{
				name="";
				position[]={6233.96,2966.6599};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall946
			{
				name="";
				position[]={6195.1499,2780.5901};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall947
			{
				name="";
				position[]={6019.52,2642.28};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall948
			{
				name="";
				position[]={6157.8398,2515.4099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall950
			{
				name="";
				position[]={5975.2402,2447.75};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall951
			{
				name="";
				position[]={5825.9902,2572.1299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall952
			{
				name="";
				position[]={6209.7002,2308.6399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall953
			{
				name="";
				position[]={5747.5298,2235.3301};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall954
			{
				name="";
				position[]={5471.02,2207.4399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall955
			{
				name="";
				position[]={5549.9102,1898.26};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall956
			{
				name="";
				position[]={4329.9102,2919.8301};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall957
			{
				name="";
				position[]={4397.6499,2942.1499};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall958
			{
				name="";
				position[]={4539.8901,2998.51};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall959
			{
				name="";
				position[]={4453.3198,3000};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall960
			{
				name="";
				position[]={4425.9502,3088.0601};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall961
			{
				name="";
				position[]={4508.04,3134.3301};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall962
			{
				name="";
				position[]={4576.21,3096.02};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall963
			{
				name="";
				position[]={4815.02,3048.26};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall964
			{
				name="";
				position[]={4754.8198,3112.9399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall965
			{
				name="";
				position[]={4727.9502,3219.4099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall966
			{
				name="";
				position[]={4784.1699,3147.76};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall967
			{
				name="";
				position[]={4864.27,3074.6299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall968
			{
				name="";
				position[]={4912.0298,3086.5701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall969
			{
				name="";
				position[]={5044.8701,3223.8799};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall970
			{
				name="";
				position[]={4841.8799,3256.72};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall971
			{
				name="";
				position[]={4884.6699,3290.55};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall972
			{
				name="";
				position[]={4827.9502,3318.4099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall973
			{
				name="";
				position[]={4948.3501,3307.47};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall974
			{
				name="";
				position[]={4988.1602,3219.8999};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall975
			{
				name="";
				position[]={4988.1602,3099.5};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall976
			{
				name="";
				position[]={5132.4399,3097.02};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall977
			{
				name="";
				position[]={5235.9199,3236.3201};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall978
			{
				name="";
				position[]={5285.1802,3166.1699};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall979
			{
				name="";
				position[]={5352.3398,3111.4399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall980
			{
				name="";
				position[]={5382.2002,3233.3401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall981
			{
				name="";
				position[]={5499.1099,3186.5701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall982
			{
				name="";
				position[]={5766.7798,3232.3401};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall983
			{
				name="";
				position[]={5499.6099,3276.6201};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall984
			{
				name="";
				position[]={5218.5098,3309.96};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall985
			{
				name="";
				position[]={5111.54,3502.5};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall986
			{
				name="";
				position[]={3600.99,2930.01};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall987
			{
				name="";
				position[]={3518.25,3022.0801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall988
			{
				name="";
				position[]={3563.98,2845.4099};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall989
			{
				name="";
				position[]={3313.5801,3028.3};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall990
			{
				name="";
				position[]={3153.3899,3018.3501};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall991
			{
				name="";
				position[]={3286.8201,3125.4199};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall992
			{
				name="";
				position[]={1699.52,4229.8799};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall993
			{
				name="";
				position[]={644.44,8353.6201};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall994
			{
				name="";
				position[]={3430.26,9889.1602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall995
			{
				name="";
				position[]={3620.71,9928.21};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall996
			{
				name="";
				position[]={6155.52,10011.9};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall997
			{
				name="";
				position[]={6288.5898,9662.0596};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall998
			{
				name="";
				position[]={6662.3198,9794.3398};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall999
			{
				name="";
				position[]={6965.9199,9872.4297};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1001
			{
				name="";
				position[]={7192.23,9859.6797};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1002
			{
				name="";
				position[]={7406.5898,10021.4};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1003
			{
				name="";
				position[]={7773.1401,9996.7402};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1004
			{
				name="";
				position[]={7889.48,9503.4805};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1005
			{
				name="";
				position[]={9289.5596,9897.1299};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1006
			{
				name="";
				position[]={9308.6904,9991.1602};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1007
			{
				name="";
				position[]={9583.6104,10164.9};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1008
			{
				name="";
				position[]={9860.1201,10214.3};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1009
			{
				name="";
				position[]={9045.4697,9007.6104};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1010
			{
				name="";
				position[]={8993.7305,9068.2998};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1011
			{
				name="";
				position[]={9106.6602,8906.1104};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1012
			{
				name="";
				position[]={9133.5303,9240.9502};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1013
			{
				name="";
				position[]={9022.0801,9231.4902};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1014
			{
				name="";
				position[]={9055.4199,9115.0703};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1015
			{
				name="";
				position[]={9177.8096,8937.46};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1016
			{
				name="";
				position[]={8979.9902,8949.6104};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1017
			{
				name="";
				position[]={9233.5303,9859.8701};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1018
			{
				name="";
				position[]={7202.6401,8686.7002};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1019
			{
				name="";
				position[]={8219.3799,5438.7002};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1020
			{
				name="";
				position[]={8220.1797,5597.2798};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1021
			{
				name="";
				position[]={8135.71,5520.7798};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1022
			{
				name="";
				position[]={8301.46,5524.77};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1023
			{
				name="";
				position[]={8424.5596,4808.0801};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1024
			{
				name="";
				position[]={7738.8799,4825.54};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1025
			{
				name="";
				position[]={7734.8301,4891.1802};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1027
			{
				name="";
				position[]={7872.3101,3815.4399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1028
			{
				name="";
				position[]={8089.23,3607.48};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1029
			{
				name="";
				position[]={8261.3701,3450.26};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
			class FlatAreaCitySmall1030
			{
				name="";
				position[]={8334.5098,3273.6399};
				type="FlatAreaCitySmall";
				radiusA=10;
				radiusB=10;
			};
		};
	};
};
class CfgWorldList
{
	class fallujah
	{
	};
};
class CfgMissions
{
};
class CfgSurfaces
{
	class Default
	{
	};
	class Water
	{
	};
	class siFallujahTerra: Default
	{
		access=2;
		files="fallujah_d2mesto_*";
		rough=0.050000001;
		dust=0.30000001;
		soundEnviron="dirt";
		character="siFallujahStubbleClutter";
		soundHit="hard_ground";
	};
	class siFallujahAcqua: Default
	{
		access=2;
		files="fallujah_ut_hlina_*";
		rough=0.050000001;
		dust=0.1;
		soundEnviron="dirt";
		character="Empty";
		soundHit="soft_ground";
	};
	class siFallujahErba: Default
	{
		access=2;
		files="fallujah_d2trava_*";
		rough=0.11;
		dust=0.1;
		soundEnviron="grass";
		character="siFallujahErbaClutter";
		soundHit="soft_ground";
	};
	class siFallujahSabbia: Default
	{
		access=2;
		files="fallujah_d2pisek_*";
		rough=0.029999999;
		dust=0.69999999;
		soundEnviron="rock";
		character="siFallujahStubbleClutter";
		soundHit="soft_ground";
	};
	class siFallujahCitta: Default
	{
		access=2;
		files="fallujah_pisek_*";
		rough=0.029999999;
		dust=0.69999999;
		soundEnviron="rock";
		character="siFallujahStubbleClutter";
		soundHit="soft_ground";
	};
};
class CfgSurfaceCharacters
{
	class siFallujahErbaClutter
	{
		probability[]={0.1,0.050000001,0.050000001,0.050000001};
		names[]=
		{
			"si_StubbleClutter",
			"si_GrassCrooked",
			"si_WeedDeadSmall",
			"si_WeedDead"
		};
	};
	class siTallGrassClutter
	{
		probability[]={0.40000001,0.2,0.30000001,0.07,0.02,0.0099999998};
		names[]=
		{
			"si_GrassTall",
			"si_AutumnFlowers",
			"si_GrassBunch",
			"si_GrassCrooked",
			"si_WeedDead",
			"si_WeedDeadSmall"
		};
	};
	class siGrassWClutter
	{
		probability[]={0.64999998,0.17,0.1,0.050000001,0.029999999};
		names[]=
		{
			"si_GrassCrooked",
			"si_GrassCrookedGreen",
			"si_AutumnFlowers",
			"si_WeedDead",
			"si_WeedDeadSmall"
		};
	};
	class siFallujahTallGrassWClutter
	{
		probability[]={0.1,0.050000001,0.050000001,0.050000001,0.2};
		names[]=
		{
			"si_GrassTall",
			"si_GrassBunch",
			"si_GrassCrookedGreen",
			"si_GrassCrooked",
			"si_StubbleClutter"
		};
	};
	class siForestMixedClutter
	{
		probability[]={0.2,0.1,0.2,0.001,0.003};
		names[]=
		{
			"si_GrassCrookedForest",
			"si_FernAutumn",
			"si_FernAutumnTall",
			"si_MushroomsHorcak",
			"si_MushroomsPrasivka"
		};
	};
	class siForestFirClutter
	{
		probability[]={0.40000001,0.1,0.1,0.15000001,0.050000001,0.003,0.0049999999,0.0080000004,0.0040000002};
		names[]=
		{
			"si_BlueBerry",
			"si_FernAutumn",
			"si_FernAutumnTall",
			"si_SmallPicea",
			"si_RaspBerry",
			"si_MushroomsHorcak",
			"si_MushroomsPrasivka",
			"si_MushroomsBabka",
			"si_MushroomsMuchomurka"
		};
	};
	class siHeatherClutter
	{
		probability[]={0.15000001,0.5,0.30000001,0.1};
		names[]=
		{
			"si_BlueBerry",
			"si_HeatherBrush",
			"si_GrassCrooked",
			"si_WeedSedge"
		};
	};
	class siFallujahStubbleClutter
	{
		probability[]={0.1,0.0049999999,0.0049999999,0.0049999999};
		names[]=
		{
			"si_StubbleClutter",
			"si_AutumnFlowers",
			"si_WeedDeadSmall",
			"si_WeedDead"
		};
	};
};
