class CfgPatches
{
	class asr_mapgrids_fallujah
	{
		units[]={};
		weapons[]={};
		requiredVersion=1;
		requiredAddons[]=
		{
			"fallujah_v1_0"
		};
		author[]=
		{
			"Robalo"
		};
	};
};
class CfgWorlds
{
	class CAWorld;
	class Utes: CAWorld
	{
		class Grid;
	};
	class fallujah: Utes
	{
		class Grid: Grid
		{
			offsetY=10240;
		};
	};
};
