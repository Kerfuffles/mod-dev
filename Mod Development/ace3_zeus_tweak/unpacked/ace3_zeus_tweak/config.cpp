class CfgPatches {
	class cbb_tweaks_ace3_zeus {
		name = "CBB Tweaks ACE3 (Zeus)";
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"ace_zeus"};
		author = "Clear Backblast!";
		Url = "http://www.reddit.com/r/clearbackblast/";
	};
};

class CBA_Extended_EventHandlers;
class EventHandlers;
class CfgVehicles {
	class Module_F;
	class ModuleCurator_F : Module_F {
		class EventHandlers : EventHandlers {
			class CBA_Extended_EventHandlers : CBA_Extended_EventHandlers {};
		};
	};
};

class Extended_InitPost_EventHandlers {
	class ModuleCurator_F {
		class cbb_tweaks_ace3_zeus {
			serverInit = "call compile preprocessfilelinenumbers 'ace3_zeus_tweak\functions\fnc_hookPingEvent.sqf';";
		};
	};
};