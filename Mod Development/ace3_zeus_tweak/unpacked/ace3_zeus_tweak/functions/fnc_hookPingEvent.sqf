params ['_logic'];

_logic addEventHandler ["curatorPinged",{
	params ['_logic','_player'];
	diag_log format ["[ZEUS PING] Player: %1 | UID: %2 | Game Time: %3 | Logic: %4",(name _player),(getPlayerUID _player),time,_logic];
}];