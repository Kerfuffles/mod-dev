class CfgWeapons
{
	class Rifle;
	class Rifle_Base_F: Rifle{};
	class Rifle_Long_Base_F: Rifle_Base_F{};
	
	class mk20_base_F: Rifle_Base_F{};
	class arifle_Mk20_F: mk20_base_F{};
	class arifle_mk20_plain_f: arifle_Mk20_F{};
	
	/*rifles*/
	class UK3CB_BAF_L85A2: arifle_mk20_plain_f
	{
		magazines[] += {
			"ACE_30Rnd_556x45_Stanag_M995_AP_mag",
			"ACE_30Rnd_556x45_Stanag_Mk262_mag",
			"ACE_30Rnd_556x45_Stanag_Mk318_mag",
			"ACE_30Rnd_556x45_Stanag_Tracer_Dim",
            "hlc_30rnd_556x45_EPR",
			"hlc_30rnd_556x45_SOST",
			"hlc_30rnd_556x45_SPR",
			"hlc_30rnd_556x45_S",
			"hlc_30rnd_556x45_MDim",
			"hlc_30rnd_556x45_TDim",
			"hlc_50rnd_556x45_EPR",
			"rhs_mag_30Rnd_556x45_Mk318_Stanag",
			"rhs_mag_30Rnd_556x45_Mk262_Stanag",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_No_Tracer",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Green",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Yellow",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Orange",
			"rhs_mag_30Rnd_556x45_M200_Stanag"
        };
	};
	class UK3CB_BAF_L119_Base: arifle_mk20_plain_f
	{
		magazines[] += {
			"ACE_30Rnd_556x45_Stanag_M995_AP_mag",
			"ACE_30Rnd_556x45_Stanag_Mk262_mag",
			"ACE_30Rnd_556x45_Stanag_Mk318_mag",
			"ACE_30Rnd_556x45_Stanag_Tracer_Dim",
            "hlc_30rnd_556x45_EPR",
			"hlc_30rnd_556x45_SOST",
			"hlc_30rnd_556x45_SPR",
			"hlc_30rnd_556x45_S",
			"hlc_30rnd_556x45_MDim",
			"hlc_30rnd_556x45_TDim",
			"hlc_50rnd_556x45_EPR",
			"rhs_mag_30Rnd_556x45_Mk318_Stanag",
			"rhs_mag_30Rnd_556x45_Mk262_Stanag",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_No_Tracer",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Green",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Yellow",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Orange",
			"rhs_mag_30Rnd_556x45_M200_Stanag"
        };
	};
	
	/*MG*/
	class UK3CB_BAF_L110_Base: Rifle_Long_Base_F{};
	
	class UK3CB_BAF_L110_556_Base: UK3CB_BAF_L110_Base
	{
		magazines[] += {
			"ACE_30Rnd_556x45_Stanag_M995_AP_mag",
			"ACE_30Rnd_556x45_Stanag_Mk262_mag",
			"ACE_30Rnd_556x45_Stanag_Mk318_mag",
			"ACE_30Rnd_556x45_Stanag_Tracer_Dim",
			"hlc_200rnd_556x45_M_SAW",
			"hlc_200rnd_556x45_B_SAW",
			"hlc_200rnd_556x45_T_SAW",
			"hlc_200rnd_556x45_Mdim_SAW",
			"hlc_30rnd_556x45_EPR",
			"hlc_30rnd_556x45_SOST",
			"hlc_30rnd_556x45_SPR",
			"hlc_30rnd_556x45_S",
			"hlc_30rnd_556x45_MDim",
			"hlc_30rnd_556x45_TDim",
			"hlc_50rnd_556x45_EPR",
			"rhs_mag_30Rnd_556x45_Mk318_Stanag",
			"rhs_mag_30Rnd_556x45_Mk262_Stanag",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_No_Tracer",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Green",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Yellow",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Orange",
			"rhs_mag_30Rnd_556x45_M200_Stanag"
		};
	};
	
	/*Long Range*/
	
	class EBR_base_F: Rifle_Long_Base_F{};
	class srifle_EBR_F: EBR_base_F{};
	
	class UK3CB_BAF_L129A1: srifle_EBR_F
	{
		magazines[] += {
            "hlc_20rnd_762x51_b_G3",
			"hlc_20rnd_762x51_Mk316_G3",
			"hlc_20rnd_762x51_barrier_G3",
			"hlc_20rnd_762x51_T_G3",
			"hlc_20rnd_762x51_MDim_G3",
			"hlc_50rnd_762x51_M_G3",
			"hlc_50rnd_762x51_MDIM_G3",
			"hlc_20rnd_762x51_IRDim_G3",
			"hlc_20rnd_762x51_S_G3",
			"ACE_20Rnd_762x51_Mag_Tracer",
            "ACE_20Rnd_762x51_Mag_Tracer_Dim",
            "ACE_20Rnd_762x51_Mk316_Mod_0_Mag",
            "ACE_20Rnd_762x51_M118LR_Mag",
            "ACE_20Rnd_762x51_Mk319_Mod_0_Mag",
            "ACE_20Rnd_762x51_M993_AP_Mag",
            "ACE_20Rnd_762x51_Mag_SD",
			"rhsusf_20Rnd_762x51_m118_special_Mag",
			"rhsusf_20Rnd_762x51_m993_Mag",
			"rhsusf_20Rnd_762x51_m62_Mag"
			
        };
	};
};