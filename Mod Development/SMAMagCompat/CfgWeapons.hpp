class CfgWeapons
{
	class RifleCore;
	class GrenadeLauncher;

	class Rifle: RifleCore
	{
	};

	class Rifle_Base_F: Rifle
	{
	};

	class mk20_base_f: Rifle_Base_F
	{
	};

	class SMA_AssaultBase: mk20_base_F
	{
	};
	
	class UGL_F: GrenadeLauncher
	{
		class Single;
	};

	class SMA_556_RIFLEBASE: SMA_AssaultBase
	{
		magazines[] += 
		{
			"ACE_30Rnd_556x45_Stanag_M995_AP_mag",
			"ACE_30Rnd_556x45_Stanag_Mk262_mag",
			"ACE_30Rnd_556x45_Stanag_Mk318_mag",
			"ACE_30Rnd_556x45_Stanag_Tracer_Dim",
            "hlc_30rnd_556x45_EPR",
			"hlc_30rnd_556x45_SOST",
			"hlc_30rnd_556x45_SPR",
			"hlc_30rnd_556x45_S",
			"hlc_30rnd_556x45_MDim",
			"hlc_30rnd_556x45_TDim",
			"hlc_50rnd_556x45_EPR",
			"rhs_mag_30Rnd_556x45_Mk318_Stanag",
			"rhs_mag_30Rnd_556x45_Mk262_Stanag",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_No_Tracer",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Green",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Yellow",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Orange",
			"rhs_mag_30Rnd_556x45_M200_Stanag"
		};
		
		
	};
	class SMA_762_RIFLEBASE: SMA_AssaultBase
	{
		magazines[] += 
		{
			"hlc_20rnd_762x51_b_G3",
			"hlc_20rnd_762x51_Mk316_G3",
			"hlc_20rnd_762x51_barrier_G3",
			"hlc_20rnd_762x51_T_G3",
			"hlc_20rnd_762x51_MDim_G3",
			"hlc_50rnd_762x51_M_G3",
			"hlc_50rnd_762x51_MDIM_G3",
			"hlc_20rnd_762x51_IRDim_G3",
			"hlc_20rnd_762x51_S_G3",
			"ACE_20Rnd_762x51_Mag_Tracer",
            "ACE_20Rnd_762x51_Mag_Tracer_Dim",
            "ACE_20Rnd_762x51_Mk316_Mod_0_Mag",
            "ACE_20Rnd_762x51_M118LR_Mag",
            "ACE_20Rnd_762x51_Mk319_Mod_0_Mag",
            "ACE_20Rnd_762x51_M993_AP_Mag",
            "ACE_20Rnd_762x51_Mag_SD",
			"rhsusf_20Rnd_762x51_m118_special_Mag",
			"rhsusf_20Rnd_762x51_m993_Mag",
			"rhsusf_20Rnd_762x51_m62_Mag"
		};
	};
	class SMA_AAC_MPW_BASE: SMA_556_RIFLEBASE
	{
		magazines[] = 
		{
			"29rnd_300BLK_STANAG_T",
			"29rnd_300BLK_STANAG_S",
			"SMA_30Rnd_762x35_BLK_EPR",
			"SMA_30Rnd_762x35_SS"
		};
	};

	class SMA_ACRGL : SMA_556_RIFLEBASE
	{
		class ACR_GL : UGL_F
		{
			magazines[] = 
			{
				"rhs_mag_M441_HE",
				"rhs_mag_M433_HEDP",
				"rhs_mag_M781_Practice",
				"rhs_mag_m4009",
				"rhs_mag_m576",
				"rhs_mag_M585_white",
				"rhs_mag_m661_green",
				"rhs_mag_m662_red",
				"rhs_mag_m713_Red",
				"rhs_mag_m714_White",
				"rhs_mag_m715_Green",
				"rhs_mag_m716_yellow",
				"1Rnd_HE_Grenade_shell",
				"1Rnd_SmokeBlue_Grenade_shell",
				"1Rnd_SmokeGreen_Grenade_shell",
				"1Rnd_SmokeOrange_Grenade_shell",
				"1Rnd_SmokePurple_Grenade_shell",
				"1Rnd_SmokeRed_Grenade_shell",
				"1Rnd_Smoke_Grenade_shell",
				"1Rnd_SmokeYellow_Grenade_shell",
				"UGL_FlareYellow_F",
				"UGL_FlareWhite_F",
				"UGL_FlareRed_F",
				"UGL_FlareGreen_F",
				"UGL_FlareCIR_F"
			};
		};
	};
	
	class SMA_ACRGL_B : SMA_ACRGL
	{
		class ACR_GL : UGL_F
		{
			magazines[] = 
			{
				"rhs_mag_M441_HE",
				"rhs_mag_M433_HEDP",
				"rhs_mag_M781_Practice",
				"rhs_mag_m4009",
				"rhs_mag_m576",
				"rhs_mag_M585_white",
				"rhs_mag_m661_green",
				"rhs_mag_m662_red",
				"rhs_mag_m713_Red",
				"rhs_mag_m714_White",
				"rhs_mag_m715_Green",
				"rhs_mag_m716_yellow",
				"1Rnd_HE_Grenade_shell",
				"1Rnd_SmokeBlue_Grenade_shell",
				"1Rnd_SmokeGreen_Grenade_shell",
				"1Rnd_SmokeOrange_Grenade_shell",
				"1Rnd_SmokePurple_Grenade_shell",
				"1Rnd_SmokeRed_Grenade_shell",
				"1Rnd_Smoke_Grenade_shell",
				"1Rnd_SmokeYellow_Grenade_shell",
				"UGL_FlareYellow_F",
				"UGL_FlareWhite_F",
				"UGL_FlareRed_F",
				"UGL_FlareGreen_F",
				"UGL_FlareCIR_F"
			};
		};
	};

	/*use these grenade classes*/
	/*
	"rhs_mag_M441_HE",
	"rhs_mag_M433_HEDP",
	"rhs_mag_M781_Practice",
	"rhs_mag_m4009",
	"rhs_mag_m576",
	"rhs_mag_M585_white",
	"rhs_mag_m661_green",
	"rhs_mag_m662_red",
	"rhs_mag_m713_Red",
	"rhs_mag_m714_White",
	"rhs_mag_m715_Green",
	"rhs_mag_m716_yellow",
	"1Rnd_HE_Grenade_shell",
	"1Rnd_SmokeBlue_Grenade_shell",
	"1Rnd_SmokeGreen_Grenade_shell",
	"1Rnd_SmokeOrange_Grenade_shell",
	"1Rnd_SmokePurple_Grenade_shell",
	"1Rnd_SmokeRed_Grenade_shell",
	"1Rnd_Smoke_Grenade_shell",
	"1Rnd_SmokeYellow_Grenade_shell",
	"UGL_FlareYellow_F",
	"UGL_FlareWhite_F",
	"UGL_FlareRed_F",
	"UGL_FlareGreen_F",
	"UGL_FlareCIR_F"

	*/
};