class CfgPatches
{
	class SMA_ACE3_HLC_RHS_MAG_COMPAT
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Weapons_F","hlcweapons_core","ace_common","hlcweapons_g3","rhsusf_c_weapons","hlcweapons_ar15","ace_ballistics","SMA_Weapons","SMA_AAC_MPW"};
		version = "1";
		projectName = "IEU Compatibility";
		author = "V. Zeitsev";
	};
};

#include "CfgWeapons.hpp"