class CfgWeapons
{
	class Rifle;
	class Rifle_Base_F: Rifle
	{
	};
	class hlc_g3_base: Rifle_Base_F
	{
		
	};
	
	class hlc_rifle_g3sg1: hlc_g3_base
	{
		magazines[] = {
			"hlc_20rnd_762x51_b_G3",
			"hlc_20rnd_762x51_mk316_G3",
			"hlc_20rnd_762x51_barrier_G3",
			"hlc_20rnd_762x51_T_G3",
			"hlc_20rnd_762x51_IRDim_G3",
			"hlc_20rnd_762x51_MDIM_G3",
			"hlc_50rnd_762x51_M_G3",
			"hlc_50rnd_762x51_MDIM_G3",
			"hlc_20rnd_762x51_S_G3",
            "ACE_20Rnd_762x51_Mag_Tracer",
            "ACE_20Rnd_762x51_Mag_Tracer_Dim",
            "ACE_20Rnd_762x51_Mk316_Mod_0_Mag",
            "ACE_20Rnd_762x51_M118LR_Mag",
            "ACE_20Rnd_762x51_Mk319_Mod_0_Mag",
            "ACE_20Rnd_762x51_M993_AP_Mag",
            "ACE_20Rnd_762x51_Mag_SD"
		};
	};
	class hlc_rifle_psg1: hlc_g3_base
	{
		magazines[] = {
			"hlc_20rnd_762x51_b_G3",
			"hlc_20rnd_762x51_mk316_G3",
			"hlc_20rnd_762x51_barrier_G3",
			"hlc_20rnd_762x51_T_G3",
			"hlc_20rnd_762x51_IRDim_G3",
			"hlc_20rnd_762x51_MDIM_G3",
			"hlc_50rnd_762x51_M_G3",
			"hlc_50rnd_762x51_MDIM_G3",
			"hlc_20rnd_762x51_S_G3",
            "ACE_20Rnd_762x51_Mag_Tracer",
            "ACE_20Rnd_762x51_Mag_Tracer_Dim",
            "ACE_20Rnd_762x51_Mk316_Mod_0_Mag",
            "ACE_20Rnd_762x51_M118LR_Mag",
            "ACE_20Rnd_762x51_Mk319_Mod_0_Mag",
            "ACE_20Rnd_762x51_M993_AP_Mag",
            "ACE_20Rnd_762x51_Mag_SD"
		};
	};
	class hlc_rifle_psg1A1: hlc_g3_base
	{
		magazines[] = {
			"hlc_20rnd_762x51_b_G3",
			"hlc_20rnd_762x51_mk316_G3",
			"hlc_20rnd_762x51_barrier_G3",
			"hlc_20rnd_762x51_T_G3",
			"hlc_20rnd_762x51_IRDim_G3",
			"hlc_20rnd_762x51_MDIM_G3",
			"hlc_50rnd_762x51_M_G3",
			"hlc_50rnd_762x51_MDIM_G3",
			"hlc_20rnd_762x51_S_G3",
            "ACE_20Rnd_762x51_Mag_Tracer",
            "ACE_20Rnd_762x51_Mag_Tracer_Dim",
            "ACE_20Rnd_762x51_Mk316_Mod_0_Mag",
            "ACE_20Rnd_762x51_M118LR_Mag",
            "ACE_20Rnd_762x51_Mk319_Mod_0_Mag",
            "ACE_20Rnd_762x51_M993_AP_Mag",
            "ACE_20Rnd_762x51_Mag_SD"
		};
	};
	class hlc_rifle_PSG1A1_RIS: hlc_g3_base
	{
		magazines[] = {
			"hlc_20rnd_762x51_b_G3",
			"hlc_20rnd_762x51_mk316_G3",
			"hlc_20rnd_762x51_barrier_G3",
			"hlc_20rnd_762x51_T_G3",
			"hlc_20rnd_762x51_IRDim_G3",
			"hlc_20rnd_762x51_MDIM_G3",
			"hlc_50rnd_762x51_M_G3",
			"hlc_50rnd_762x51_MDIM_G3",
			"hlc_20rnd_762x51_S_G3",
            "ACE_20Rnd_762x51_Mag_Tracer",
            "ACE_20Rnd_762x51_Mag_Tracer_Dim",
            "ACE_20Rnd_762x51_Mk316_Mod_0_Mag",
            "ACE_20Rnd_762x51_M118LR_Mag",
            "ACE_20Rnd_762x51_Mk319_Mod_0_Mag",
            "ACE_20Rnd_762x51_M993_AP_Mag",
            "ACE_20Rnd_762x51_Mag_SD"
		};
	};
	class hlc_rifle_g3a3: hlc_rifle_g3sg1
	{
		magazines[] = {
			"hlc_20rnd_762x51_b_G3",
			"hlc_20rnd_762x51_mk316_G3",
			"hlc_20rnd_762x51_barrier_G3",
			"hlc_20rnd_762x51_T_G3",
			"hlc_20rnd_762x51_IRDim_G3",
			"hlc_20rnd_762x51_MDIM_G3",
			"hlc_50rnd_762x51_M_G3",
			"hlc_50rnd_762x51_MDIM_G3",
			"hlc_20rnd_762x51_S_G3",
            "ACE_20Rnd_762x51_Mag_Tracer",
            "ACE_20Rnd_762x51_Mag_Tracer_Dim",
            "ACE_20Rnd_762x51_Mk316_Mod_0_Mag",
            "ACE_20Rnd_762x51_M118LR_Mag",
            "ACE_20Rnd_762x51_Mk319_Mod_0_Mag",
            "ACE_20Rnd_762x51_M993_AP_Mag",
            "ACE_20Rnd_762x51_Mag_SD"
		};
	};
	class hlc_rifle_g3a3ris: hlc_rifle_g3a3
	{
		magazines[] = {
			"hlc_20rnd_762x51_b_G3",
			"hlc_20rnd_762x51_mk316_G3",
			"hlc_20rnd_762x51_barrier_G3",
			"hlc_20rnd_762x51_T_G3",
			"hlc_20rnd_762x51_IRDim_G3",
			"hlc_20rnd_762x51_MDIM_G3",
			"hlc_50rnd_762x51_M_G3",
			"hlc_50rnd_762x51_MDIM_G3",
			"hlc_20rnd_762x51_S_G3",
            "ACE_20Rnd_762x51_Mag_Tracer",
            "ACE_20Rnd_762x51_Mag_Tracer_Dim",
            "ACE_20Rnd_762x51_Mk316_Mod_0_Mag",
            "ACE_20Rnd_762x51_M118LR_Mag",
            "ACE_20Rnd_762x51_Mk319_Mod_0_Mag",
            "ACE_20Rnd_762x51_M993_AP_Mag",
            "ACE_20Rnd_762x51_Mag_SD"
		};
	};
	class hlc_rifle_g3a3v: hlc_rifle_g3a3
	{
		magazines[] = {
			"hlc_20rnd_762x51_b_G3",
			"hlc_20rnd_762x51_mk316_G3",
			"hlc_20rnd_762x51_barrier_G3",
			"hlc_20rnd_762x51_T_G3",
			"hlc_20rnd_762x51_IRDim_G3",
			"hlc_20rnd_762x51_MDIM_G3",
			"hlc_50rnd_762x51_M_G3",
			"hlc_50rnd_762x51_MDIM_G3",
			"hlc_20rnd_762x51_S_G3",
            "ACE_20Rnd_762x51_Mag_Tracer",
            "ACE_20Rnd_762x51_Mag_Tracer_Dim",
            "ACE_20Rnd_762x51_Mk316_Mod_0_Mag",
            "ACE_20Rnd_762x51_M118LR_Mag",
            "ACE_20Rnd_762x51_Mk319_Mod_0_Mag",
            "ACE_20Rnd_762x51_M993_AP_Mag",
            "ACE_20Rnd_762x51_Mag_SD"
		};
	};
	class hlc_rifle_g3ka4: hlc_rifle_g3a3
	{
		magazines[] = {
			"hlc_20rnd_762x51_b_G3",
			"hlc_20rnd_762x51_mk316_G3",
			"hlc_20rnd_762x51_barrier_G3",
			"hlc_20rnd_762x51_T_G3",
			"hlc_20rnd_762x51_IRDim_G3",
			"hlc_20rnd_762x51_MDIM_G3",
			"hlc_50rnd_762x51_M_G3",
			"hlc_50rnd_762x51_MDIM_G3",
			"hlc_20rnd_762x51_S_G3",
            "ACE_20Rnd_762x51_Mag_Tracer",
            "ACE_20Rnd_762x51_Mag_Tracer_Dim",
            "ACE_20Rnd_762x51_Mk316_Mod_0_Mag",
            "ACE_20Rnd_762x51_M118LR_Mag",
            "ACE_20Rnd_762x51_Mk319_Mod_0_Mag",
            "ACE_20Rnd_762x51_M993_AP_Mag",
            "ACE_20Rnd_762x51_Mag_SD"
		};
	};
	class HLC_Rifle_g3ka4_GL: hlc_rifle_g3a3ris
	{
		magazines[] = {
			"hlc_20rnd_762x51_b_G3",
			"hlc_20rnd_762x51_mk316_G3",
			"hlc_20rnd_762x51_barrier_G3",
			"hlc_20rnd_762x51_T_G3",
			"hlc_20rnd_762x51_IRDim_G3",
			"hlc_20rnd_762x51_MDIM_G3",
			"hlc_50rnd_762x51_M_G3",
			"hlc_50rnd_762x51_MDIM_G3",
			"hlc_20rnd_762x51_S_G3",
            "ACE_20Rnd_762x51_Mag_Tracer",
            "ACE_20Rnd_762x51_Mag_Tracer_Dim",
            "ACE_20Rnd_762x51_Mk316_Mod_0_Mag",
            "ACE_20Rnd_762x51_M118LR_Mag",
            "ACE_20Rnd_762x51_Mk319_Mod_0_Mag",
            "ACE_20Rnd_762x51_M993_AP_Mag",
            "ACE_20Rnd_762x51_Mag_SD"
		};
	};
	class hlc_rifle_hk51: hlc_rifle_g3sg1
	{
		magazines[] = {
			"hlc_20rnd_762x51_b_G3",
			"hlc_20rnd_762x51_mk316_G3",
			"hlc_20rnd_762x51_barrier_G3",
			"hlc_20rnd_762x51_T_G3",
			"hlc_20rnd_762x51_IRDim_G3",
			"hlc_20rnd_762x51_MDIM_G3",
			"hlc_50rnd_762x51_M_G3",
			"hlc_50rnd_762x51_MDIM_G3",
			"hlc_20rnd_762x51_S_G3",
            "ACE_20Rnd_762x51_Mag_Tracer",
            "ACE_20Rnd_762x51_Mag_Tracer_Dim",
            "ACE_20Rnd_762x51_Mk316_Mod_0_Mag",
            "ACE_20Rnd_762x51_M118LR_Mag",
            "ACE_20Rnd_762x51_Mk319_Mod_0_Mag",
            "ACE_20Rnd_762x51_M993_AP_Mag",
            "ACE_20Rnd_762x51_Mag_SD"
		};
	};
	class hlc_rifle_hk53: hlc_g3_base
	{
		magazines[] = {
			"ACE_30Rnd_556x45_Stanag_M995_AP_mag",
			"ACE_30Rnd_556x45_Stanag_Mk262_mag",
			"ACE_30Rnd_556x45_Stanag_Mk318_mag",
			"ACE_30Rnd_556x45_Stanag_Tracer_Dim",
            "hlc_30rnd_556x45_EPR",
			"hlc_30rnd_556x45_SOST",
			"hlc_30rnd_556x45_SPR",
			"hlc_30rnd_556x45_S",
			"hlc_30rnd_556x45_MDim",
			"hlc_30rnd_556x45_TDim",
			"hlc_50rnd_556x45_EPR",
			"rhs_mag_30Rnd_556x45_Mk318_Stanag",
			"rhs_mag_30Rnd_556x45_Mk262_Stanag",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_No_Tracer",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Green",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Yellow",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Orange",
			"rhs_mag_30Rnd_556x45_M200_Stanag"
		};
	};
	class hlc_rifle_hk53RAS: hlc_g3_base
	{
		magazines[] = {
			"ACE_30Rnd_556x45_Stanag_M995_AP_mag",
			"ACE_30Rnd_556x45_Stanag_Mk262_mag",
			"ACE_30Rnd_556x45_Stanag_Mk318_mag",
			"ACE_30Rnd_556x45_Stanag_Tracer_Dim",
            "hlc_30rnd_556x45_EPR",
			"hlc_30rnd_556x45_SOST",
			"hlc_30rnd_556x45_SPR",
			"hlc_30rnd_556x45_S",
			"hlc_30rnd_556x45_MDim",
			"hlc_30rnd_556x45_TDim",
			"hlc_50rnd_556x45_EPR",
			"rhs_mag_30Rnd_556x45_Mk318_Stanag",
			"rhs_mag_30Rnd_556x45_Mk262_Stanag",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_No_Tracer",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Green",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Yellow",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Orange",
			"rhs_mag_30Rnd_556x45_M200_Stanag"
		};
	};
	class hlc_rifle_hk33a2: hlc_g3_base
	{
		magazines[] = {
			"ACE_30Rnd_556x45_Stanag_M995_AP_mag",
			"ACE_30Rnd_556x45_Stanag_Mk262_mag",
			"ACE_30Rnd_556x45_Stanag_Mk318_mag",
			"ACE_30Rnd_556x45_Stanag_Tracer_Dim",
            "hlc_30rnd_556x45_EPR",
			"hlc_30rnd_556x45_SOST",
			"hlc_30rnd_556x45_SPR",
			"hlc_30rnd_556x45_S",
			"hlc_30rnd_556x45_MDim",
			"hlc_30rnd_556x45_TDim",
			"hlc_50rnd_556x45_EPR",
			"rhs_mag_30Rnd_556x45_Mk318_Stanag",
			"rhs_mag_30Rnd_556x45_Mk262_Stanag",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_No_Tracer",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Green",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Yellow",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Orange",
			"rhs_mag_30Rnd_556x45_M200_Stanag"
		};
	};
	class hlc_rifle_hk33a2RIS: hlc_g3_base
	{
		magazines[] = {
			"ACE_30Rnd_556x45_Stanag_M995_AP_mag",
			"ACE_30Rnd_556x45_Stanag_Mk262_mag",
			"ACE_30Rnd_556x45_Stanag_Mk318_mag",
			"ACE_30Rnd_556x45_Stanag_Tracer_Dim",
            "hlc_30rnd_556x45_EPR",
			"hlc_30rnd_556x45_SOST",
			"hlc_30rnd_556x45_SPR",
			"hlc_30rnd_556x45_S",
			"hlc_30rnd_556x45_MDim",
			"hlc_30rnd_556x45_TDim",
			"hlc_50rnd_556x45_EPR",
			"rhs_mag_30Rnd_556x45_Mk318_Stanag",
			"rhs_mag_30Rnd_556x45_Mk262_Stanag",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_No_Tracer",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Green",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Yellow",
			"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Orange",
			"rhs_mag_30Rnd_556x45_M200_Stanag"
		};
	};
	
};