class CfgWeapons
{
    class Rifle;
    class Rifle_Base_F : Rifle
	{
    };

    class hlc_fal_base : Rifle_Base_F
    {
		magazines[] =
	    {
			"hlc_20rnd_762x51_b_G3",
			"hlc_20rnd_762x51_Mk316_G3",
			"hlc_20rnd_762x51_barrier_G3",
			"hlc_20rnd_762x51_T_G3",
			"hlc_20rnd_762x51_MDim_G3",
			"hlc_50rnd_762x51_M_G3",
			"hlc_50rnd_762x51_MDIM_G3",
			"hlc_20rnd_762x51_IRDim_G3",
			"hlc_20rnd_762x51_S_G3",
			"ACE_20Rnd_762x51_Mag_Tracer",
			"ACE_20Rnd_762x51_Mag_Tracer_Dim",
			"ACE_20Rnd_762x51_Mk316_Mod_0_Mag",
			"ACE_20Rnd_762x51_M118LR_Mag",
			"ACE_20Rnd_762x51_Mk319_Mod_0_Mag",
			"ACE_20Rnd_762x51_M993_AP_Mag",
			"ACE_20Rnd_762x51_Mag_SD",
			"rhsusf_20Rnd_762x51_m118_special_Mag",
			"rhsusf_20Rnd_762x51_m993_Mag",
			"rhsusf_20Rnd_762x51_m62_Mag"
		};
	};
};
