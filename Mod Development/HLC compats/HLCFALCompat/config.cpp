class CfgPatches
{
	class HLCFAL_ACE3_HLC_Mag_Compat
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = 
		{
			"A3_Weapons_F",
			"hlcweapons_core",
			"ace_common",
			"ace_ballistics",
			"rhsusf_c_weapons",
			"hlcweapons_g3",
			"hlcweapons_falpocalypse"
		};

		version = "1";
		projectName = "IEU Compatibility";
		author = "D. Nuke";
	};
};

#include "CfgWeapons.hpp"