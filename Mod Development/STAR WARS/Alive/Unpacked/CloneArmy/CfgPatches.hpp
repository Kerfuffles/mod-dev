class CfgPatches {
    class GALACTIC_REPUBLIC {
        units[] = {
            "SW_BARC"
        };
        weapons[] = {};
        requiredVersion = 1.62;
        requiredAddons[] = {
            "SW_SpeederBike_barc"
        };
        author = "[RM] Cpt Smith";
        authors[] = { "[RM] Cpt Smith" };
    };
};
