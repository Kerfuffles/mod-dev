class CfgPatches {
    class SW_Civs {
        units[] = {
            "C_SWCivs_Man_1_01",
            "C_SWCivs_Man_2_01",
            "C_SWCivs_Man_3_01",
            "C_SWCivs_Man_4_01",
            "C_SWCivs_Man_5_01",
            "C_SWCivs_Man_6_01",
            "C_SWCivs_Man_7_01",
            "C_SWCivs_Man_8_01",
            "C_SWCivs_Man_9_01",
            "C_SWCivs_Man_10_01",
            "C_SWCivs_Man_11_01",
            "C_SWCivs_Man_12_01",
            "C_SWCivs_Civilian_Car_1_01",
            "C_SWCivs_Civilian_Speeder_01"
        };
        weapons[] = {};
        requiredVersion = 1.62;
        requiredAddons[] = {
            "SWOP_Characters_Rebel",
            "JM_landspeeder",
            "SW_SpeederBike"
        };
        author = "Lemon Mission Creator";
        authors[] = { "Lemon Mission Creator" };
    };
};
