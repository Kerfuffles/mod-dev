//////////////////////////////////////////////////////////////////////////////////
// Config Automatically Generated by ALiVE ORBAT Creator
// Generated with Arma 3 version 170.141838 on Stable branch
// Generated with ALiVE version 1.3.5.1705161
//////////////////////////////////////////////////////////////////////////////////

class CfgFactionClasses {
    class ALiVE_Droids {
        displayName = "ALiVE Droids";
        side = 0;
        flag = "";
        icon = "";
        priority = 0;
    };
};

class CfgGroups {
    class EAST {

        class ALiVE_Droids {
            name = "ALiVE Droids";

            class Infantry {
                name = "Infantry";

                class alivedroids_infantry_battledroid_squad {
                    name = "Battledroid Squad";
                    side = 0;
                    faction = "ALiVE_Droids";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 0.5;

                    class Unit0 {
                        position[] = { 0 , 0 , 0 };
                        rank = "SERGEANT";
                        side = 0;
                        vehicle = "O_ALiVEDroids_Battledroid_Commander_01";
                    };
                    class Unit1 {
                        position[] = { 5 , -5 , 0 };
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "O_ALiVEDroids_Battledroid3_01";
                    };
                    class Unit2 {
                        position[] = { -5 , -5 , 0 };
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "O_ALiVEDroids_Battledroid2_01";
                    };
                    class Unit3 {
                        position[] = { 10 , -10 , 0 };
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "O_ALiVEDroids_Battledroid2_01";
                    };
                    class Unit4 {
                        position[] = { -10 , -10 , 0 };
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "O_ALiVEDroids_Battledroid2_01";
                    };
                    class Unit5 {
                        position[] = { 15 , -15 , 0 };
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "O_ALiVEDroids_Battledroid2_01";
                    };
                    class Unit6 {
                        position[] = { -15 , -15 , 0 };
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "O_ALiVEDroids_Battledroid2_01";
                    };
                    class Unit7 {
                        position[] = { 20 , -20 , 0 };
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "O_ALiVEDroids_Battledroid2_01";
                    };
                    class Unit8 {
                        position[] = { -20 , -20 , 0 };
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "O_ALiVEDroids_BattleDroid1_01";
                    };
                    class Unit9 {
                        position[] = { 25 , -25 , 0 };
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "O_ALiVEDroids_BattleDroid1_01";
                    };
                    class Unit10 {
                        position[] = { -25 , -25 , 0 };
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "O_ALiVEDroids_BattleDroid1_01";
                    };
                    class Unit11 {
                        position[] = { 30 , -30 , 0 };
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "O_ALiVEDroids_BattleDroid1_01";
                    };
                    class Unit12 {
                        position[] = { -30 , -30 , 0 };
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "O_ALiVEDroids_Battledroid3_01";
                    };
                    class Unit13 {
                        position[] = { 35 , -35 , 0 };
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "O_ALiVEDroids_Battledroid3_01";
                    };
                };

                class alivedroids_infantry_superbattledroid_team {
                    name = "Superbattledroid Team";
                    side = 0;
                    faction = "ALiVE_Droids";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 0.5;

                    class Unit0 {
                        position[] = { 0 , 0 , 0 };
                        rank = "SERGEANT";
                        side = 0;
                        vehicle = "O_ALiVEDroids_Battledroid_Commander_01";
                    };
                    class Unit1 {
                        position[] = { 5 , -5 , 0 };
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "O_ALiVEDroids_Super_Battle_Droid_01";
                    };
                    class Unit2 {
                        position[] = { -5 , -5 , 0 };
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "O_ALiVEDroids_Super_Battle_Droid_01";
                    };
                    class Unit3 {
                        position[] = { 10 , -10 , 0 };
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "O_ALiVEDroids_Super_Battle_Droid_01";
                    };
                    class Unit4 {
                        position[] = { -10 , -10 , 0 };
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "O_ALiVEDroids_Super_Battle_Droid_01";
                    };
                    class Unit5 {
                        position[] = { 15 , -15 , 0 };
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "O_ALiVEDroids_Super_Battle_Droid_01";
                    };
                    class Unit6 {
                        position[] = { -15 , -15 , 0 };
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "O_ALiVEDroids_Super_Battle_Droid_01";
                    };
                };

            };

            class SpecOps {
                name = "Special Forces";

            };

            class Motorized {
                name = "Motorized Infantry";

            };

            class Motorized_MTP {
                name = "Motorized Infantry (MTP)";

            };

            class Support {
                name = "Support Infantry";

            };

            class Mechanized {
                name = "Mechanized Infantry";

            };

            class Armored {
                name = "Armor";

                class alivedroids_infantry_tanks {
                    name = "Tanks";
                    side = 0;
                    faction = "ALiVE_Droids";
                    icon = "\A3\ui_f\data\map\markers\nato\o_armor.paa";
                    rarityGroup = 0.5;

                    class Unit0 {
                        position[] = { 0 , 0 , 0 };
                        rank = "SERGEANT";
                        side = 0;
                        vehicle = "O_ALiVEDroids_AAY_Tank_01";
                    };
                    class Unit1 {
                        position[] = { 12 , -15 , 0 };
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "O_ALiVEDroids_AAY_Tank_01";
                    };
                };

                class alivedroids_armored_fighters {
                    name = "Fighters";
                    side = 0;
                    faction = "ALiVE_Droids";
                    icon = "\A3\ui_f\data\map\markers\nato\o_plane.paa";
                    rarityGroup = 0.5;

                    class Unit0 {
                        position[] = { 0 , 0 , 0 };
                        rank = "SERGEANT";
                        side = 0;
                        vehicle = "O_ALiVEDroids_Nantex_Fighter_01";
                    };
                    class Unit1 {
                        position[] = { 10 , -18 , 0 };
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "O_ALiVEDroids_Nantex_Fighter_01";
                    };
                };

            };

            class Artillery {
                name = "Artillery";

            };

            class Naval {
                name = "Naval";

            };

            class Air {
                name = "Air";

            };


        };

    };
};


class CBA_Extended_EventHandlers_base;

class CfgVehicles {

    class SWOP_CIS_droid;
    class SWOP_CIS_droid_OCimport_01 : SWOP_CIS_droid { scope = 0; class EventHandlers; };
    class SWOP_CIS_droid_OCimport_02 : SWOP_CIS_droid_OCimport_01 { class EventHandlers; };

    class SWOP_CIS_droid_mg;
    class SWOP_CIS_droid_mg_OCimport_01 : SWOP_CIS_droid_mg { scope = 0; class EventHandlers; };
    class SWOP_CIS_droid_mg_OCimport_02 : SWOP_CIS_droid_mg_OCimport_01 { class EventHandlers; };

    class CAA_CIS_Battledroid_AT;
    class CAA_CIS_Battledroid_AT_OCimport_01 : CAA_CIS_Battledroid_AT { scope = 0; class EventHandlers; };
    class CAA_CIS_Battledroid_AT_OCimport_02 : CAA_CIS_Battledroid_AT_OCimport_01 { class EventHandlers; };

    class SWOP_CIS_droid_com;
    class SWOP_CIS_droid_com_OCimport_01 : SWOP_CIS_droid_com { scope = 0; class EventHandlers; };
    class SWOP_CIS_droid_com_OCimport_02 : SWOP_CIS_droid_com_OCimport_01 { class EventHandlers; };

    class CAA_CIS_b2droid_laser;
    class CAA_CIS_b2droid_laser_OCimport_01 : CAA_CIS_b2droid_laser { scope = 0; class EventHandlers; };
    class CAA_CIS_b2droid_laser_OCimport_02 : CAA_CIS_b2droid_laser_OCimport_01 { class EventHandlers; };

    class swop_geofighter;
    class swop_geofighter_OCimport_01 : swop_geofighter { scope = 0; class EventHandlers; };
    class swop_geofighter_OCimport_02 : swop_geofighter_OCimport_01 { scope = 0; class EventHandlers; };

    class SWOP_CIS_droid_pilot;
    class SWOP_CIS_droid_pilot_OCimport_01 : SWOP_CIS_droid_pilot { scope = 0; class EventHandlers; };
    class SWOP_CIS_droid_pilot_OCimport_02 : SWOP_CIS_droid_pilot_OCimport_01 { class EventHandlers; };

    class SWOP_CIS_droid_crew;
    class SWOP_CIS_droid_crew_OCimport_01 : SWOP_CIS_droid_crew { scope = 0; class EventHandlers; };
    class SWOP_CIS_droid_crew_OCimport_02 : SWOP_CIS_droid_crew_OCimport_01 { class EventHandlers; };

    class O_SWOP_AAT_2;
    class O_SWOP_AAT_2_OCimport_01 : O_SWOP_AAT_2 { scope = 0; class EventHandlers; class Turrets; };
    class O_SWOP_AAT_2_OCimport_02 : O_SWOP_AAT_2_OCimport_01 { 
        class EventHandlers; 
        class Turrets : Turrets {
            class MainTurret;
        };
    };


    class O_ALiVEDroids_BattleDroid1_01 : SWOP_CIS_droid_OCimport_02 {
        author = "[RM] Cpt Smith";
        scope = 2;
        scopeCurator = 2;
        displayName = "BattleDroid1";
        side = 0;
        faction = "ALiVE_Droids";

        identityTypes[] = { "TK421" };


        class EventHandlers : EventHandlers {
            class CBA_Extended_EventHandlers : CBA_Extended_EventHandlers_base {};

            class ALiVE_orbatCreator {
                init = "if (local (_this select 0)) then {_unit = _this select 0;_onSpawn = {_unit = _this select 0;_unit setUnitLoadout [['SWOP_E5','','','',['SWOP_E5_Mag',60],[],''],[],[],['B1_F_CombatUniform',[['FirstAidKit',1],['SWOP_E5_Mag',8,60],['SWOP_termDet_G',2,1]]],['STbron',[]],['CAA_InvisBag',[]],'','',[],['ItemMap','ItemGPS','ItemRadio','ItemCompass','ItemWatch','']];reload _unit;};[_unit] call _onSpawn;_unit addMPEventHandler ['MPRespawn', _onSpawn];};";
            };

        };

        // custom attributes (do not delete)
        ALiVE_orbatCreator_owned = 1;

    };

    class O_ALiVEDroids_Battledroid2_01 : SWOP_CIS_droid_mg_OCimport_02 {
        author = "[RM] Cpt Smith";
        scope = 2;
        scopeCurator = 2;
        displayName = "Battledroid2";
        side = 0;
        faction = "ALiVE_Droids";

        identityTypes[] = { "TK421" };


        class EventHandlers : EventHandlers {
            class CBA_Extended_EventHandlers : CBA_Extended_EventHandlers_base {};

            class ALiVE_orbatCreator {
                init = "if (local (_this select 0)) then {_unit = _this select 0;_onSpawn = {_unit = _this select 0;_unit setUnitLoadout [['SWOP_E5l','','','',['SWOP_E5l_Mag',100],[],''],[],[],['B1_F_CombatUniform',[['FirstAidKit',1],['SWOP_E5l_Mag',8,100]]],['STbron',[['SWOP_E5l_Mag',1,100],['SWOP_termDet_G',2,1]]],['CAA_InvisBag',[]],'','',[],['ItemMap','ItemGPS','ItemRadio','ItemCompass','ItemWatch','']];reload _unit;};[_unit] call _onSpawn;_unit addMPEventHandler ['MPRespawn', _onSpawn];};";
            };

        };

        // custom attributes (do not delete)
        ALiVE_orbatCreator_owned = 1;

    };

    class O_ALiVEDroids_Battledroid3_01 : CAA_CIS_Battledroid_AT_OCimport_02 {
        author = "[RM] Cpt Smith";
        scope = 2;
        scopeCurator = 2;
        displayName = "Battledroid3";
        side = 0;
        faction = "ALiVE_Droids";

        identityTypes[] = { "TK421" };


        class EventHandlers : EventHandlers {
            class CBA_Extended_EventHandlers : CBA_Extended_EventHandlers_base {};

            class ALiVE_orbatCreator {
                init = "if (local (_this select 0)) then {_unit = _this select 0;_onSpawn = {_unit = _this select 0;_unit setUnitLoadout [['SWOP_E5','','','',['SWOP_E5_Mag',60],[],''],['SW_E60r','','','',['e60rRocket',1],[],''],[],['B1_F_CombatUniform',[['FirstAidKit',1],['ACE_EarPlugs',1],['SWOP_E5_Mag',4,60]]],[],['CAA_InvisBag',[]],'','',[],['ItemMap','','ItemRadio','ItemCompass','ItemWatch','']];reload _unit;};[_unit] call _onSpawn;_unit addMPEventHandler ['MPRespawn', _onSpawn];};";
            };

        };

        // custom attributes (do not delete)
        ALiVE_orbatCreator_owned = 1;

    };

    class O_ALiVEDroids_Battledroid_Commander_01 : SWOP_CIS_droid_com_OCimport_02 {
        author = "[RM] Cpt Smith";
        scope = 2;
        scopeCurator = 2;
        displayName = "Battledroid Commander";
        side = 0;
        faction = "ALiVE_Droids";

        identityTypes[] = { "TK421" };


        class EventHandlers : EventHandlers {
            class CBA_Extended_EventHandlers : CBA_Extended_EventHandlers_base {};

            class ALiVE_orbatCreator {
                init = "if (local (_this select 0)) then {_unit = _this select 0;_onSpawn = {_unit = _this select 0;_unit setUnitLoadout [['SWOP_E5l','','','',['SWOP_E5l_Mag',100],[],''],[],[],['B1_F_com_CombatUniform',[['FirstAidKit',1],['SWOP_E5l_Mag',4,100],['SWOP_E5_Mag',3,60],['SWOP_termDet_G',2,1]]],['STbron',[]],['CAA_InvisBag',[]],'','',['ElectroBinocularsD_F','','','',[],[],''],['ItemMap','ItemGPS','ItemRadio','ItemCompass','ItemWatch','']];reload _unit;};[_unit] call _onSpawn;_unit addMPEventHandler ['MPRespawn', _onSpawn];};";
            };

        };

        // custom attributes (do not delete)
        ALiVE_orbatCreator_owned = 1;

    };

    class O_ALiVEDroids_Super_Battle_Droid_01 : CAA_CIS_b2droid_laser_OCimport_02 {
        author = "[RM] Cpt Smith";
        scope = 2;
        scopeCurator = 2;
        displayName = "Super Battle Droid";
        side = 0;
        faction = "ALiVE_Droids";

        identityTypes[] = { "TK421" };


        class EventHandlers : EventHandlers {
            class CBA_Extended_EventHandlers : CBA_Extended_EventHandlers_base {};

            class ALiVE_orbatCreator {
                init = "if (local (_this select 0)) then {_unit = _this select 0;_onSpawn = {_unit = _this select 0;_unit setUnitLoadout [['b2gun','','','',['b2gun_Mag',60],[],''],[],[],['caa_b2_CombatUniform',[['FirstAidKit',1],['b2gun_Mag',6,60]]],[],['CAA_InvisBag',[]],'','',[],['ItemMap','','ItemRadio','ItemCompass','ItemWatch','']];reload _unit;};[_unit] call _onSpawn;_unit addMPEventHandler ['MPRespawn', _onSpawn];};";
            };

        };

        // custom attributes (do not delete)
        ALiVE_orbatCreator_owned = 1;

    };

    class O_ALiVEDroids_Nantex_Fighter_01 : swop_geofighter_OCimport_02 {
        author = "[RM] Cpt Smith";
        scope = 2;
        scopeCurator = 2;
        displayName = "Nantex Fighter";
        side = 0;
        faction = "ALiVE_Droids";
        crew = "SWOP_CIS_geon_pilot";


        class EventHandlers : EventHandlers {
            class CBA_Extended_EventHandlers : CBA_Extended_EventHandlers_base {};

            class ALiVE_orbatCreator {
            };

        };

        // custom attributes (do not delete)
        ALiVE_orbatCreator_owned = 1;

    };

    class O_ALiVEDroids_Pilot_01 : SWOP_CIS_droid_pilot_OCimport_02 {
        author = "[RM] Cpt Smith";
        scope = 2;
        scopeCurator = 2;
        displayName = "Pilot";
        side = 0;
        faction = "ALiVE_Droids";

        identityTypes[] = { "TK421" };


        class EventHandlers : EventHandlers {
            class CBA_Extended_EventHandlers : CBA_Extended_EventHandlers_base {};

            class ALiVE_orbatCreator {
                init = "if (local (_this select 0)) then {_unit = _this select 0;_onSpawn = {_unit = _this select 0;_unit setUnitLoadout [['SWOP_E5','','','',['SWOP_E5_Mag',60],[],''],[],[],['B1_F_pil_CombatUniform',[['FirstAidKit',1],['SWOP_E5_Mag',8,60]]],['STbron',[['SWOP_termDet_G',2,1]]],['CAA_InvisBag',[]],'','',[],['ItemMap','ItemGPS','ItemRadio','ItemCompass','ItemWatch','']];reload _unit;};[_unit] call _onSpawn;_unit addMPEventHandler ['MPRespawn', _onSpawn];};";
            };

        };

        // custom attributes (do not delete)
        ALiVE_orbatCreator_owned = 1;

    };

    class O_ALiVEDroids_Crew_01 : SWOP_CIS_droid_crew_OCimport_02 {
        author = "[RM] Cpt Smith";
        scope = 2;
        scopeCurator = 2;
        displayName = "Crew";
        side = 0;
        faction = "ALiVE_Droids";

        identityTypes[] = { "TK421" };


        class EventHandlers : EventHandlers {
            class CBA_Extended_EventHandlers : CBA_Extended_EventHandlers_base {};

            class ALiVE_orbatCreator {
                init = "if (local (_this select 0)) then {_unit = _this select 0;_onSpawn = {_unit = _this select 0;_unit setUnitLoadout [['SWOP_E5','','','',['SWOP_E5_Mag',60],[],''],[],[],['B1_F_tank_CombatUniform',[['FirstAidKit',1],['SWOP_E5_Mag',8,60],['SWOP_termDet_G',2,1]]],['STbron',[]],['CAA_InvisBag',[]],'','',[],['ItemMap','ItemGPS','ItemRadio','ItemCompass','ItemWatch','']];reload _unit;};[_unit] call _onSpawn;_unit addMPEventHandler ['MPRespawn', _onSpawn];};";
            };

        };

        // custom attributes (do not delete)
        ALiVE_orbatCreator_owned = 1;

    };

    class O_ALiVEDroids_Nantex_Fighter_02 : swop_geofighter_OCimport_02 {
        author = "[RM] Cpt Smith";
        scope = 2;
        scopeCurator = 2;
        displayName = "Nantex Fighter";
        side = 0;
        faction = "ALiVE_Droids";
        crew = "O_ALiVEDroids_Pilot_01";


        class EventHandlers : EventHandlers {
            class CBA_Extended_EventHandlers : CBA_Extended_EventHandlers_base {};

            class ALiVE_orbatCreator {
            };

        };

        // custom attributes (do not delete)
        ALiVE_orbatCreator_owned = 1;

    };

    class O_ALiVEDroids_AAY_Tank_01 : O_SWOP_AAT_2_OCimport_02 {
        author = "[RM] Cpt Smith";
        scope = 2;
        scopeCurator = 2;
        displayName = "AAY Tank";
        side = 0;
        faction = "ALiVE_Droids";
        crew = "O_ALiVEDroids_Crew_01";

        class Turrets : Turrets {
            class MainTurret : MainTurret { gunnerType = "O_ALiVEDroids_Crew_01"; };
        };



        class EventHandlers : EventHandlers {
            class CBA_Extended_EventHandlers : CBA_Extended_EventHandlers_base {};

            class ALiVE_orbatCreator {
            };

        };

        // custom attributes (do not delete)
        ALiVE_orbatCreator_owned = 1;

    };

};