class CfgPatches {
    class ALiVE_Droids {
        units[] = {
            "O_ALiVEDroids_BattleDroid1_01",
            "O_ALiVEDroids_Battledroid2_01",
            "O_ALiVEDroids_Battledroid3_01",
            "O_ALiVEDroids_Battledroid_Commander_01",
            "O_ALiVEDroids_Super_Battle_Droid_01",
            "O_ALiVEDroids_Nantex_Fighter_01",
            "O_ALiVEDroids_Pilot_01",
            "O_ALiVEDroids_Crew_01",
            "O_ALiVEDroids_Nantex_Fighter_02",
            "O_ALiVEDroids_AAY_Tank_01"
        };
        weapons[] = {};
        requiredVersion = 1.62;
        requiredAddons[] = {
            "SWOP_Characters_CIS",
            "A3_Air_F_Beta",
            "A3_Air_F_Gamma_Plane_Fighter_03",
            "arc170",
            "awing",
            "geon_figherplane",
            "tie",
            "tieinterceptor",
            "TIE_Stryker",
            "tieb",
            "tiedef",
            "tiex1",
            "vulture",
            "swop_xwing",
            "A3_Air_F_EPC_Plane_CAS_01",
            "A3_Air_F_EPC_Plane_CAS_02",
            "A3_Air_F_Exp_Plane_Civil_01",
            "A3_Air_F_Jets_Plane_Fighter_01",
            "A3_Air_F_Jets_Plane_Fighter_02",
            "A3_Air_F_Jets_Plane_Fighter_04",
            "A3_Air_F_Jets_UAV_05",
            "SWOP_AAT"
        };
        author = "[RM] Cpt Smith";
        authors[] = { "[RM] Cpt Smith" };
    };
};
