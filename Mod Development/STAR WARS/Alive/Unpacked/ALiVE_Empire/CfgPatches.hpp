class CfgPatches {
    class ALIVE_Empire {
        units[] = {
            "O_ALIVEEmpire_Stormtrooper_01",
            "O_ALIVEEmpire_Stormtrooper_AT_01",
            "O_ALIVEEmpire_Stormtrooper_Medic_01",
            "O_ALIVEEmpire_Stormtrooper_MachineGunner_01",
            "O_ALIVEEmpire_Stormtrooper_Corporal_01",
            "O_ALIVEEmpire_Stormtrooper_Sergeant_01",
            "O_ALIVEEmpire_Crew_01",
            "O_ALIVEEmpire_Pilot_01",
            "O_ALIVEEmpire_Scouttrooper_01",
            "O_ALIVEEmpire_Scouttrooper_Marksman_01",
            "O_ALIVEEmpire_Stormtrooper_Flamethrower_01",
            "O_ALIVEEmpire_ITT_Transport_01",
            "O_ALIVEEmpire_ITT_Transport_Armed_01",
            "O_ALIVEEmpire_ITT_Transport_Flame_01",
            "O_ALIVEEmpire_ITT_Transport_Flame_02",
            "O_ALIVEEmpire_ITT_Occupier_01",
            "O_ALIVEEmpire_TX_130m1_Sabre_01",
            "O_ALIVEEmpire_ATST_01"
        };
        weapons[] = {};
        requiredVersion = 1.62;
        requiredAddons[] = {
            "A3_Characters_F",
            "SWOP_HoverT",
            "SWOP_HoverTa",
            "SWOP_HoverTr",
            "SWOP_HoverTf",
            "CAA_TX225",
            "JM_TX130m1",
            "ATST_Movil"
        };
        author = "Lemon Mission Creator";
        authors[] = { "Lemon Mission Creator" };
    };
};
