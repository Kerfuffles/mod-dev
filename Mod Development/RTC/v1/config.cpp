#include "BIS_AddonInfo.hpp"
#include "mod.cpp"

class CfgPatches 
{
	class RTC
	{
		units[] = {};
		weapons[] = {}; 
		requiredAddons[] = {};	
		author[]= {"vlad_8011"}; 		
	};
};

class Extended_InitPost_EventHandlers {
  class Tank {
    class tankIsNowBunker { init = "(_this select 0) allowCrewInImmobile true;"; };
  };
};

class CfgFunctions
{
	class RTC
	{
		class RTC_Initialization
		{
			class Init
			{
				file = "\rtc\init.sqf";
				preInit = 1;
			};
		};

	};
};
