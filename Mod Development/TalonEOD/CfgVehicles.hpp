class cfgVehicles
{
	class Weapon_Bag_Base;
	class B_UAV_01_backpack_F: Weapon_Bag_Base
	{
		class assembleinfo;
		class EventHandlers;
	};
	class IEU_Talon_Pack: B_UAV_01_backpack_F
	{
		_generalmacro = "IEU_Talon_Pack";
		author = "V. Zeitsev";
		displayname = "Talon II Backpack";
		faction = "BLU_F";
		hiddenselectionstextures[] = {"\A3\Drones_F\Weapons_F_Gamma\Ammoboxes\Bags\data\UAV_backpack_rgr_co.paa"};
		mapsize = 0.6;
		mass = 600;
		maximumload = 0;
		model = "\A3\Drones_F\Weapons_F_Gamma\Ammoboxes\Bags\UAV_backpack_F.p3d";
		picture = "\A3\Drones_F\Weapons_F_Gamma\ammoboxes\bags\data\ui\icon_B_C_UAV_rgr_ca";
		scope = 2;
		side = 1;
		class assembleInfo: assembleinfo
		{
			assembleto = "TALON_MARKII";
			base = "";
			displayname = "Talon II";
		};
	};
	
	class LandVehicle;
	class Tank: LandVehicle
	{
	};
	class Tank_F: Tank
	{
	};
	class EWK_Talon_base: Tank_F
	{
	};
	class Talon_Base: EWK_Talon_base
	{
	};
	
	class TALON_MARKII: Talon_Base
	{
		class assembleInfo
		{
			assembleto = "";
			base = "";
			displayname = "";
			dissasembleto[] = {"IEU_Talon_Pack"};
			primary = 1;
		};
	};
};