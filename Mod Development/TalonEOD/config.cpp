class CfgPatches
{
	class IEU_EODSTALON_PACK
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {};
		version = "1";
		projectName = "IEU Talon Backpack";
		author = "V. Zeitsev";
	};
};

#include "CfgVehicles.hpp"