GVAR(exShakeEnabled) = true;
GVAR(recoilEnabled) = true;

[
    "L_Immerse_exShake",
    "CHECKBOX",
    ["Enable exShake", "CamShake for explosions, even distant ones!"],
    "Laxemann Immerse",
    true,
    nil,
    {
        params ["_value"];
        GVAR(exShakeEnabled) = _value;
        if (GVAR(exShakeEnabled)) then {
            if (isNil QGVAR(exShakePFH)) then {
                GVAR(exShakePFH) = [{
                    call FUNC(exShakeHandler);
                }, 0.05] call CBA_fnc_addPerframeHandler;
            };
        } else {
            GVAR(exShakePFH) call CBA_fnc_removePerFrameHandler;
            GVAR(exShakePFH) = nil;
        };
    }
] call CBA_Settings_fnc_init;

[
    "L_Immerse_recoil",
    "CHECKBOX",
    ["Enable recoil", "Adds a slight CamShake when shooting, should make shooting the guns feel more satisfying (Disabled if L_align is Activ)"],
    "Laxemann Immerse",
    true,
    nil,
    {
        params ["_value"];
        GVAR(recoilEnabled) = _value;
    }
] call CBA_Settings_fnc_init;