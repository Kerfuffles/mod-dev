#define COMPONENT Immerse_sys

//#define DEBUG_MODE_FULL
#include "\LAxemann\L_Immerse\addons\Immerse_main\script_mod.hpp"
#include "\LAxemann\L_Immerse\addons\Immerse_main\script_macros.hpp"

// #define isDev

#ifdef isDev
    #define DUMP(var) diag_log format ["(%1)[Immerse]: %2", diag_frameNo, var]; systemChat format ["(%1)[Immerse]: %2", diag_frameNo, var];
#else
    #define DUMP(var) /* Disabled */
#endif

