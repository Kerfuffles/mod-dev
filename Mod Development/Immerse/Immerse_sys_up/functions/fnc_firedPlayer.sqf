#include "script_component.hpp"
/*
 * Author: joko // Jonas
 * Player Fired Event Handler
 *
 * Arguments:
 * Fired Event Paramter
 *
 * Return Value:
 * None
 */

params [
	"_shooter",
	"_weapon",
	"",
	"",
	"_ammo",
	"",
	"_projectile"
];
if (toLower(_weapon) isEqualTo "put") exitWith {};
if (isNull _projectile) then {
     _projectile = nearestObject [_shooter, _ammo];
     _this set [6, _projectile];
};
if (GVAR(recoilEnabled)) then {
	if (isNil "L_align_active") then {
		if ((_weapon != (toLower "throw")) && (_weapon != (toLower "put"))) then {
			addCamshake [1.3,0.4,15];
			DUMP("Recoil Triggerd")
		};
	};
};