#include "script_component.hpp"

ADDON = false;
// Config Save Namespace
GVAR(variableHandler) = call CBA_fnc_createNamespace;
GVAR(exShakeArray) = [];
GVAR(playerFiredEHID) = -1;
// event Functions
PREP(fired);
PREP(firedExShake);
PREP(firedPlayer);
PREP(exShakeHandler);

// Misc
PREP(readCacheValues);

["unit", {
    params ["_newPlayer", "_oldPlayer"];
    if (GVAR(playerFiredEHID) != -1) then {
        _oldPlayer removeEventHandler ["Fired", GVAR(playerFiredEHID)];
    };
    GVAR(playerFiredEHID) = _newPlayer addEventHandler ["Fired", {_this call FUNC(firedPlayer)}];
    resetCamShake; // reset CamShake Effect
}] call CBA_fnc_addPlayerEventHandler;

// ExShake Effect Functions
L_fnc_ExShake_BigBomb = compile preprocessFileLineNumbers "\LAxemann\L_Immerse\addons\Immerse_sys\effects\BigBomb.sqf";
L_fnc_ExShake_FuelExploSmall = compile preprocessFileLineNumbers "\LAxemann\L_Immerse\addons\Immerse_sys\effects\FuelExploSmall.sqf";
L_fnc_ExShake_FuelExploBig = compile preprocessFileLineNumbers "\LAxemann\L_Immerse\addons\Immerse_sys\effects\FuelExploBig.sqf";
L_fnc_ExShake_BigRocket = compile preprocessFileLineNumbers "\LAxemann\L_Immerse\addons\Immerse_sys\effects\BigRocket.sqf";
L_fnc_ExShake_SmallRocket = compile preprocessFileLineNumbers "\LAxemann\L_Immerse\addons\Immerse_sys\effects\SmallRocket.sqf";
L_fnc_ExShake_120mm = compile preprocessFileLineNumbers "\LAxemann\L_Immerse\addons\Immerse_sys\effects\120mm.sqf";
L_fnc_ExShake_Arty = compile preprocessFileLineNumbers "\LAxemann\L_Immerse\addons\Immerse_sys\effects\Arty.sqf";
L_fnc_ExShake_Mortar = compile preprocessFileLineNumbers "\LAxemann\L_Immerse\addons\Immerse_sys\effects\Mortar.sqf";
L_fnc_ExShake_Satchel = compile preprocessFileLineNumbers "\LAxemann\L_Immerse\addons\Immerse_sys\effects\IED.sqf";
L_fnc_ExShake_Grenade = compile preprocessFileLineNumbers "\LAxemann\L_Immerse\addons\Immerse_sys\effects\Grenade.sqf";

#include "settings.sqf"

ADDON = false;