class CfgPatches
{
	class IEU_Debug
	{
		name = "IEU Debugging and Messaging Script Handler";
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.64;
		requiredAddons[] = {};
		authors[] = {"V. Zeitsev"};
		url = "http://ieu-a3.com/";
		version = "0.0.0.1";
		versionStr = "0.0.0.1";
		versionAr[] = {0,0,0,1};
	};
};

class IEU_EH_Init
{
	class IEU_Debug
	{
		init = "call compile preProcessFileLineNumbers 'IEU_Debug\scripts\playerjoin.sqf'";
	};
};